package main

import (
	"alortee/audplay/player"
	"os"
)

func main() {
	p := player.NewPlayer(os.Args)
	p.Run()
}
