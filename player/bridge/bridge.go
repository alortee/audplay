package bridge

import (
	"alortee/audplay/player/engine"
	"alortee/audplay/player/models"
	"alortee/audplay/player/views"
	"github.com/therecipe/qt/core"
	"log"
	"strings"
	"time"
)

func init() { qmlBridge_QmlRegisterType2("QMLBridge", 1, 0, "QMLBridge") }

var Engine *engine.Engine
var Views *views.Views

type qmlBridge struct {
	core.QObject

	_ func() `constructor:"init"`

	// navigator
	_ *Navigator `property:"navigator"`

	// qMedia
	_ *QMedia `property:"qMedia"`

	// props
	_ string `property:"thumbsPath"`

	// models
	_ core.QAbstractItemModel `property:"tracksModel"`
	_ core.QAbstractItemModel `property:"albumsModel"`
	_ core.QAbstractItemModel `property:"favouritesModel"`

	// slots
	_ func(folders []string) `slot:"addFolders,auto"`
	_ func()                 `slot:"importTracks,auto"`
	_ func()                 `slot:"updateTracksModelData,auto"`
	_ func(filepath string)  `slot:"toggleTrackFavourite,auto"`

	// signals
	_ func(scanCount int)     `signal:"folderScanComplete"`
	_ func(trackTitle string) `signal:"trackImported"`
	_ func()                  `signal:"trackImportComplete"`
}

func (q *qmlBridge) init() {
	q.SetThumbsPath("file://" + Engine.ThumbsPath())
	q.SetNavigator(NewNavigator(nil))
	q.SetQMedia(NewQMedia(nil))
	q.SetTracksModel(models.NewTracksModel(nil))
	q.SetAlbumsModel(models.NewAlbumsModel(nil))
	q.SetFavouritesModel(models.NewFavouritesModel(nil))
	q.Navigator().SetCurrentView("Tracks")

	q.ConnectTrackImportComplete(func() {
		if Views.Has("FirstRun") {
			Views.RemoveView("FirstRun")
			Views.AddView("Default")
			Views.ShowView("Default")
		}
	})
}

func (q *qmlBridge) addFolders(folders []string) {
	go func() {
		start := time.Now()
		for _, folder := range folders {
			f := strings.Replace(folder, "file://", "", -1)
			_ = Engine.AddFolder(f)
		}
		end := time.Now()
		scanCount := Engine.Player.TrackPathsCount()
		log.Println("scanned for", scanCount, "tracks in:", end.Sub(start).Seconds())
		q.FolderScanComplete(scanCount)
	}()
}

func (q *qmlBridge) importTracks() {
	go func() {
		start := time.Now()
		addedChan, _ := Engine.ImportTracks()
		for title := range addedChan {
			q.TrackImported(title)
		}
		end := time.Now()
		scanCount := Engine.Player.TrackPathsCount()
		log.Println("added", scanCount, "tracks in:", end.Sub(start).Seconds())
		q.TrackImportComplete()
	}()
}

func (q *qmlBridge) updateTracksModelData() {
	lenEnd := len(Engine.Player.Tracks())
	if lenEnd > 0 {
		q.TracksModel().BeginResetModel()
		q.TracksModel().EndResetModel()
		//q.TracksModel().BeginInsertRows(core.NewQModelIndex(), 0, lenEnd-1)
		//q.TracksModel().EndInsertRows()
	}
}

func (q *qmlBridge) toggleTrackFavourite(filepath string) {
	t, _ := Engine.Player.GetTrackByFilePath(filepath)
	err := Engine.ToggleTrackFavourite(t)
	if err != nil {
		return
	}
	q.TracksModel().DataChanged(q.TracksModel().CreateIndex(0, 0, nil), q.TracksModel().CreateIndex(q.TracksModel().RowCount(nil)-1, 0, nil), []int{})
	q.FavouritesModel().BeginResetModel()
	q.FavouritesModel().EndResetModel()
	if filepath == q.QMedia().CurrentTrackFilePath() {
		q.QMedia().CurrentMediaChanged(t.Title, t.Artist, t.Picture, t.FilePath, t.IsFavourite)
	}
}
