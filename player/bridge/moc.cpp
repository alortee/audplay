

#define protected public
#define private public

#include "moc.h"
#include "_cgo_export.h"

#include <QAbstractItemModel>
#include <QAbstractListModel>
#include <QByteArray>
#include <QCameraImageCapture>
#include <QChildEvent>
#include <QEvent>
#include <QGraphicsObject>
#include <QGraphicsWidget>
#include <QLayout>
#include <QMediaPlaylist>
#include <QMediaRecorder>
#include <QMetaMethod>
#include <QObject>
#include <QOffscreenSurface>
#include <QPaintDeviceWindow>
#include <QPdfWriter>
#include <QQuickItem>
#include <QRadioData>
#include <QString>
#include <QTimerEvent>
#include <QWidget>
#include <QWindow>

#ifdef QT_QML_LIB
	#include <QQmlEngine>
#endif

class AlbumsModelb23e62: public QAbstractListModel{
public:
	AlbumsModelb23e62(QObject *parent) : QAbstractListModel(parent) {};

};
class FavouritesModelb23e62: public QAbstractListModel{
public:
	FavouritesModelb23e62(QObject *parent) : QAbstractListModel(parent) {};

};
class TracksModelb23e62: public QAbstractListModel{
public:
	TracksModelb23e62(QObject *parent) : QAbstractListModel(parent) {};

};

class Navigator027749: public QObject
{
Q_OBJECT
Q_PROPERTY(QString currentView READ currentView WRITE setCurrentView NOTIFY currentViewChanged)
public:
	Navigator027749(QObject *parent = Q_NULLPTR) : QObject(parent) {qRegisterMetaType<quintptr>("quintptr");Navigator027749_Navigator027749_QRegisterMetaType();Navigator027749_Navigator027749_QRegisterMetaTypes();callbackNavigator027749_Constructor(this);};
	void Signal_Navigated(QString view) { QByteArray t8f3a07 = view.toUtf8(); Moc_PackedString viewPacked = { const_cast<char*>(t8f3a07.prepend("WHITESPACE").constData()+10), t8f3a07.size()-10 };callbackNavigator027749_Navigated(this, viewPacked); };
	QString currentView() { return ({ Moc_PackedString tempVal = callbackNavigator027749_CurrentView(this); QString ret = QString::fromUtf8(tempVal.data, tempVal.len); free(tempVal.data); ret; }); };
	void setCurrentView(QString currentView) { QByteArray t3d06cc = currentView.toUtf8(); Moc_PackedString currentViewPacked = { const_cast<char*>(t3d06cc.prepend("WHITESPACE").constData()+10), t3d06cc.size()-10 };callbackNavigator027749_SetCurrentView(this, currentViewPacked); };
	void Signal_CurrentViewChanged(QString currentView) { QByteArray t3d06cc = currentView.toUtf8(); Moc_PackedString currentViewPacked = { const_cast<char*>(t3d06cc.prepend("WHITESPACE").constData()+10), t3d06cc.size()-10 };callbackNavigator027749_CurrentViewChanged(this, currentViewPacked); };
	 ~Navigator027749() { callbackNavigator027749_DestroyNavigator(this); };
	void childEvent(QChildEvent * event) { callbackNavigator027749_ChildEvent(this, event); };
	void connectNotify(const QMetaMethod & sign) { callbackNavigator027749_ConnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void customEvent(QEvent * event) { callbackNavigator027749_CustomEvent(this, event); };
	void deleteLater() { callbackNavigator027749_DeleteLater(this); };
	void Signal_Destroyed(QObject * obj) { callbackNavigator027749_Destroyed(this, obj); };
	void disconnectNotify(const QMetaMethod & sign) { callbackNavigator027749_DisconnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	bool event(QEvent * e) { return callbackNavigator027749_Event(this, e) != 0; };
	bool eventFilter(QObject * watched, QEvent * event) { return callbackNavigator027749_EventFilter(this, watched, event) != 0; };
	void Signal_ObjectNameChanged(const QString & objectName) { QByteArray taa2c4f = objectName.toUtf8(); Moc_PackedString objectNamePacked = { const_cast<char*>(taa2c4f.prepend("WHITESPACE").constData()+10), taa2c4f.size()-10 };callbackNavigator027749_ObjectNameChanged(this, objectNamePacked); };
	void timerEvent(QTimerEvent * event) { callbackNavigator027749_TimerEvent(this, event); };
	QString currentViewDefault() { return _currentView; };
	void setCurrentViewDefault(QString p) { if (p != _currentView) { _currentView = p; currentViewChanged(_currentView); } };
signals:
	void navigated(QString view);
	void currentViewChanged(QString currentView);
public slots:
	void navigateTo(QString view) { QByteArray t8f3a07 = view.toUtf8(); Moc_PackedString viewPacked = { const_cast<char*>(t8f3a07.prepend("WHITESPACE").constData()+10), t8f3a07.size()-10 };callbackNavigator027749_NavigateTo(this, viewPacked); };
private:
	QString _currentView;
};

Q_DECLARE_METATYPE(Navigator027749*)


void Navigator027749_Navigator027749_QRegisterMetaTypes() {
	qRegisterMetaType<QString>();
}

class QMedia027749: public QObject
{
Q_OBJECT
Q_PROPERTY(QString currentTrackFilePath READ currentTrackFilePath WRITE setCurrentTrackFilePath NOTIFY currentTrackFilePathChanged)
Q_PROPERTY(qint64 currentPosition READ currentPosition WRITE setCurrentPosition NOTIFY currentPositionChanged)
Q_PROPERTY(qint64 currentDuration READ currentDuration WRITE setCurrentDuration NOTIFY currentDurationChanged)
public:
	QMedia027749(QObject *parent = Q_NULLPTR) : QObject(parent) {qRegisterMetaType<quintptr>("quintptr");QMedia027749_QMedia027749_QRegisterMetaType();QMedia027749_QMedia027749_QRegisterMetaTypes();callbackQMedia027749_Constructor(this);};
	void Signal_PlayerStateChanged(qint32 state) { callbackQMedia027749_PlayerStateChanged(this, state); };
	void Signal_CurrentMediaChanged(QString title, QString artist, QString picture, QString filePath, bool favourite) { QByteArray t3c6de1 = title.toUtf8(); Moc_PackedString titlePacked = { const_cast<char*>(t3c6de1.prepend("WHITESPACE").constData()+10), t3c6de1.size()-10 };QByteArray t55051f = artist.toUtf8(); Moc_PackedString artistPacked = { const_cast<char*>(t55051f.prepend("WHITESPACE").constData()+10), t55051f.size()-10 };QByteArray t0e66b4 = picture.toUtf8(); Moc_PackedString picturePacked = { const_cast<char*>(t0e66b4.prepend("WHITESPACE").constData()+10), t0e66b4.size()-10 };QByteArray t7df503 = filePath.toUtf8(); Moc_PackedString filePathPacked = { const_cast<char*>(t7df503.prepend("WHITESPACE").constData()+10), t7df503.size()-10 };callbackQMedia027749_CurrentMediaChanged(this, titlePacked, artistPacked, picturePacked, filePathPacked, favourite); };
	QString currentTrackFilePath() { return ({ Moc_PackedString tempVal = callbackQMedia027749_CurrentTrackFilePath(this); QString ret = QString::fromUtf8(tempVal.data, tempVal.len); free(tempVal.data); ret; }); };
	void setCurrentTrackFilePath(QString currentTrackFilePath) { QByteArray tea7e18 = currentTrackFilePath.toUtf8(); Moc_PackedString currentTrackFilePathPacked = { const_cast<char*>(tea7e18.prepend("WHITESPACE").constData()+10), tea7e18.size()-10 };callbackQMedia027749_SetCurrentTrackFilePath(this, currentTrackFilePathPacked); };
	void Signal_CurrentTrackFilePathChanged(QString currentTrackFilePath) { QByteArray tea7e18 = currentTrackFilePath.toUtf8(); Moc_PackedString currentTrackFilePathPacked = { const_cast<char*>(tea7e18.prepend("WHITESPACE").constData()+10), tea7e18.size()-10 };callbackQMedia027749_CurrentTrackFilePathChanged(this, currentTrackFilePathPacked); };
	qint64 currentPosition() { return callbackQMedia027749_CurrentPosition(this); };
	void setCurrentPosition(qint64 currentPosition) { callbackQMedia027749_SetCurrentPosition(this, currentPosition); };
	void Signal_CurrentPositionChanged(qint64 currentPosition) { callbackQMedia027749_CurrentPositionChanged(this, currentPosition); };
	qint64 currentDuration() { return callbackQMedia027749_CurrentDuration(this); };
	void setCurrentDuration(qint64 currentDuration) { callbackQMedia027749_SetCurrentDuration(this, currentDuration); };
	void Signal_CurrentDurationChanged(qint64 currentDuration) { callbackQMedia027749_CurrentDurationChanged(this, currentDuration); };
	 ~QMedia027749() { callbackQMedia027749_DestroyQMedia(this); };
	void childEvent(QChildEvent * event) { callbackQMedia027749_ChildEvent(this, event); };
	void connectNotify(const QMetaMethod & sign) { callbackQMedia027749_ConnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void customEvent(QEvent * event) { callbackQMedia027749_CustomEvent(this, event); };
	void deleteLater() { callbackQMedia027749_DeleteLater(this); };
	void Signal_Destroyed(QObject * obj) { callbackQMedia027749_Destroyed(this, obj); };
	void disconnectNotify(const QMetaMethod & sign) { callbackQMedia027749_DisconnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	bool event(QEvent * e) { return callbackQMedia027749_Event(this, e) != 0; };
	bool eventFilter(QObject * watched, QEvent * event) { return callbackQMedia027749_EventFilter(this, watched, event) != 0; };
	void Signal_ObjectNameChanged(const QString & objectName) { QByteArray taa2c4f = objectName.toUtf8(); Moc_PackedString objectNamePacked = { const_cast<char*>(taa2c4f.prepend("WHITESPACE").constData()+10), taa2c4f.size()-10 };callbackQMedia027749_ObjectNameChanged(this, objectNamePacked); };
	void timerEvent(QTimerEvent * event) { callbackQMedia027749_TimerEvent(this, event); };
	QString currentTrackFilePathDefault() { return _currentTrackFilePath; };
	void setCurrentTrackFilePathDefault(QString p) { if (p != _currentTrackFilePath) { _currentTrackFilePath = p; currentTrackFilePathChanged(_currentTrackFilePath); } };
	qint64 currentPositionDefault() { return _currentPosition; };
	void setCurrentPositionDefault(qint64 p) { if (p != _currentPosition) { _currentPosition = p; currentPositionChanged(_currentPosition); } };
	qint64 currentDurationDefault() { return _currentDuration; };
	void setCurrentDurationDefault(qint64 p) { if (p != _currentDuration) { _currentDuration = p; currentDurationChanged(_currentDuration); } };
signals:
	void playerStateChanged(qint32 state);
	void currentMediaChanged(QString title, QString artist, QString picture, QString filePath, bool favourite);
	void currentTrackFilePathChanged(QString currentTrackFilePath);
	void currentPositionChanged(qint64 currentPosition);
	void currentDurationChanged(qint64 currentDuration);
public slots:
	void togglePlayback() { callbackQMedia027749_TogglePlayback(this); };
	void seek(qint64 position) { callbackQMedia027749_Seek(this, position); };
	void setVolume(qint32 position) { callbackQMedia027749_SetVolume(this, position); };
	void playSingleTrack(qint32 trackIndex) { callbackQMedia027749_PlaySingleTrack(this, trackIndex); };
	void shuffleAllTracks() { callbackQMedia027749_ShuffleAllTracks(this); };
	void previousTrack() { callbackQMedia027749_PreviousTrack(this); };
	void nextTrack() { callbackQMedia027749_NextTrack(this); };
private:
	QString _currentTrackFilePath;
	qint64 _currentPosition;
	qint64 _currentDuration;
};

Q_DECLARE_METATYPE(QMedia027749*)


void QMedia027749_QMedia027749_QRegisterMetaTypes() {
	qRegisterMetaType<QString>();
}

class qmlBridge027749: public QObject
{
Q_OBJECT
Q_PROPERTY(Navigator027749* navigator READ navigator WRITE setNavigator NOTIFY navigatorChanged)
Q_PROPERTY(QMedia027749* qMedia READ qMedia WRITE setQMedia NOTIFY qMediaChanged)
Q_PROPERTY(QString thumbsPath READ thumbsPath WRITE setThumbsPath NOTIFY thumbsPathChanged)
Q_PROPERTY(QAbstractItemModel* tracksModel READ tracksModel WRITE setTracksModel NOTIFY tracksModelChanged)
Q_PROPERTY(QAbstractItemModel* albumsModel READ albumsModel WRITE setAlbumsModel NOTIFY albumsModelChanged)
Q_PROPERTY(QAbstractItemModel* favouritesModel READ favouritesModel WRITE setFavouritesModel NOTIFY favouritesModelChanged)
public:
	qmlBridge027749(QObject *parent = Q_NULLPTR) : QObject(parent) {qRegisterMetaType<quintptr>("quintptr");qmlBridge027749_qmlBridge027749_QRegisterMetaType();qmlBridge027749_qmlBridge027749_QRegisterMetaTypes();callbackqmlBridge027749_Constructor(this);};
	void Signal_FolderScanComplete(qint32 scanCount) { callbackqmlBridge027749_FolderScanComplete(this, scanCount); };
	void Signal_TrackImported(QString trackTitle) { QByteArray t4e6a51 = trackTitle.toUtf8(); Moc_PackedString trackTitlePacked = { const_cast<char*>(t4e6a51.prepend("WHITESPACE").constData()+10), t4e6a51.size()-10 };callbackqmlBridge027749_TrackImported(this, trackTitlePacked); };
	void Signal_TrackImportComplete() { callbackqmlBridge027749_TrackImportComplete(this); };
	Navigator027749* navigator() { return static_cast<Navigator027749*>(callbackqmlBridge027749_Navigator027749(this)); };
	void setNavigator(Navigator027749* navigator) { callbackqmlBridge027749_SetNavigator(this, navigator); };
	void Signal_NavigatorChanged(Navigator027749* navigator) { callbackqmlBridge027749_NavigatorChanged(this, navigator); };
	QMedia027749* qMedia() { return static_cast<QMedia027749*>(callbackqmlBridge027749_QMedia027749(this)); };
	void setQMedia(QMedia027749* qMedia) { callbackqmlBridge027749_SetQMedia(this, qMedia); };
	void Signal_QMediaChanged(QMedia027749* qMedia) { callbackqmlBridge027749_QMediaChanged(this, qMedia); };
	QString thumbsPath() { return ({ Moc_PackedString tempVal = callbackqmlBridge027749_ThumbsPath(this); QString ret = QString::fromUtf8(tempVal.data, tempVal.len); free(tempVal.data); ret; }); };
	void setThumbsPath(QString thumbsPath) { QByteArray t1a8c6c = thumbsPath.toUtf8(); Moc_PackedString thumbsPathPacked = { const_cast<char*>(t1a8c6c.prepend("WHITESPACE").constData()+10), t1a8c6c.size()-10 };callbackqmlBridge027749_SetThumbsPath(this, thumbsPathPacked); };
	void Signal_ThumbsPathChanged(QString thumbsPath) { QByteArray t1a8c6c = thumbsPath.toUtf8(); Moc_PackedString thumbsPathPacked = { const_cast<char*>(t1a8c6c.prepend("WHITESPACE").constData()+10), t1a8c6c.size()-10 };callbackqmlBridge027749_ThumbsPathChanged(this, thumbsPathPacked); };
	QAbstractItemModel* tracksModel() { return static_cast<QAbstractItemModel*>(callbackqmlBridge027749_TracksModelb23e62(this)); };
	void setTracksModel(QAbstractItemModel* tracksModel) { callbackqmlBridge027749_SetTracksModel(this, tracksModel); };
	void Signal_TracksModelChanged(QAbstractItemModel* tracksModel) { callbackqmlBridge027749_TracksModelChanged(this, tracksModel); };
	QAbstractItemModel* albumsModel() { return static_cast<QAbstractItemModel*>(callbackqmlBridge027749_AlbumsModelb23e62(this)); };
	void setAlbumsModel(QAbstractItemModel* albumsModel) { callbackqmlBridge027749_SetAlbumsModel(this, albumsModel); };
	void Signal_AlbumsModelChanged(QAbstractItemModel* albumsModel) { callbackqmlBridge027749_AlbumsModelChanged(this, albumsModel); };
	QAbstractItemModel* favouritesModel() { return static_cast<QAbstractItemModel*>(callbackqmlBridge027749_FavouritesModelb23e62(this)); };
	void setFavouritesModel(QAbstractItemModel* favouritesModel) { callbackqmlBridge027749_SetFavouritesModel(this, favouritesModel); };
	void Signal_FavouritesModelChanged(QAbstractItemModel* favouritesModel) { callbackqmlBridge027749_FavouritesModelChanged(this, favouritesModel); };
	 ~qmlBridge027749() { callbackqmlBridge027749_DestroyQmlBridge(this); };
	void childEvent(QChildEvent * event) { callbackqmlBridge027749_ChildEvent(this, event); };
	void connectNotify(const QMetaMethod & sign) { callbackqmlBridge027749_ConnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void customEvent(QEvent * event) { callbackqmlBridge027749_CustomEvent(this, event); };
	void deleteLater() { callbackqmlBridge027749_DeleteLater(this); };
	void Signal_Destroyed(QObject * obj) { callbackqmlBridge027749_Destroyed(this, obj); };
	void disconnectNotify(const QMetaMethod & sign) { callbackqmlBridge027749_DisconnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	bool event(QEvent * e) { return callbackqmlBridge027749_Event(this, e) != 0; };
	bool eventFilter(QObject * watched, QEvent * event) { return callbackqmlBridge027749_EventFilter(this, watched, event) != 0; };
	void Signal_ObjectNameChanged(const QString & objectName) { QByteArray taa2c4f = objectName.toUtf8(); Moc_PackedString objectNamePacked = { const_cast<char*>(taa2c4f.prepend("WHITESPACE").constData()+10), taa2c4f.size()-10 };callbackqmlBridge027749_ObjectNameChanged(this, objectNamePacked); };
	void timerEvent(QTimerEvent * event) { callbackqmlBridge027749_TimerEvent(this, event); };
	Navigator027749* navigatorDefault() { return _navigator; };
	void setNavigatorDefault(Navigator027749* p) { if (p != _navigator) { _navigator = p; navigatorChanged(_navigator); } };
	QMedia027749* qMediaDefault() { return _qMedia; };
	void setQMediaDefault(QMedia027749* p) { if (p != _qMedia) { _qMedia = p; qMediaChanged(_qMedia); } };
	QString thumbsPathDefault() { return _thumbsPath; };
	void setThumbsPathDefault(QString p) { if (p != _thumbsPath) { _thumbsPath = p; thumbsPathChanged(_thumbsPath); } };
	QAbstractItemModel* tracksModelDefault() { return _tracksModel; };
	void setTracksModelDefault(QAbstractItemModel* p) { if (p != _tracksModel) { _tracksModel = p; tracksModelChanged(_tracksModel); } };
	QAbstractItemModel* albumsModelDefault() { return _albumsModel; };
	void setAlbumsModelDefault(QAbstractItemModel* p) { if (p != _albumsModel) { _albumsModel = p; albumsModelChanged(_albumsModel); } };
	QAbstractItemModel* favouritesModelDefault() { return _favouritesModel; };
	void setFavouritesModelDefault(QAbstractItemModel* p) { if (p != _favouritesModel) { _favouritesModel = p; favouritesModelChanged(_favouritesModel); } };
signals:
	void folderScanComplete(qint32 scanCount);
	void trackImported(QString trackTitle);
	void trackImportComplete();
	void navigatorChanged(Navigator027749* navigator);
	void qMediaChanged(QMedia027749* qMedia);
	void thumbsPathChanged(QString thumbsPath);
	void tracksModelChanged(QAbstractItemModel* tracksModel);
	void albumsModelChanged(QAbstractItemModel* albumsModel);
	void favouritesModelChanged(QAbstractItemModel* favouritesModel);
public slots:
	void addFolders(QStringList folders) { QByteArray t9fa302 = folders.join("¡¦!").toUtf8(); Moc_PackedString foldersPacked = { const_cast<char*>(t9fa302.prepend("WHITESPACE").constData()+10), t9fa302.size()-10 };callbackqmlBridge027749_AddFolders(this, foldersPacked); };
	void importTracks() { callbackqmlBridge027749_ImportTracks(this); };
	void updateTracksModelData() { callbackqmlBridge027749_UpdateTracksModelData(this); };
	void toggleTrackFavourite(QString filepath) { QByteArray te4afa0 = filepath.toUtf8(); Moc_PackedString filepathPacked = { const_cast<char*>(te4afa0.prepend("WHITESPACE").constData()+10), te4afa0.size()-10 };callbackqmlBridge027749_ToggleTrackFavourite(this, filepathPacked); };
private:
	Navigator027749* _navigator;
	QMedia027749* _qMedia;
	QString _thumbsPath;
	QAbstractItemModel* _tracksModel;
	QAbstractItemModel* _albumsModel;
	QAbstractItemModel* _favouritesModel;
};

Q_DECLARE_METATYPE(qmlBridge027749*)


void qmlBridge027749_qmlBridge027749_QRegisterMetaTypes() {
	qRegisterMetaType<QString>();
	qRegisterMetaType<QAbstractItemModel*>();
}

void Navigator027749_NavigateTo(void* ptr, struct Moc_PackedString view)
{
	QMetaObject::invokeMethod(static_cast<Navigator027749*>(ptr), "navigateTo", Q_ARG(QString, QString::fromUtf8(view.data, view.len)));
}

void Navigator027749_ConnectNavigated(void* ptr)
{
	QObject::connect(static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::navigated), static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::Signal_Navigated));
}

void Navigator027749_DisconnectNavigated(void* ptr)
{
	QObject::disconnect(static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::navigated), static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::Signal_Navigated));
}

void Navigator027749_Navigated(void* ptr, struct Moc_PackedString view)
{
	static_cast<Navigator027749*>(ptr)->navigated(QString::fromUtf8(view.data, view.len));
}

struct Moc_PackedString Navigator027749_CurrentView(void* ptr)
{
	return ({ QByteArray t996400 = static_cast<Navigator027749*>(ptr)->currentView().toUtf8(); Moc_PackedString { const_cast<char*>(t996400.prepend("WHITESPACE").constData()+10), t996400.size()-10 }; });
}

struct Moc_PackedString Navigator027749_CurrentViewDefault(void* ptr)
{
	return ({ QByteArray t378520 = static_cast<Navigator027749*>(ptr)->currentViewDefault().toUtf8(); Moc_PackedString { const_cast<char*>(t378520.prepend("WHITESPACE").constData()+10), t378520.size()-10 }; });
}

void Navigator027749_SetCurrentView(void* ptr, struct Moc_PackedString currentView)
{
	static_cast<Navigator027749*>(ptr)->setCurrentView(QString::fromUtf8(currentView.data, currentView.len));
}

void Navigator027749_SetCurrentViewDefault(void* ptr, struct Moc_PackedString currentView)
{
	static_cast<Navigator027749*>(ptr)->setCurrentViewDefault(QString::fromUtf8(currentView.data, currentView.len));
}

void Navigator027749_ConnectCurrentViewChanged(void* ptr)
{
	QObject::connect(static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::currentViewChanged), static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::Signal_CurrentViewChanged));
}

void Navigator027749_DisconnectCurrentViewChanged(void* ptr)
{
	QObject::disconnect(static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::currentViewChanged), static_cast<Navigator027749*>(ptr), static_cast<void (Navigator027749::*)(QString)>(&Navigator027749::Signal_CurrentViewChanged));
}

void Navigator027749_CurrentViewChanged(void* ptr, struct Moc_PackedString currentView)
{
	static_cast<Navigator027749*>(ptr)->currentViewChanged(QString::fromUtf8(currentView.data, currentView.len));
}

int Navigator027749_Navigator027749_QRegisterMetaType()
{
	return qRegisterMetaType<Navigator027749*>();
}

int Navigator027749_Navigator027749_QRegisterMetaType2(char* typeName)
{
	return qRegisterMetaType<Navigator027749*>(const_cast<const char*>(typeName));
}

int Navigator027749_Navigator027749_QmlRegisterType()
{
#ifdef QT_QML_LIB
	return qmlRegisterType<Navigator027749>();
#else
	return 0;
#endif
}

int Navigator027749_Navigator027749_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName)
{
#ifdef QT_QML_LIB
	return qmlRegisterType<Navigator027749>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName));
#else
	return 0;
#endif
}

void* Navigator027749___children_atList(void* ptr, int i)
{
	return ({QObject * tmp = static_cast<QList<QObject *>*>(ptr)->at(i); if (i == static_cast<QList<QObject *>*>(ptr)->size()-1) { static_cast<QList<QObject *>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void Navigator027749___children_setList(void* ptr, void* i)
{
	static_cast<QList<QObject *>*>(ptr)->append(static_cast<QObject*>(i));
}

void* Navigator027749___children_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject *>();
}

void* Navigator027749___dynamicPropertyNames_atList(void* ptr, int i)
{
	return new QByteArray(({QByteArray tmp = static_cast<QList<QByteArray>*>(ptr)->at(i); if (i == static_cast<QList<QByteArray>*>(ptr)->size()-1) { static_cast<QList<QByteArray>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void Navigator027749___dynamicPropertyNames_setList(void* ptr, void* i)
{
	static_cast<QList<QByteArray>*>(ptr)->append(*static_cast<QByteArray*>(i));
}

void* Navigator027749___dynamicPropertyNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QByteArray>();
}

void* Navigator027749___findChildren_atList(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void Navigator027749___findChildren_setList(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* Navigator027749___findChildren_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* Navigator027749___findChildren_atList3(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void Navigator027749___findChildren_setList3(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* Navigator027749___findChildren_newList3(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* Navigator027749___qFindChildren_atList2(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void Navigator027749___qFindChildren_setList2(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* Navigator027749___qFindChildren_newList2(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* Navigator027749_NewNavigator(void* parent)
{
	if (dynamic_cast<QCameraImageCapture*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QCameraImageCapture*>(parent));
	} else if (dynamic_cast<QGraphicsObject*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QGraphicsObject*>(parent));
	} else if (dynamic_cast<QGraphicsWidget*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QGraphicsWidget*>(parent));
	} else if (dynamic_cast<QLayout*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QLayout*>(parent));
	} else if (dynamic_cast<QMediaPlaylist*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QMediaPlaylist*>(parent));
	} else if (dynamic_cast<QMediaRecorder*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QMediaRecorder*>(parent));
	} else if (dynamic_cast<QOffscreenSurface*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QOffscreenSurface*>(parent));
	} else if (dynamic_cast<QPaintDeviceWindow*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QPaintDeviceWindow*>(parent));
	} else if (dynamic_cast<QPdfWriter*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QPdfWriter*>(parent));
	} else if (dynamic_cast<QQuickItem*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QQuickItem*>(parent));
	} else if (dynamic_cast<QRadioData*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QRadioData*>(parent));
	} else if (dynamic_cast<QWidget*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QWidget*>(parent));
	} else if (dynamic_cast<QWindow*>(static_cast<QObject*>(parent))) {
		return new Navigator027749(static_cast<QWindow*>(parent));
	} else {
		return new Navigator027749(static_cast<QObject*>(parent));
	}
}

void Navigator027749_DestroyNavigator(void* ptr)
{
	static_cast<Navigator027749*>(ptr)->~Navigator027749();
}

void Navigator027749_DestroyNavigatorDefault(void* ptr)
{
	Q_UNUSED(ptr);

}

void Navigator027749_ChildEventDefault(void* ptr, void* event)
{
	static_cast<Navigator027749*>(ptr)->QObject::childEvent(static_cast<QChildEvent*>(event));
}

void Navigator027749_ConnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<Navigator027749*>(ptr)->QObject::connectNotify(*static_cast<QMetaMethod*>(sign));
}

void Navigator027749_CustomEventDefault(void* ptr, void* event)
{
	static_cast<Navigator027749*>(ptr)->QObject::customEvent(static_cast<QEvent*>(event));
}

void Navigator027749_DeleteLaterDefault(void* ptr)
{
	static_cast<Navigator027749*>(ptr)->QObject::deleteLater();
}

void Navigator027749_DisconnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<Navigator027749*>(ptr)->QObject::disconnectNotify(*static_cast<QMetaMethod*>(sign));
}

char Navigator027749_EventDefault(void* ptr, void* e)
{
	return static_cast<Navigator027749*>(ptr)->QObject::event(static_cast<QEvent*>(e));
}

char Navigator027749_EventFilterDefault(void* ptr, void* watched, void* event)
{
	return static_cast<Navigator027749*>(ptr)->QObject::eventFilter(static_cast<QObject*>(watched), static_cast<QEvent*>(event));
}

void Navigator027749_TimerEventDefault(void* ptr, void* event)
{
	static_cast<Navigator027749*>(ptr)->QObject::timerEvent(static_cast<QTimerEvent*>(event));
}

void QMedia027749_TogglePlayback(void* ptr)
{
	QMetaObject::invokeMethod(static_cast<QMedia027749*>(ptr), "togglePlayback");
}

void QMedia027749_Seek(void* ptr, long long position)
{
	QMetaObject::invokeMethod(static_cast<QMedia027749*>(ptr), "seek", Q_ARG(qint64, position));
}

void QMedia027749_SetVolume(void* ptr, int position)
{
	QMetaObject::invokeMethod(static_cast<QMedia027749*>(ptr), "setVolume", Q_ARG(qint32, position));
}

void QMedia027749_PlaySingleTrack(void* ptr, int trackIndex)
{
	QMetaObject::invokeMethod(static_cast<QMedia027749*>(ptr), "playSingleTrack", Q_ARG(qint32, trackIndex));
}

void QMedia027749_ShuffleAllTracks(void* ptr)
{
	QMetaObject::invokeMethod(static_cast<QMedia027749*>(ptr), "shuffleAllTracks");
}

void QMedia027749_PreviousTrack(void* ptr)
{
	QMetaObject::invokeMethod(static_cast<QMedia027749*>(ptr), "previousTrack");
}

void QMedia027749_NextTrack(void* ptr)
{
	QMetaObject::invokeMethod(static_cast<QMedia027749*>(ptr), "nextTrack");
}

void QMedia027749_ConnectPlayerStateChanged(void* ptr)
{
	QObject::connect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint32)>(&QMedia027749::playerStateChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint32)>(&QMedia027749::Signal_PlayerStateChanged));
}

void QMedia027749_DisconnectPlayerStateChanged(void* ptr)
{
	QObject::disconnect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint32)>(&QMedia027749::playerStateChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint32)>(&QMedia027749::Signal_PlayerStateChanged));
}

void QMedia027749_PlayerStateChanged(void* ptr, int state)
{
	static_cast<QMedia027749*>(ptr)->playerStateChanged(state);
}

void QMedia027749_ConnectCurrentMediaChanged(void* ptr)
{
	QObject::connect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString, QString, QString, QString, bool)>(&QMedia027749::currentMediaChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString, QString, QString, QString, bool)>(&QMedia027749::Signal_CurrentMediaChanged));
}

void QMedia027749_DisconnectCurrentMediaChanged(void* ptr)
{
	QObject::disconnect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString, QString, QString, QString, bool)>(&QMedia027749::currentMediaChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString, QString, QString, QString, bool)>(&QMedia027749::Signal_CurrentMediaChanged));
}

void QMedia027749_CurrentMediaChanged(void* ptr, struct Moc_PackedString title, struct Moc_PackedString artist, struct Moc_PackedString picture, struct Moc_PackedString filePath, char favourite)
{
	static_cast<QMedia027749*>(ptr)->currentMediaChanged(QString::fromUtf8(title.data, title.len), QString::fromUtf8(artist.data, artist.len), QString::fromUtf8(picture.data, picture.len), QString::fromUtf8(filePath.data, filePath.len), favourite != 0);
}

struct Moc_PackedString QMedia027749_CurrentTrackFilePath(void* ptr)
{
	return ({ QByteArray tdfb8c0 = static_cast<QMedia027749*>(ptr)->currentTrackFilePath().toUtf8(); Moc_PackedString { const_cast<char*>(tdfb8c0.prepend("WHITESPACE").constData()+10), tdfb8c0.size()-10 }; });
}

struct Moc_PackedString QMedia027749_CurrentTrackFilePathDefault(void* ptr)
{
	return ({ QByteArray tac6284 = static_cast<QMedia027749*>(ptr)->currentTrackFilePathDefault().toUtf8(); Moc_PackedString { const_cast<char*>(tac6284.prepend("WHITESPACE").constData()+10), tac6284.size()-10 }; });
}

void QMedia027749_SetCurrentTrackFilePath(void* ptr, struct Moc_PackedString currentTrackFilePath)
{
	static_cast<QMedia027749*>(ptr)->setCurrentTrackFilePath(QString::fromUtf8(currentTrackFilePath.data, currentTrackFilePath.len));
}

void QMedia027749_SetCurrentTrackFilePathDefault(void* ptr, struct Moc_PackedString currentTrackFilePath)
{
	static_cast<QMedia027749*>(ptr)->setCurrentTrackFilePathDefault(QString::fromUtf8(currentTrackFilePath.data, currentTrackFilePath.len));
}

void QMedia027749_ConnectCurrentTrackFilePathChanged(void* ptr)
{
	QObject::connect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString)>(&QMedia027749::currentTrackFilePathChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString)>(&QMedia027749::Signal_CurrentTrackFilePathChanged));
}

void QMedia027749_DisconnectCurrentTrackFilePathChanged(void* ptr)
{
	QObject::disconnect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString)>(&QMedia027749::currentTrackFilePathChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(QString)>(&QMedia027749::Signal_CurrentTrackFilePathChanged));
}

void QMedia027749_CurrentTrackFilePathChanged(void* ptr, struct Moc_PackedString currentTrackFilePath)
{
	static_cast<QMedia027749*>(ptr)->currentTrackFilePathChanged(QString::fromUtf8(currentTrackFilePath.data, currentTrackFilePath.len));
}

long long QMedia027749_CurrentPosition(void* ptr)
{
	return static_cast<QMedia027749*>(ptr)->currentPosition();
}

long long QMedia027749_CurrentPositionDefault(void* ptr)
{
	return static_cast<QMedia027749*>(ptr)->currentPositionDefault();
}

void QMedia027749_SetCurrentPosition(void* ptr, long long currentPosition)
{
	static_cast<QMedia027749*>(ptr)->setCurrentPosition(currentPosition);
}

void QMedia027749_SetCurrentPositionDefault(void* ptr, long long currentPosition)
{
	static_cast<QMedia027749*>(ptr)->setCurrentPositionDefault(currentPosition);
}

void QMedia027749_ConnectCurrentPositionChanged(void* ptr)
{
	QObject::connect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::currentPositionChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::Signal_CurrentPositionChanged));
}

void QMedia027749_DisconnectCurrentPositionChanged(void* ptr)
{
	QObject::disconnect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::currentPositionChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::Signal_CurrentPositionChanged));
}

void QMedia027749_CurrentPositionChanged(void* ptr, long long currentPosition)
{
	static_cast<QMedia027749*>(ptr)->currentPositionChanged(currentPosition);
}

long long QMedia027749_CurrentDuration(void* ptr)
{
	return static_cast<QMedia027749*>(ptr)->currentDuration();
}

long long QMedia027749_CurrentDurationDefault(void* ptr)
{
	return static_cast<QMedia027749*>(ptr)->currentDurationDefault();
}

void QMedia027749_SetCurrentDuration(void* ptr, long long currentDuration)
{
	static_cast<QMedia027749*>(ptr)->setCurrentDuration(currentDuration);
}

void QMedia027749_SetCurrentDurationDefault(void* ptr, long long currentDuration)
{
	static_cast<QMedia027749*>(ptr)->setCurrentDurationDefault(currentDuration);
}

void QMedia027749_ConnectCurrentDurationChanged(void* ptr)
{
	QObject::connect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::currentDurationChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::Signal_CurrentDurationChanged));
}

void QMedia027749_DisconnectCurrentDurationChanged(void* ptr)
{
	QObject::disconnect(static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::currentDurationChanged), static_cast<QMedia027749*>(ptr), static_cast<void (QMedia027749::*)(qint64)>(&QMedia027749::Signal_CurrentDurationChanged));
}

void QMedia027749_CurrentDurationChanged(void* ptr, long long currentDuration)
{
	static_cast<QMedia027749*>(ptr)->currentDurationChanged(currentDuration);
}

int QMedia027749_QMedia027749_QRegisterMetaType()
{
	return qRegisterMetaType<QMedia027749*>();
}

int QMedia027749_QMedia027749_QRegisterMetaType2(char* typeName)
{
	return qRegisterMetaType<QMedia027749*>(const_cast<const char*>(typeName));
}

int QMedia027749_QMedia027749_QmlRegisterType()
{
#ifdef QT_QML_LIB
	return qmlRegisterType<QMedia027749>();
#else
	return 0;
#endif
}

int QMedia027749_QMedia027749_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName)
{
#ifdef QT_QML_LIB
	return qmlRegisterType<QMedia027749>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName));
#else
	return 0;
#endif
}

void* QMedia027749___children_atList(void* ptr, int i)
{
	return ({QObject * tmp = static_cast<QList<QObject *>*>(ptr)->at(i); if (i == static_cast<QList<QObject *>*>(ptr)->size()-1) { static_cast<QList<QObject *>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QMedia027749___children_setList(void* ptr, void* i)
{
	static_cast<QList<QObject *>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QMedia027749___children_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject *>();
}

void* QMedia027749___dynamicPropertyNames_atList(void* ptr, int i)
{
	return new QByteArray(({QByteArray tmp = static_cast<QList<QByteArray>*>(ptr)->at(i); if (i == static_cast<QList<QByteArray>*>(ptr)->size()-1) { static_cast<QList<QByteArray>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void QMedia027749___dynamicPropertyNames_setList(void* ptr, void* i)
{
	static_cast<QList<QByteArray>*>(ptr)->append(*static_cast<QByteArray*>(i));
}

void* QMedia027749___dynamicPropertyNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QByteArray>();
}

void* QMedia027749___findChildren_atList(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QMedia027749___findChildren_setList(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QMedia027749___findChildren_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* QMedia027749___findChildren_atList3(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QMedia027749___findChildren_setList3(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QMedia027749___findChildren_newList3(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* QMedia027749___qFindChildren_atList2(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QMedia027749___qFindChildren_setList2(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QMedia027749___qFindChildren_newList2(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* QMedia027749_NewQMedia(void* parent)
{
	if (dynamic_cast<QCameraImageCapture*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QCameraImageCapture*>(parent));
	} else if (dynamic_cast<QGraphicsObject*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QGraphicsObject*>(parent));
	} else if (dynamic_cast<QGraphicsWidget*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QGraphicsWidget*>(parent));
	} else if (dynamic_cast<QLayout*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QLayout*>(parent));
	} else if (dynamic_cast<QMediaPlaylist*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QMediaPlaylist*>(parent));
	} else if (dynamic_cast<QMediaRecorder*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QMediaRecorder*>(parent));
	} else if (dynamic_cast<QOffscreenSurface*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QOffscreenSurface*>(parent));
	} else if (dynamic_cast<QPaintDeviceWindow*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QPaintDeviceWindow*>(parent));
	} else if (dynamic_cast<QPdfWriter*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QPdfWriter*>(parent));
	} else if (dynamic_cast<QQuickItem*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QQuickItem*>(parent));
	} else if (dynamic_cast<QRadioData*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QRadioData*>(parent));
	} else if (dynamic_cast<QWidget*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QWidget*>(parent));
	} else if (dynamic_cast<QWindow*>(static_cast<QObject*>(parent))) {
		return new QMedia027749(static_cast<QWindow*>(parent));
	} else {
		return new QMedia027749(static_cast<QObject*>(parent));
	}
}

void QMedia027749_DestroyQMedia(void* ptr)
{
	static_cast<QMedia027749*>(ptr)->~QMedia027749();
}

void QMedia027749_DestroyQMediaDefault(void* ptr)
{
	Q_UNUSED(ptr);

}

void QMedia027749_ChildEventDefault(void* ptr, void* event)
{
	static_cast<QMedia027749*>(ptr)->QObject::childEvent(static_cast<QChildEvent*>(event));
}

void QMedia027749_ConnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<QMedia027749*>(ptr)->QObject::connectNotify(*static_cast<QMetaMethod*>(sign));
}

void QMedia027749_CustomEventDefault(void* ptr, void* event)
{
	static_cast<QMedia027749*>(ptr)->QObject::customEvent(static_cast<QEvent*>(event));
}

void QMedia027749_DeleteLaterDefault(void* ptr)
{
	static_cast<QMedia027749*>(ptr)->QObject::deleteLater();
}

void QMedia027749_DisconnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<QMedia027749*>(ptr)->QObject::disconnectNotify(*static_cast<QMetaMethod*>(sign));
}

char QMedia027749_EventDefault(void* ptr, void* e)
{
	return static_cast<QMedia027749*>(ptr)->QObject::event(static_cast<QEvent*>(e));
}

char QMedia027749_EventFilterDefault(void* ptr, void* watched, void* event)
{
	return static_cast<QMedia027749*>(ptr)->QObject::eventFilter(static_cast<QObject*>(watched), static_cast<QEvent*>(event));
}

void QMedia027749_TimerEventDefault(void* ptr, void* event)
{
	static_cast<QMedia027749*>(ptr)->QObject::timerEvent(static_cast<QTimerEvent*>(event));
}

void qmlBridge027749_AddFolders(void* ptr, struct Moc_PackedString folders)
{
	QMetaObject::invokeMethod(static_cast<qmlBridge027749*>(ptr), "addFolders", Q_ARG(QStringList, QString::fromUtf8(folders.data, folders.len).split("¡¦!", QString::SkipEmptyParts)));
}

void qmlBridge027749_ImportTracks(void* ptr)
{
	QMetaObject::invokeMethod(static_cast<qmlBridge027749*>(ptr), "importTracks");
}

void qmlBridge027749_UpdateTracksModelData(void* ptr)
{
	QMetaObject::invokeMethod(static_cast<qmlBridge027749*>(ptr), "updateTracksModelData");
}

void qmlBridge027749_ToggleTrackFavourite(void* ptr, struct Moc_PackedString filepath)
{
	QMetaObject::invokeMethod(static_cast<qmlBridge027749*>(ptr), "toggleTrackFavourite", Q_ARG(QString, QString::fromUtf8(filepath.data, filepath.len)));
}

void qmlBridge027749_ConnectFolderScanComplete(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(qint32)>(&qmlBridge027749::folderScanComplete), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(qint32)>(&qmlBridge027749::Signal_FolderScanComplete));
}

void qmlBridge027749_DisconnectFolderScanComplete(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(qint32)>(&qmlBridge027749::folderScanComplete), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(qint32)>(&qmlBridge027749::Signal_FolderScanComplete));
}

void qmlBridge027749_FolderScanComplete(void* ptr, int scanCount)
{
	static_cast<qmlBridge027749*>(ptr)->folderScanComplete(scanCount);
}

void qmlBridge027749_ConnectTrackImported(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::trackImported), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::Signal_TrackImported));
}

void qmlBridge027749_DisconnectTrackImported(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::trackImported), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::Signal_TrackImported));
}

void qmlBridge027749_TrackImported(void* ptr, struct Moc_PackedString trackTitle)
{
	static_cast<qmlBridge027749*>(ptr)->trackImported(QString::fromUtf8(trackTitle.data, trackTitle.len));
}

void qmlBridge027749_ConnectTrackImportComplete(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)()>(&qmlBridge027749::trackImportComplete), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)()>(&qmlBridge027749::Signal_TrackImportComplete));
}

void qmlBridge027749_DisconnectTrackImportComplete(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)()>(&qmlBridge027749::trackImportComplete), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)()>(&qmlBridge027749::Signal_TrackImportComplete));
}

void qmlBridge027749_TrackImportComplete(void* ptr)
{
	static_cast<qmlBridge027749*>(ptr)->trackImportComplete();
}

void* qmlBridge027749_Navigator027749(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->navigator();
}

void* qmlBridge027749_NavigatorDefault(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->navigatorDefault();
}

void qmlBridge027749_SetNavigator(void* ptr, void* navigator)
{
	static_cast<qmlBridge027749*>(ptr)->setNavigator(static_cast<Navigator027749*>(navigator));
}

void qmlBridge027749_SetNavigatorDefault(void* ptr, void* navigator)
{
	static_cast<qmlBridge027749*>(ptr)->setNavigatorDefault(static_cast<Navigator027749*>(navigator));
}

void qmlBridge027749_ConnectNavigatorChanged(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(Navigator027749*)>(&qmlBridge027749::navigatorChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(Navigator027749*)>(&qmlBridge027749::Signal_NavigatorChanged));
}

void qmlBridge027749_DisconnectNavigatorChanged(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(Navigator027749*)>(&qmlBridge027749::navigatorChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(Navigator027749*)>(&qmlBridge027749::Signal_NavigatorChanged));
}

void qmlBridge027749_NavigatorChanged(void* ptr, void* navigator)
{
	static_cast<qmlBridge027749*>(ptr)->navigatorChanged(static_cast<Navigator027749*>(navigator));
}

void* qmlBridge027749_QMedia027749(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->qMedia();
}

void* qmlBridge027749_QMediaDefault(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->qMediaDefault();
}

void qmlBridge027749_SetQMedia(void* ptr, void* qMedia)
{
	static_cast<qmlBridge027749*>(ptr)->setQMedia(static_cast<QMedia027749*>(qMedia));
}

void qmlBridge027749_SetQMediaDefault(void* ptr, void* qMedia)
{
	static_cast<qmlBridge027749*>(ptr)->setQMediaDefault(static_cast<QMedia027749*>(qMedia));
}

void qmlBridge027749_ConnectQMediaChanged(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QMedia027749*)>(&qmlBridge027749::qMediaChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QMedia027749*)>(&qmlBridge027749::Signal_QMediaChanged));
}

void qmlBridge027749_DisconnectQMediaChanged(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QMedia027749*)>(&qmlBridge027749::qMediaChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QMedia027749*)>(&qmlBridge027749::Signal_QMediaChanged));
}

void qmlBridge027749_QMediaChanged(void* ptr, void* qMedia)
{
	static_cast<qmlBridge027749*>(ptr)->qMediaChanged(static_cast<QMedia027749*>(qMedia));
}

struct Moc_PackedString qmlBridge027749_ThumbsPath(void* ptr)
{
	return ({ QByteArray t82d65e = static_cast<qmlBridge027749*>(ptr)->thumbsPath().toUtf8(); Moc_PackedString { const_cast<char*>(t82d65e.prepend("WHITESPACE").constData()+10), t82d65e.size()-10 }; });
}

struct Moc_PackedString qmlBridge027749_ThumbsPathDefault(void* ptr)
{
	return ({ QByteArray t15dfc5 = static_cast<qmlBridge027749*>(ptr)->thumbsPathDefault().toUtf8(); Moc_PackedString { const_cast<char*>(t15dfc5.prepend("WHITESPACE").constData()+10), t15dfc5.size()-10 }; });
}

void qmlBridge027749_SetThumbsPath(void* ptr, struct Moc_PackedString thumbsPath)
{
	static_cast<qmlBridge027749*>(ptr)->setThumbsPath(QString::fromUtf8(thumbsPath.data, thumbsPath.len));
}

void qmlBridge027749_SetThumbsPathDefault(void* ptr, struct Moc_PackedString thumbsPath)
{
	static_cast<qmlBridge027749*>(ptr)->setThumbsPathDefault(QString::fromUtf8(thumbsPath.data, thumbsPath.len));
}

void qmlBridge027749_ConnectThumbsPathChanged(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::thumbsPathChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::Signal_ThumbsPathChanged));
}

void qmlBridge027749_DisconnectThumbsPathChanged(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::thumbsPathChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QString)>(&qmlBridge027749::Signal_ThumbsPathChanged));
}

void qmlBridge027749_ThumbsPathChanged(void* ptr, struct Moc_PackedString thumbsPath)
{
	static_cast<qmlBridge027749*>(ptr)->thumbsPathChanged(QString::fromUtf8(thumbsPath.data, thumbsPath.len));
}

void* qmlBridge027749_TracksModelb23e62(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->tracksModel();
}

void* qmlBridge027749_TracksModelDefault(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->tracksModelDefault();
}

void qmlBridge027749_SetTracksModel(void* ptr, void* tracksModel)
{
	static_cast<qmlBridge027749*>(ptr)->setTracksModel(static_cast<QAbstractItemModel*>(tracksModel));
}

void qmlBridge027749_SetTracksModelDefault(void* ptr, void* tracksModel)
{
	static_cast<qmlBridge027749*>(ptr)->setTracksModelDefault(static_cast<QAbstractItemModel*>(tracksModel));
}

void qmlBridge027749_ConnectTracksModelChanged(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::tracksModelChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::Signal_TracksModelChanged));
}

void qmlBridge027749_DisconnectTracksModelChanged(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::tracksModelChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::Signal_TracksModelChanged));
}

void qmlBridge027749_TracksModelChanged(void* ptr, void* tracksModel)
{
	static_cast<qmlBridge027749*>(ptr)->tracksModelChanged(static_cast<QAbstractItemModel*>(tracksModel));
}

void* qmlBridge027749_AlbumsModelb23e62(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->albumsModel();
}

void* qmlBridge027749_AlbumsModelDefault(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->albumsModelDefault();
}

void qmlBridge027749_SetAlbumsModel(void* ptr, void* albumsModel)
{
	static_cast<qmlBridge027749*>(ptr)->setAlbumsModel(static_cast<QAbstractItemModel*>(albumsModel));
}

void qmlBridge027749_SetAlbumsModelDefault(void* ptr, void* albumsModel)
{
	static_cast<qmlBridge027749*>(ptr)->setAlbumsModelDefault(static_cast<QAbstractItemModel*>(albumsModel));
}

void qmlBridge027749_ConnectAlbumsModelChanged(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::albumsModelChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::Signal_AlbumsModelChanged));
}

void qmlBridge027749_DisconnectAlbumsModelChanged(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::albumsModelChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::Signal_AlbumsModelChanged));
}

void qmlBridge027749_AlbumsModelChanged(void* ptr, void* albumsModel)
{
	static_cast<qmlBridge027749*>(ptr)->albumsModelChanged(static_cast<QAbstractItemModel*>(albumsModel));
}

void* qmlBridge027749_FavouritesModelb23e62(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->favouritesModel();
}

void* qmlBridge027749_FavouritesModelDefault(void* ptr)
{
	return static_cast<qmlBridge027749*>(ptr)->favouritesModelDefault();
}

void qmlBridge027749_SetFavouritesModel(void* ptr, void* favouritesModel)
{
	static_cast<qmlBridge027749*>(ptr)->setFavouritesModel(static_cast<QAbstractItemModel*>(favouritesModel));
}

void qmlBridge027749_SetFavouritesModelDefault(void* ptr, void* favouritesModel)
{
	static_cast<qmlBridge027749*>(ptr)->setFavouritesModelDefault(static_cast<QAbstractItemModel*>(favouritesModel));
}

void qmlBridge027749_ConnectFavouritesModelChanged(void* ptr)
{
	QObject::connect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::favouritesModelChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::Signal_FavouritesModelChanged));
}

void qmlBridge027749_DisconnectFavouritesModelChanged(void* ptr)
{
	QObject::disconnect(static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::favouritesModelChanged), static_cast<qmlBridge027749*>(ptr), static_cast<void (qmlBridge027749::*)(QAbstractItemModel*)>(&qmlBridge027749::Signal_FavouritesModelChanged));
}

void qmlBridge027749_FavouritesModelChanged(void* ptr, void* favouritesModel)
{
	static_cast<qmlBridge027749*>(ptr)->favouritesModelChanged(static_cast<QAbstractItemModel*>(favouritesModel));
}

int qmlBridge027749_qmlBridge027749_QRegisterMetaType()
{
	return qRegisterMetaType<qmlBridge027749*>();
}

int qmlBridge027749_qmlBridge027749_QRegisterMetaType2(char* typeName)
{
	return qRegisterMetaType<qmlBridge027749*>(const_cast<const char*>(typeName));
}

int qmlBridge027749_qmlBridge027749_QmlRegisterType()
{
#ifdef QT_QML_LIB
	return qmlRegisterType<qmlBridge027749>();
#else
	return 0;
#endif
}

int qmlBridge027749_qmlBridge027749_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName)
{
#ifdef QT_QML_LIB
	return qmlRegisterType<qmlBridge027749>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName));
#else
	return 0;
#endif
}

void* qmlBridge027749___children_atList(void* ptr, int i)
{
	return ({QObject * tmp = static_cast<QList<QObject *>*>(ptr)->at(i); if (i == static_cast<QList<QObject *>*>(ptr)->size()-1) { static_cast<QList<QObject *>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void qmlBridge027749___children_setList(void* ptr, void* i)
{
	static_cast<QList<QObject *>*>(ptr)->append(static_cast<QObject*>(i));
}

void* qmlBridge027749___children_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject *>();
}

void* qmlBridge027749___dynamicPropertyNames_atList(void* ptr, int i)
{
	return new QByteArray(({QByteArray tmp = static_cast<QList<QByteArray>*>(ptr)->at(i); if (i == static_cast<QList<QByteArray>*>(ptr)->size()-1) { static_cast<QList<QByteArray>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void qmlBridge027749___dynamicPropertyNames_setList(void* ptr, void* i)
{
	static_cast<QList<QByteArray>*>(ptr)->append(*static_cast<QByteArray*>(i));
}

void* qmlBridge027749___dynamicPropertyNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QByteArray>();
}

void* qmlBridge027749___findChildren_atList(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void qmlBridge027749___findChildren_setList(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* qmlBridge027749___findChildren_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* qmlBridge027749___findChildren_atList3(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void qmlBridge027749___findChildren_setList3(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* qmlBridge027749___findChildren_newList3(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* qmlBridge027749___qFindChildren_atList2(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void qmlBridge027749___qFindChildren_setList2(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* qmlBridge027749___qFindChildren_newList2(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* qmlBridge027749_NewQmlBridge(void* parent)
{
	if (dynamic_cast<QCameraImageCapture*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QCameraImageCapture*>(parent));
	} else if (dynamic_cast<QGraphicsObject*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QGraphicsObject*>(parent));
	} else if (dynamic_cast<QGraphicsWidget*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QGraphicsWidget*>(parent));
	} else if (dynamic_cast<QLayout*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QLayout*>(parent));
	} else if (dynamic_cast<QMediaPlaylist*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QMediaPlaylist*>(parent));
	} else if (dynamic_cast<QMediaRecorder*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QMediaRecorder*>(parent));
	} else if (dynamic_cast<QOffscreenSurface*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QOffscreenSurface*>(parent));
	} else if (dynamic_cast<QPaintDeviceWindow*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QPaintDeviceWindow*>(parent));
	} else if (dynamic_cast<QPdfWriter*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QPdfWriter*>(parent));
	} else if (dynamic_cast<QQuickItem*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QQuickItem*>(parent));
	} else if (dynamic_cast<QRadioData*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QRadioData*>(parent));
	} else if (dynamic_cast<QWidget*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QWidget*>(parent));
	} else if (dynamic_cast<QWindow*>(static_cast<QObject*>(parent))) {
		return new qmlBridge027749(static_cast<QWindow*>(parent));
	} else {
		return new qmlBridge027749(static_cast<QObject*>(parent));
	}
}

void qmlBridge027749_DestroyQmlBridge(void* ptr)
{
	static_cast<qmlBridge027749*>(ptr)->~qmlBridge027749();
}

void qmlBridge027749_DestroyQmlBridgeDefault(void* ptr)
{
	Q_UNUSED(ptr);

}

void qmlBridge027749_ChildEventDefault(void* ptr, void* event)
{
	static_cast<qmlBridge027749*>(ptr)->QObject::childEvent(static_cast<QChildEvent*>(event));
}

void qmlBridge027749_ConnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<qmlBridge027749*>(ptr)->QObject::connectNotify(*static_cast<QMetaMethod*>(sign));
}

void qmlBridge027749_CustomEventDefault(void* ptr, void* event)
{
	static_cast<qmlBridge027749*>(ptr)->QObject::customEvent(static_cast<QEvent*>(event));
}

void qmlBridge027749_DeleteLaterDefault(void* ptr)
{
	static_cast<qmlBridge027749*>(ptr)->QObject::deleteLater();
}

void qmlBridge027749_DisconnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<qmlBridge027749*>(ptr)->QObject::disconnectNotify(*static_cast<QMetaMethod*>(sign));
}

char qmlBridge027749_EventDefault(void* ptr, void* e)
{
	return static_cast<qmlBridge027749*>(ptr)->QObject::event(static_cast<QEvent*>(e));
}

char qmlBridge027749_EventFilterDefault(void* ptr, void* watched, void* event)
{
	return static_cast<qmlBridge027749*>(ptr)->QObject::eventFilter(static_cast<QObject*>(watched), static_cast<QEvent*>(event));
}

void qmlBridge027749_TimerEventDefault(void* ptr, void* event)
{
	static_cast<qmlBridge027749*>(ptr)->QObject::timerEvent(static_cast<QTimerEvent*>(event));
}

#include "moc_moc.h"
