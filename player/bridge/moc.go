package bridge

//#include <stdint.h>
//#include <stdlib.h>
//#include <string.h>
//#include "moc.h"
import "C"
import (
	"runtime"
	"strings"
	"unsafe"

	"github.com/therecipe/qt"
	std_core "github.com/therecipe/qt/core"
)

func cGoUnpackString(s C.struct_Moc_PackedString) string {
	if int(s.len) == -1 {
		return C.GoString(s.data)
	}
	return C.GoStringN(s.data, C.int(s.len))
}
func cGoUnpackBytes(s C.struct_Moc_PackedString) []byte {
	if int(s.len) == -1 {
		gs := C.GoString(s.data)
		return *(*[]byte)(unsafe.Pointer(&gs))
	}
	return C.GoBytes(unsafe.Pointer(s.data), C.int(s.len))
}
func unpackStringList(s string) []string {
	if len(s) == 0 {
		return make([]string, 0)
	}
	return strings.Split(s, "¡¦!")
}

type Navigator_ITF interface {
	std_core.QObject_ITF
	Navigator_PTR() *Navigator
}

func (ptr *Navigator) Navigator_PTR() *Navigator {
	return ptr
}

func (ptr *Navigator) Pointer() unsafe.Pointer {
	if ptr != nil {
		return ptr.QObject_PTR().Pointer()
	}
	return nil
}

func (ptr *Navigator) SetPointer(p unsafe.Pointer) {
	if ptr != nil {
		ptr.QObject_PTR().SetPointer(p)
	}
}

func PointerFromNavigator(ptr Navigator_ITF) unsafe.Pointer {
	if ptr != nil {
		return ptr.Navigator_PTR().Pointer()
	}
	return nil
}

func NewNavigatorFromPointer(ptr unsafe.Pointer) (n *Navigator) {
	if gPtr, ok := qt.Receive(ptr); !ok {
		n = new(Navigator)
		n.SetPointer(ptr)
	} else {
		switch deduced := gPtr.(type) {
		case *Navigator:
			n = deduced

		case *std_core.QObject:
			n = &Navigator{QObject: *deduced}

		default:
			n = new(Navigator)
			n.SetPointer(ptr)
		}
	}
	return
}

//export callbackNavigator027749_Constructor
func callbackNavigator027749_Constructor(ptr unsafe.Pointer) {
	this := NewNavigatorFromPointer(ptr)
	qt.Register(ptr, this)
	this.ConnectNavigateTo(this.navigateTo)
}

//export callbackNavigator027749_NavigateTo
func callbackNavigator027749_NavigateTo(ptr unsafe.Pointer, view C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "navigateTo"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(view))
	}

}

func (ptr *Navigator) ConnectNavigateTo(f func(view string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "navigateTo"); signal != nil {
			f := func(view string) {
				(*(*func(string))(signal))(view)
				f(view)
			}
			qt.ConnectSignal(ptr.Pointer(), "navigateTo", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "navigateTo", unsafe.Pointer(&f))
		}
	}
}

func (ptr *Navigator) DisconnectNavigateTo() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "navigateTo")
	}
}

func (ptr *Navigator) NavigateTo(view string) {
	if ptr.Pointer() != nil {
		var viewC *C.char
		if view != "" {
			viewC = C.CString(view)
			defer C.free(unsafe.Pointer(viewC))
		}
		C.Navigator027749_NavigateTo(ptr.Pointer(), C.struct_Moc_PackedString{data: viewC, len: C.longlong(len(view))})
	}
}

//export callbackNavigator027749_Navigated
func callbackNavigator027749_Navigated(ptr unsafe.Pointer, view C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "navigated"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(view))
	}

}

func (ptr *Navigator) ConnectNavigated(f func(view string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "navigated") {
			C.Navigator027749_ConnectNavigated(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "navigated"); signal != nil {
			f := func(view string) {
				(*(*func(string))(signal))(view)
				f(view)
			}
			qt.ConnectSignal(ptr.Pointer(), "navigated", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "navigated", unsafe.Pointer(&f))
		}
	}
}

func (ptr *Navigator) DisconnectNavigated() {
	if ptr.Pointer() != nil {
		C.Navigator027749_DisconnectNavigated(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "navigated")
	}
}

func (ptr *Navigator) Navigated(view string) {
	if ptr.Pointer() != nil {
		var viewC *C.char
		if view != "" {
			viewC = C.CString(view)
			defer C.free(unsafe.Pointer(viewC))
		}
		C.Navigator027749_Navigated(ptr.Pointer(), C.struct_Moc_PackedString{data: viewC, len: C.longlong(len(view))})
	}
}

//export callbackNavigator027749_CurrentView
func callbackNavigator027749_CurrentView(ptr unsafe.Pointer) C.struct_Moc_PackedString {
	if signal := qt.GetSignal(ptr, "currentView"); signal != nil {
		tempVal := (*(*func() string)(signal))()
		return C.struct_Moc_PackedString{data: C.CString(tempVal), len: C.longlong(len(tempVal))}
	}
	tempVal := NewNavigatorFromPointer(ptr).CurrentViewDefault()
	return C.struct_Moc_PackedString{data: C.CString(tempVal), len: C.longlong(len(tempVal))}
}

func (ptr *Navigator) ConnectCurrentView(f func() string) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "currentView"); signal != nil {
			f := func() string {
				(*(*func() string)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "currentView", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentView", unsafe.Pointer(&f))
		}
	}
}

func (ptr *Navigator) DisconnectCurrentView() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "currentView")
	}
}

func (ptr *Navigator) CurrentView() string {
	if ptr.Pointer() != nil {
		return cGoUnpackString(C.Navigator027749_CurrentView(ptr.Pointer()))
	}
	return ""
}

func (ptr *Navigator) CurrentViewDefault() string {
	if ptr.Pointer() != nil {
		return cGoUnpackString(C.Navigator027749_CurrentViewDefault(ptr.Pointer()))
	}
	return ""
}

//export callbackNavigator027749_SetCurrentView
func callbackNavigator027749_SetCurrentView(ptr unsafe.Pointer, currentView C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "setCurrentView"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(currentView))
	} else {
		NewNavigatorFromPointer(ptr).SetCurrentViewDefault(cGoUnpackString(currentView))
	}
}

func (ptr *Navigator) ConnectSetCurrentView(f func(currentView string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setCurrentView"); signal != nil {
			f := func(currentView string) {
				(*(*func(string))(signal))(currentView)
				f(currentView)
			}
			qt.ConnectSignal(ptr.Pointer(), "setCurrentView", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setCurrentView", unsafe.Pointer(&f))
		}
	}
}

func (ptr *Navigator) DisconnectSetCurrentView() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setCurrentView")
	}
}

func (ptr *Navigator) SetCurrentView(currentView string) {
	if ptr.Pointer() != nil {
		var currentViewC *C.char
		if currentView != "" {
			currentViewC = C.CString(currentView)
			defer C.free(unsafe.Pointer(currentViewC))
		}
		C.Navigator027749_SetCurrentView(ptr.Pointer(), C.struct_Moc_PackedString{data: currentViewC, len: C.longlong(len(currentView))})
	}
}

func (ptr *Navigator) SetCurrentViewDefault(currentView string) {
	if ptr.Pointer() != nil {
		var currentViewC *C.char
		if currentView != "" {
			currentViewC = C.CString(currentView)
			defer C.free(unsafe.Pointer(currentViewC))
		}
		C.Navigator027749_SetCurrentViewDefault(ptr.Pointer(), C.struct_Moc_PackedString{data: currentViewC, len: C.longlong(len(currentView))})
	}
}

//export callbackNavigator027749_CurrentViewChanged
func callbackNavigator027749_CurrentViewChanged(ptr unsafe.Pointer, currentView C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "currentViewChanged"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(currentView))
	}

}

func (ptr *Navigator) ConnectCurrentViewChanged(f func(currentView string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "currentViewChanged") {
			C.Navigator027749_ConnectCurrentViewChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "currentViewChanged"); signal != nil {
			f := func(currentView string) {
				(*(*func(string))(signal))(currentView)
				f(currentView)
			}
			qt.ConnectSignal(ptr.Pointer(), "currentViewChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentViewChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *Navigator) DisconnectCurrentViewChanged() {
	if ptr.Pointer() != nil {
		C.Navigator027749_DisconnectCurrentViewChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "currentViewChanged")
	}
}

func (ptr *Navigator) CurrentViewChanged(currentView string) {
	if ptr.Pointer() != nil {
		var currentViewC *C.char
		if currentView != "" {
			currentViewC = C.CString(currentView)
			defer C.free(unsafe.Pointer(currentViewC))
		}
		C.Navigator027749_CurrentViewChanged(ptr.Pointer(), C.struct_Moc_PackedString{data: currentViewC, len: C.longlong(len(currentView))})
	}
}

func Navigator_QRegisterMetaType() int {
	return int(int32(C.Navigator027749_Navigator027749_QRegisterMetaType()))
}

func (ptr *Navigator) QRegisterMetaType() int {
	return int(int32(C.Navigator027749_Navigator027749_QRegisterMetaType()))
}

func Navigator_QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.Navigator027749_Navigator027749_QRegisterMetaType2(typeNameC)))
}

func (ptr *Navigator) QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.Navigator027749_Navigator027749_QRegisterMetaType2(typeNameC)))
}

func Navigator_QmlRegisterType() int {
	return int(int32(C.Navigator027749_Navigator027749_QmlRegisterType()))
}

func (ptr *Navigator) QmlRegisterType() int {
	return int(int32(C.Navigator027749_Navigator027749_QmlRegisterType()))
}

func Navigator_QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.Navigator027749_Navigator027749_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *Navigator) QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.Navigator027749_Navigator027749_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *Navigator) __children_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.Navigator027749___children_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *Navigator) __children_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749___children_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *Navigator) __children_newList() unsafe.Pointer {
	return C.Navigator027749___children_newList(ptr.Pointer())
}

func (ptr *Navigator) __dynamicPropertyNames_atList(i int) *std_core.QByteArray {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQByteArrayFromPointer(C.Navigator027749___dynamicPropertyNames_atList(ptr.Pointer(), C.int(int32(i))))
		runtime.SetFinalizer(tmpValue, (*std_core.QByteArray).DestroyQByteArray)
		return tmpValue
	}
	return nil
}

func (ptr *Navigator) __dynamicPropertyNames_setList(i std_core.QByteArray_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749___dynamicPropertyNames_setList(ptr.Pointer(), std_core.PointerFromQByteArray(i))
	}
}

func (ptr *Navigator) __dynamicPropertyNames_newList() unsafe.Pointer {
	return C.Navigator027749___dynamicPropertyNames_newList(ptr.Pointer())
}

func (ptr *Navigator) __findChildren_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.Navigator027749___findChildren_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *Navigator) __findChildren_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749___findChildren_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *Navigator) __findChildren_newList() unsafe.Pointer {
	return C.Navigator027749___findChildren_newList(ptr.Pointer())
}

func (ptr *Navigator) __findChildren_atList3(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.Navigator027749___findChildren_atList3(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *Navigator) __findChildren_setList3(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749___findChildren_setList3(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *Navigator) __findChildren_newList3() unsafe.Pointer {
	return C.Navigator027749___findChildren_newList3(ptr.Pointer())
}

func (ptr *Navigator) __qFindChildren_atList2(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.Navigator027749___qFindChildren_atList2(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *Navigator) __qFindChildren_setList2(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749___qFindChildren_setList2(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *Navigator) __qFindChildren_newList2() unsafe.Pointer {
	return C.Navigator027749___qFindChildren_newList2(ptr.Pointer())
}

func NewNavigator(parent std_core.QObject_ITF) *Navigator {
	tmpValue := NewNavigatorFromPointer(C.Navigator027749_NewNavigator(std_core.PointerFromQObject(parent)))
	if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
		tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
	}
	return tmpValue
}

//export callbackNavigator027749_DestroyNavigator
func callbackNavigator027749_DestroyNavigator(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "~Navigator"); signal != nil {
		(*(*func())(signal))()
	} else {
		NewNavigatorFromPointer(ptr).DestroyNavigatorDefault()
	}
}

func (ptr *Navigator) ConnectDestroyNavigator(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "~Navigator"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "~Navigator", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "~Navigator", unsafe.Pointer(&f))
		}
	}
}

func (ptr *Navigator) DisconnectDestroyNavigator() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "~Navigator")
	}
}

func (ptr *Navigator) DestroyNavigator() {
	if ptr.Pointer() != nil {
		C.Navigator027749_DestroyNavigator(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

func (ptr *Navigator) DestroyNavigatorDefault() {
	if ptr.Pointer() != nil {
		C.Navigator027749_DestroyNavigatorDefault(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackNavigator027749_ChildEvent
func callbackNavigator027749_ChildEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "childEvent"); signal != nil {
		(*(*func(*std_core.QChildEvent))(signal))(std_core.NewQChildEventFromPointer(event))
	} else {
		NewNavigatorFromPointer(ptr).ChildEventDefault(std_core.NewQChildEventFromPointer(event))
	}
}

func (ptr *Navigator) ChildEventDefault(event std_core.QChildEvent_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749_ChildEventDefault(ptr.Pointer(), std_core.PointerFromQChildEvent(event))
	}
}

//export callbackNavigator027749_ConnectNotify
func callbackNavigator027749_ConnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "connectNotify"); signal != nil {
		(*(*func(*std_core.QMetaMethod))(signal))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewNavigatorFromPointer(ptr).ConnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *Navigator) ConnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749_ConnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackNavigator027749_CustomEvent
func callbackNavigator027749_CustomEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "customEvent"); signal != nil {
		(*(*func(*std_core.QEvent))(signal))(std_core.NewQEventFromPointer(event))
	} else {
		NewNavigatorFromPointer(ptr).CustomEventDefault(std_core.NewQEventFromPointer(event))
	}
}

func (ptr *Navigator) CustomEventDefault(event std_core.QEvent_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749_CustomEventDefault(ptr.Pointer(), std_core.PointerFromQEvent(event))
	}
}

//export callbackNavigator027749_DeleteLater
func callbackNavigator027749_DeleteLater(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "deleteLater"); signal != nil {
		(*(*func())(signal))()
	} else {
		NewNavigatorFromPointer(ptr).DeleteLaterDefault()
	}
}

func (ptr *Navigator) DeleteLaterDefault() {
	if ptr.Pointer() != nil {
		C.Navigator027749_DeleteLaterDefault(ptr.Pointer())
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackNavigator027749_Destroyed
func callbackNavigator027749_Destroyed(ptr unsafe.Pointer, obj unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "destroyed"); signal != nil {
		(*(*func(*std_core.QObject))(signal))(std_core.NewQObjectFromPointer(obj))
	}
	qt.Unregister(ptr)

}

//export callbackNavigator027749_DisconnectNotify
func callbackNavigator027749_DisconnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "disconnectNotify"); signal != nil {
		(*(*func(*std_core.QMetaMethod))(signal))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewNavigatorFromPointer(ptr).DisconnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *Navigator) DisconnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749_DisconnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackNavigator027749_Event
func callbackNavigator027749_Event(ptr unsafe.Pointer, e unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "event"); signal != nil {
		return C.char(int8(qt.GoBoolToInt((*(*func(*std_core.QEvent) bool)(signal))(std_core.NewQEventFromPointer(e)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewNavigatorFromPointer(ptr).EventDefault(std_core.NewQEventFromPointer(e)))))
}

func (ptr *Navigator) EventDefault(e std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.Navigator027749_EventDefault(ptr.Pointer(), std_core.PointerFromQEvent(e))) != 0
	}
	return false
}

//export callbackNavigator027749_EventFilter
func callbackNavigator027749_EventFilter(ptr unsafe.Pointer, watched unsafe.Pointer, event unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "eventFilter"); signal != nil {
		return C.char(int8(qt.GoBoolToInt((*(*func(*std_core.QObject, *std_core.QEvent) bool)(signal))(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewNavigatorFromPointer(ptr).EventFilterDefault(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
}

func (ptr *Navigator) EventFilterDefault(watched std_core.QObject_ITF, event std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.Navigator027749_EventFilterDefault(ptr.Pointer(), std_core.PointerFromQObject(watched), std_core.PointerFromQEvent(event))) != 0
	}
	return false
}

//export callbackNavigator027749_ObjectNameChanged
func callbackNavigator027749_ObjectNameChanged(ptr unsafe.Pointer, objectName C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "objectNameChanged"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(objectName))
	}

}

//export callbackNavigator027749_TimerEvent
func callbackNavigator027749_TimerEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "timerEvent"); signal != nil {
		(*(*func(*std_core.QTimerEvent))(signal))(std_core.NewQTimerEventFromPointer(event))
	} else {
		NewNavigatorFromPointer(ptr).TimerEventDefault(std_core.NewQTimerEventFromPointer(event))
	}
}

func (ptr *Navigator) TimerEventDefault(event std_core.QTimerEvent_ITF) {
	if ptr.Pointer() != nil {
		C.Navigator027749_TimerEventDefault(ptr.Pointer(), std_core.PointerFromQTimerEvent(event))
	}
}

type QMedia_ITF interface {
	std_core.QObject_ITF
	QMedia_PTR() *QMedia
}

func (ptr *QMedia) QMedia_PTR() *QMedia {
	return ptr
}

func (ptr *QMedia) Pointer() unsafe.Pointer {
	if ptr != nil {
		return ptr.QObject_PTR().Pointer()
	}
	return nil
}

func (ptr *QMedia) SetPointer(p unsafe.Pointer) {
	if ptr != nil {
		ptr.QObject_PTR().SetPointer(p)
	}
}

func PointerFromQMedia(ptr QMedia_ITF) unsafe.Pointer {
	if ptr != nil {
		return ptr.QMedia_PTR().Pointer()
	}
	return nil
}

func NewQMediaFromPointer(ptr unsafe.Pointer) (n *QMedia) {
	if gPtr, ok := qt.Receive(ptr); !ok {
		n = new(QMedia)
		n.SetPointer(ptr)
	} else {
		switch deduced := gPtr.(type) {
		case *QMedia:
			n = deduced

		case *std_core.QObject:
			n = &QMedia{QObject: *deduced}

		default:
			n = new(QMedia)
			n.SetPointer(ptr)
		}
	}
	return
}
func (this *QMedia) Init() { this.init() }

//export callbackQMedia027749_Constructor
func callbackQMedia027749_Constructor(ptr unsafe.Pointer) {
	this := NewQMediaFromPointer(ptr)
	qt.Register(ptr, this)
	this.ConnectTogglePlayback(this.togglePlayback)
	this.ConnectSeek(this.seek)
	this.ConnectSetVolume(this.setVolume)
	this.ConnectPlaySingleTrack(this.playSingleTrack)
	this.ConnectShuffleAllTracks(this.shuffleAllTracks)
	this.ConnectPreviousTrack(this.previousTrack)
	this.ConnectNextTrack(this.nextTrack)
	this.init()
}

//export callbackQMedia027749_TogglePlayback
func callbackQMedia027749_TogglePlayback(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "togglePlayback"); signal != nil {
		(*(*func())(signal))()
	}

}

func (ptr *QMedia) ConnectTogglePlayback(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "togglePlayback"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "togglePlayback", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "togglePlayback", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectTogglePlayback() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "togglePlayback")
	}
}

func (ptr *QMedia) TogglePlayback() {
	if ptr.Pointer() != nil {
		C.QMedia027749_TogglePlayback(ptr.Pointer())
	}
}

//export callbackQMedia027749_Seek
func callbackQMedia027749_Seek(ptr unsafe.Pointer, position C.longlong) {
	if signal := qt.GetSignal(ptr, "seek"); signal != nil {
		(*(*func(int64))(signal))(int64(position))
	}

}

func (ptr *QMedia) ConnectSeek(f func(position int64)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "seek"); signal != nil {
			f := func(position int64) {
				(*(*func(int64))(signal))(position)
				f(position)
			}
			qt.ConnectSignal(ptr.Pointer(), "seek", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "seek", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectSeek() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "seek")
	}
}

func (ptr *QMedia) Seek(position int64) {
	if ptr.Pointer() != nil {
		C.QMedia027749_Seek(ptr.Pointer(), C.longlong(position))
	}
}

//export callbackQMedia027749_SetVolume
func callbackQMedia027749_SetVolume(ptr unsafe.Pointer, position C.int) {
	if signal := qt.GetSignal(ptr, "setVolume"); signal != nil {
		(*(*func(int))(signal))(int(int32(position)))
	}

}

func (ptr *QMedia) ConnectSetVolume(f func(position int)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setVolume"); signal != nil {
			f := func(position int) {
				(*(*func(int))(signal))(position)
				f(position)
			}
			qt.ConnectSignal(ptr.Pointer(), "setVolume", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setVolume", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectSetVolume() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setVolume")
	}
}

func (ptr *QMedia) SetVolume(position int) {
	if ptr.Pointer() != nil {
		C.QMedia027749_SetVolume(ptr.Pointer(), C.int(int32(position)))
	}
}

//export callbackQMedia027749_PlaySingleTrack
func callbackQMedia027749_PlaySingleTrack(ptr unsafe.Pointer, trackIndex C.int) {
	if signal := qt.GetSignal(ptr, "playSingleTrack"); signal != nil {
		(*(*func(int))(signal))(int(int32(trackIndex)))
	}

}

func (ptr *QMedia) ConnectPlaySingleTrack(f func(trackIndex int)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "playSingleTrack"); signal != nil {
			f := func(trackIndex int) {
				(*(*func(int))(signal))(trackIndex)
				f(trackIndex)
			}
			qt.ConnectSignal(ptr.Pointer(), "playSingleTrack", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "playSingleTrack", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectPlaySingleTrack() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "playSingleTrack")
	}
}

func (ptr *QMedia) PlaySingleTrack(trackIndex int) {
	if ptr.Pointer() != nil {
		C.QMedia027749_PlaySingleTrack(ptr.Pointer(), C.int(int32(trackIndex)))
	}
}

//export callbackQMedia027749_ShuffleAllTracks
func callbackQMedia027749_ShuffleAllTracks(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "shuffleAllTracks"); signal != nil {
		(*(*func())(signal))()
	}

}

func (ptr *QMedia) ConnectShuffleAllTracks(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "shuffleAllTracks"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "shuffleAllTracks", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "shuffleAllTracks", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectShuffleAllTracks() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "shuffleAllTracks")
	}
}

func (ptr *QMedia) ShuffleAllTracks() {
	if ptr.Pointer() != nil {
		C.QMedia027749_ShuffleAllTracks(ptr.Pointer())
	}
}

//export callbackQMedia027749_PreviousTrack
func callbackQMedia027749_PreviousTrack(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "previousTrack"); signal != nil {
		(*(*func())(signal))()
	}

}

func (ptr *QMedia) ConnectPreviousTrack(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "previousTrack"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "previousTrack", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "previousTrack", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectPreviousTrack() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "previousTrack")
	}
}

func (ptr *QMedia) PreviousTrack() {
	if ptr.Pointer() != nil {
		C.QMedia027749_PreviousTrack(ptr.Pointer())
	}
}

//export callbackQMedia027749_NextTrack
func callbackQMedia027749_NextTrack(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "nextTrack"); signal != nil {
		(*(*func())(signal))()
	}

}

func (ptr *QMedia) ConnectNextTrack(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "nextTrack"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "nextTrack", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "nextTrack", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectNextTrack() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "nextTrack")
	}
}

func (ptr *QMedia) NextTrack() {
	if ptr.Pointer() != nil {
		C.QMedia027749_NextTrack(ptr.Pointer())
	}
}

//export callbackQMedia027749_PlayerStateChanged
func callbackQMedia027749_PlayerStateChanged(ptr unsafe.Pointer, state C.int) {
	if signal := qt.GetSignal(ptr, "playerStateChanged"); signal != nil {
		(*(*func(int))(signal))(int(int32(state)))
	}

}

func (ptr *QMedia) ConnectPlayerStateChanged(f func(state int)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "playerStateChanged") {
			C.QMedia027749_ConnectPlayerStateChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "playerStateChanged"); signal != nil {
			f := func(state int) {
				(*(*func(int))(signal))(state)
				f(state)
			}
			qt.ConnectSignal(ptr.Pointer(), "playerStateChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "playerStateChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectPlayerStateChanged() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DisconnectPlayerStateChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "playerStateChanged")
	}
}

func (ptr *QMedia) PlayerStateChanged(state int) {
	if ptr.Pointer() != nil {
		C.QMedia027749_PlayerStateChanged(ptr.Pointer(), C.int(int32(state)))
	}
}

//export callbackQMedia027749_CurrentMediaChanged
func callbackQMedia027749_CurrentMediaChanged(ptr unsafe.Pointer, title C.struct_Moc_PackedString, artist C.struct_Moc_PackedString, picture C.struct_Moc_PackedString, filePath C.struct_Moc_PackedString, favourite C.char) {
	if signal := qt.GetSignal(ptr, "currentMediaChanged"); signal != nil {
		(*(*func(string, string, string, string, bool))(signal))(cGoUnpackString(title), cGoUnpackString(artist), cGoUnpackString(picture), cGoUnpackString(filePath), int8(favourite) != 0)
	}

}

func (ptr *QMedia) ConnectCurrentMediaChanged(f func(title string, artist string, picture string, filePath string, favourite bool)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "currentMediaChanged") {
			C.QMedia027749_ConnectCurrentMediaChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "currentMediaChanged"); signal != nil {
			f := func(title string, artist string, picture string, filePath string, favourite bool) {
				(*(*func(string, string, string, string, bool))(signal))(title, artist, picture, filePath, favourite)
				f(title, artist, picture, filePath, favourite)
			}
			qt.ConnectSignal(ptr.Pointer(), "currentMediaChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentMediaChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectCurrentMediaChanged() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DisconnectCurrentMediaChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "currentMediaChanged")
	}
}

func (ptr *QMedia) CurrentMediaChanged(title string, artist string, picture string, filePath string, favourite bool) {
	if ptr.Pointer() != nil {
		var titleC *C.char
		if title != "" {
			titleC = C.CString(title)
			defer C.free(unsafe.Pointer(titleC))
		}
		var artistC *C.char
		if artist != "" {
			artistC = C.CString(artist)
			defer C.free(unsafe.Pointer(artistC))
		}
		var pictureC *C.char
		if picture != "" {
			pictureC = C.CString(picture)
			defer C.free(unsafe.Pointer(pictureC))
		}
		var filePathC *C.char
		if filePath != "" {
			filePathC = C.CString(filePath)
			defer C.free(unsafe.Pointer(filePathC))
		}
		C.QMedia027749_CurrentMediaChanged(ptr.Pointer(), C.struct_Moc_PackedString{data: titleC, len: C.longlong(len(title))}, C.struct_Moc_PackedString{data: artistC, len: C.longlong(len(artist))}, C.struct_Moc_PackedString{data: pictureC, len: C.longlong(len(picture))}, C.struct_Moc_PackedString{data: filePathC, len: C.longlong(len(filePath))}, C.char(int8(qt.GoBoolToInt(favourite))))
	}
}

//export callbackQMedia027749_CurrentTrackFilePath
func callbackQMedia027749_CurrentTrackFilePath(ptr unsafe.Pointer) C.struct_Moc_PackedString {
	if signal := qt.GetSignal(ptr, "currentTrackFilePath"); signal != nil {
		tempVal := (*(*func() string)(signal))()
		return C.struct_Moc_PackedString{data: C.CString(tempVal), len: C.longlong(len(tempVal))}
	}
	tempVal := NewQMediaFromPointer(ptr).CurrentTrackFilePathDefault()
	return C.struct_Moc_PackedString{data: C.CString(tempVal), len: C.longlong(len(tempVal))}
}

func (ptr *QMedia) ConnectCurrentTrackFilePath(f func() string) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "currentTrackFilePath"); signal != nil {
			f := func() string {
				(*(*func() string)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "currentTrackFilePath", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentTrackFilePath", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectCurrentTrackFilePath() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "currentTrackFilePath")
	}
}

func (ptr *QMedia) CurrentTrackFilePath() string {
	if ptr.Pointer() != nil {
		return cGoUnpackString(C.QMedia027749_CurrentTrackFilePath(ptr.Pointer()))
	}
	return ""
}

func (ptr *QMedia) CurrentTrackFilePathDefault() string {
	if ptr.Pointer() != nil {
		return cGoUnpackString(C.QMedia027749_CurrentTrackFilePathDefault(ptr.Pointer()))
	}
	return ""
}

//export callbackQMedia027749_SetCurrentTrackFilePath
func callbackQMedia027749_SetCurrentTrackFilePath(ptr unsafe.Pointer, currentTrackFilePath C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "setCurrentTrackFilePath"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(currentTrackFilePath))
	} else {
		NewQMediaFromPointer(ptr).SetCurrentTrackFilePathDefault(cGoUnpackString(currentTrackFilePath))
	}
}

func (ptr *QMedia) ConnectSetCurrentTrackFilePath(f func(currentTrackFilePath string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setCurrentTrackFilePath"); signal != nil {
			f := func(currentTrackFilePath string) {
				(*(*func(string))(signal))(currentTrackFilePath)
				f(currentTrackFilePath)
			}
			qt.ConnectSignal(ptr.Pointer(), "setCurrentTrackFilePath", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setCurrentTrackFilePath", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectSetCurrentTrackFilePath() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setCurrentTrackFilePath")
	}
}

func (ptr *QMedia) SetCurrentTrackFilePath(currentTrackFilePath string) {
	if ptr.Pointer() != nil {
		var currentTrackFilePathC *C.char
		if currentTrackFilePath != "" {
			currentTrackFilePathC = C.CString(currentTrackFilePath)
			defer C.free(unsafe.Pointer(currentTrackFilePathC))
		}
		C.QMedia027749_SetCurrentTrackFilePath(ptr.Pointer(), C.struct_Moc_PackedString{data: currentTrackFilePathC, len: C.longlong(len(currentTrackFilePath))})
	}
}

func (ptr *QMedia) SetCurrentTrackFilePathDefault(currentTrackFilePath string) {
	if ptr.Pointer() != nil {
		var currentTrackFilePathC *C.char
		if currentTrackFilePath != "" {
			currentTrackFilePathC = C.CString(currentTrackFilePath)
			defer C.free(unsafe.Pointer(currentTrackFilePathC))
		}
		C.QMedia027749_SetCurrentTrackFilePathDefault(ptr.Pointer(), C.struct_Moc_PackedString{data: currentTrackFilePathC, len: C.longlong(len(currentTrackFilePath))})
	}
}

//export callbackQMedia027749_CurrentTrackFilePathChanged
func callbackQMedia027749_CurrentTrackFilePathChanged(ptr unsafe.Pointer, currentTrackFilePath C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "currentTrackFilePathChanged"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(currentTrackFilePath))
	}

}

func (ptr *QMedia) ConnectCurrentTrackFilePathChanged(f func(currentTrackFilePath string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "currentTrackFilePathChanged") {
			C.QMedia027749_ConnectCurrentTrackFilePathChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "currentTrackFilePathChanged"); signal != nil {
			f := func(currentTrackFilePath string) {
				(*(*func(string))(signal))(currentTrackFilePath)
				f(currentTrackFilePath)
			}
			qt.ConnectSignal(ptr.Pointer(), "currentTrackFilePathChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentTrackFilePathChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectCurrentTrackFilePathChanged() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DisconnectCurrentTrackFilePathChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "currentTrackFilePathChanged")
	}
}

func (ptr *QMedia) CurrentTrackFilePathChanged(currentTrackFilePath string) {
	if ptr.Pointer() != nil {
		var currentTrackFilePathC *C.char
		if currentTrackFilePath != "" {
			currentTrackFilePathC = C.CString(currentTrackFilePath)
			defer C.free(unsafe.Pointer(currentTrackFilePathC))
		}
		C.QMedia027749_CurrentTrackFilePathChanged(ptr.Pointer(), C.struct_Moc_PackedString{data: currentTrackFilePathC, len: C.longlong(len(currentTrackFilePath))})
	}
}

//export callbackQMedia027749_CurrentPosition
func callbackQMedia027749_CurrentPosition(ptr unsafe.Pointer) C.longlong {
	if signal := qt.GetSignal(ptr, "currentPosition"); signal != nil {
		return C.longlong((*(*func() int64)(signal))())
	}

	return C.longlong(NewQMediaFromPointer(ptr).CurrentPositionDefault())
}

func (ptr *QMedia) ConnectCurrentPosition(f func() int64) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "currentPosition"); signal != nil {
			f := func() int64 {
				(*(*func() int64)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "currentPosition", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentPosition", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectCurrentPosition() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "currentPosition")
	}
}

func (ptr *QMedia) CurrentPosition() int64 {
	if ptr.Pointer() != nil {
		return int64(C.QMedia027749_CurrentPosition(ptr.Pointer()))
	}
	return 0
}

func (ptr *QMedia) CurrentPositionDefault() int64 {
	if ptr.Pointer() != nil {
		return int64(C.QMedia027749_CurrentPositionDefault(ptr.Pointer()))
	}
	return 0
}

//export callbackQMedia027749_SetCurrentPosition
func callbackQMedia027749_SetCurrentPosition(ptr unsafe.Pointer, currentPosition C.longlong) {
	if signal := qt.GetSignal(ptr, "setCurrentPosition"); signal != nil {
		(*(*func(int64))(signal))(int64(currentPosition))
	} else {
		NewQMediaFromPointer(ptr).SetCurrentPositionDefault(int64(currentPosition))
	}
}

func (ptr *QMedia) ConnectSetCurrentPosition(f func(currentPosition int64)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setCurrentPosition"); signal != nil {
			f := func(currentPosition int64) {
				(*(*func(int64))(signal))(currentPosition)
				f(currentPosition)
			}
			qt.ConnectSignal(ptr.Pointer(), "setCurrentPosition", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setCurrentPosition", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectSetCurrentPosition() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setCurrentPosition")
	}
}

func (ptr *QMedia) SetCurrentPosition(currentPosition int64) {
	if ptr.Pointer() != nil {
		C.QMedia027749_SetCurrentPosition(ptr.Pointer(), C.longlong(currentPosition))
	}
}

func (ptr *QMedia) SetCurrentPositionDefault(currentPosition int64) {
	if ptr.Pointer() != nil {
		C.QMedia027749_SetCurrentPositionDefault(ptr.Pointer(), C.longlong(currentPosition))
	}
}

//export callbackQMedia027749_CurrentPositionChanged
func callbackQMedia027749_CurrentPositionChanged(ptr unsafe.Pointer, currentPosition C.longlong) {
	if signal := qt.GetSignal(ptr, "currentPositionChanged"); signal != nil {
		(*(*func(int64))(signal))(int64(currentPosition))
	}

}

func (ptr *QMedia) ConnectCurrentPositionChanged(f func(currentPosition int64)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "currentPositionChanged") {
			C.QMedia027749_ConnectCurrentPositionChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "currentPositionChanged"); signal != nil {
			f := func(currentPosition int64) {
				(*(*func(int64))(signal))(currentPosition)
				f(currentPosition)
			}
			qt.ConnectSignal(ptr.Pointer(), "currentPositionChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentPositionChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectCurrentPositionChanged() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DisconnectCurrentPositionChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "currentPositionChanged")
	}
}

func (ptr *QMedia) CurrentPositionChanged(currentPosition int64) {
	if ptr.Pointer() != nil {
		C.QMedia027749_CurrentPositionChanged(ptr.Pointer(), C.longlong(currentPosition))
	}
}

//export callbackQMedia027749_CurrentDuration
func callbackQMedia027749_CurrentDuration(ptr unsafe.Pointer) C.longlong {
	if signal := qt.GetSignal(ptr, "currentDuration"); signal != nil {
		return C.longlong((*(*func() int64)(signal))())
	}

	return C.longlong(NewQMediaFromPointer(ptr).CurrentDurationDefault())
}

func (ptr *QMedia) ConnectCurrentDuration(f func() int64) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "currentDuration"); signal != nil {
			f := func() int64 {
				(*(*func() int64)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "currentDuration", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentDuration", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectCurrentDuration() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "currentDuration")
	}
}

func (ptr *QMedia) CurrentDuration() int64 {
	if ptr.Pointer() != nil {
		return int64(C.QMedia027749_CurrentDuration(ptr.Pointer()))
	}
	return 0
}

func (ptr *QMedia) CurrentDurationDefault() int64 {
	if ptr.Pointer() != nil {
		return int64(C.QMedia027749_CurrentDurationDefault(ptr.Pointer()))
	}
	return 0
}

//export callbackQMedia027749_SetCurrentDuration
func callbackQMedia027749_SetCurrentDuration(ptr unsafe.Pointer, currentDuration C.longlong) {
	if signal := qt.GetSignal(ptr, "setCurrentDuration"); signal != nil {
		(*(*func(int64))(signal))(int64(currentDuration))
	} else {
		NewQMediaFromPointer(ptr).SetCurrentDurationDefault(int64(currentDuration))
	}
}

func (ptr *QMedia) ConnectSetCurrentDuration(f func(currentDuration int64)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setCurrentDuration"); signal != nil {
			f := func(currentDuration int64) {
				(*(*func(int64))(signal))(currentDuration)
				f(currentDuration)
			}
			qt.ConnectSignal(ptr.Pointer(), "setCurrentDuration", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setCurrentDuration", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectSetCurrentDuration() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setCurrentDuration")
	}
}

func (ptr *QMedia) SetCurrentDuration(currentDuration int64) {
	if ptr.Pointer() != nil {
		C.QMedia027749_SetCurrentDuration(ptr.Pointer(), C.longlong(currentDuration))
	}
}

func (ptr *QMedia) SetCurrentDurationDefault(currentDuration int64) {
	if ptr.Pointer() != nil {
		C.QMedia027749_SetCurrentDurationDefault(ptr.Pointer(), C.longlong(currentDuration))
	}
}

//export callbackQMedia027749_CurrentDurationChanged
func callbackQMedia027749_CurrentDurationChanged(ptr unsafe.Pointer, currentDuration C.longlong) {
	if signal := qt.GetSignal(ptr, "currentDurationChanged"); signal != nil {
		(*(*func(int64))(signal))(int64(currentDuration))
	}

}

func (ptr *QMedia) ConnectCurrentDurationChanged(f func(currentDuration int64)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "currentDurationChanged") {
			C.QMedia027749_ConnectCurrentDurationChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "currentDurationChanged"); signal != nil {
			f := func(currentDuration int64) {
				(*(*func(int64))(signal))(currentDuration)
				f(currentDuration)
			}
			qt.ConnectSignal(ptr.Pointer(), "currentDurationChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "currentDurationChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectCurrentDurationChanged() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DisconnectCurrentDurationChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "currentDurationChanged")
	}
}

func (ptr *QMedia) CurrentDurationChanged(currentDuration int64) {
	if ptr.Pointer() != nil {
		C.QMedia027749_CurrentDurationChanged(ptr.Pointer(), C.longlong(currentDuration))
	}
}

func QMedia_QRegisterMetaType() int {
	return int(int32(C.QMedia027749_QMedia027749_QRegisterMetaType()))
}

func (ptr *QMedia) QRegisterMetaType() int {
	return int(int32(C.QMedia027749_QMedia027749_QRegisterMetaType()))
}

func QMedia_QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.QMedia027749_QMedia027749_QRegisterMetaType2(typeNameC)))
}

func (ptr *QMedia) QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.QMedia027749_QMedia027749_QRegisterMetaType2(typeNameC)))
}

func QMedia_QmlRegisterType() int {
	return int(int32(C.QMedia027749_QMedia027749_QmlRegisterType()))
}

func (ptr *QMedia) QmlRegisterType() int {
	return int(int32(C.QMedia027749_QMedia027749_QmlRegisterType()))
}

func QMedia_QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.QMedia027749_QMedia027749_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *QMedia) QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.QMedia027749_QMedia027749_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *QMedia) __children_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QMedia027749___children_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QMedia) __children_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749___children_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QMedia) __children_newList() unsafe.Pointer {
	return C.QMedia027749___children_newList(ptr.Pointer())
}

func (ptr *QMedia) __dynamicPropertyNames_atList(i int) *std_core.QByteArray {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQByteArrayFromPointer(C.QMedia027749___dynamicPropertyNames_atList(ptr.Pointer(), C.int(int32(i))))
		runtime.SetFinalizer(tmpValue, (*std_core.QByteArray).DestroyQByteArray)
		return tmpValue
	}
	return nil
}

func (ptr *QMedia) __dynamicPropertyNames_setList(i std_core.QByteArray_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749___dynamicPropertyNames_setList(ptr.Pointer(), std_core.PointerFromQByteArray(i))
	}
}

func (ptr *QMedia) __dynamicPropertyNames_newList() unsafe.Pointer {
	return C.QMedia027749___dynamicPropertyNames_newList(ptr.Pointer())
}

func (ptr *QMedia) __findChildren_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QMedia027749___findChildren_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QMedia) __findChildren_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749___findChildren_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QMedia) __findChildren_newList() unsafe.Pointer {
	return C.QMedia027749___findChildren_newList(ptr.Pointer())
}

func (ptr *QMedia) __findChildren_atList3(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QMedia027749___findChildren_atList3(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QMedia) __findChildren_setList3(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749___findChildren_setList3(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QMedia) __findChildren_newList3() unsafe.Pointer {
	return C.QMedia027749___findChildren_newList3(ptr.Pointer())
}

func (ptr *QMedia) __qFindChildren_atList2(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QMedia027749___qFindChildren_atList2(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QMedia) __qFindChildren_setList2(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749___qFindChildren_setList2(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QMedia) __qFindChildren_newList2() unsafe.Pointer {
	return C.QMedia027749___qFindChildren_newList2(ptr.Pointer())
}

func NewQMedia(parent std_core.QObject_ITF) *QMedia {
	tmpValue := NewQMediaFromPointer(C.QMedia027749_NewQMedia(std_core.PointerFromQObject(parent)))
	if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
		tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
	}
	return tmpValue
}

//export callbackQMedia027749_DestroyQMedia
func callbackQMedia027749_DestroyQMedia(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "~QMedia"); signal != nil {
		(*(*func())(signal))()
	} else {
		NewQMediaFromPointer(ptr).DestroyQMediaDefault()
	}
}

func (ptr *QMedia) ConnectDestroyQMedia(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "~QMedia"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "~QMedia", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "~QMedia", unsafe.Pointer(&f))
		}
	}
}

func (ptr *QMedia) DisconnectDestroyQMedia() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "~QMedia")
	}
}

func (ptr *QMedia) DestroyQMedia() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DestroyQMedia(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

func (ptr *QMedia) DestroyQMediaDefault() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DestroyQMediaDefault(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackQMedia027749_ChildEvent
func callbackQMedia027749_ChildEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "childEvent"); signal != nil {
		(*(*func(*std_core.QChildEvent))(signal))(std_core.NewQChildEventFromPointer(event))
	} else {
		NewQMediaFromPointer(ptr).ChildEventDefault(std_core.NewQChildEventFromPointer(event))
	}
}

func (ptr *QMedia) ChildEventDefault(event std_core.QChildEvent_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749_ChildEventDefault(ptr.Pointer(), std_core.PointerFromQChildEvent(event))
	}
}

//export callbackQMedia027749_ConnectNotify
func callbackQMedia027749_ConnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "connectNotify"); signal != nil {
		(*(*func(*std_core.QMetaMethod))(signal))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewQMediaFromPointer(ptr).ConnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *QMedia) ConnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749_ConnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackQMedia027749_CustomEvent
func callbackQMedia027749_CustomEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "customEvent"); signal != nil {
		(*(*func(*std_core.QEvent))(signal))(std_core.NewQEventFromPointer(event))
	} else {
		NewQMediaFromPointer(ptr).CustomEventDefault(std_core.NewQEventFromPointer(event))
	}
}

func (ptr *QMedia) CustomEventDefault(event std_core.QEvent_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749_CustomEventDefault(ptr.Pointer(), std_core.PointerFromQEvent(event))
	}
}

//export callbackQMedia027749_DeleteLater
func callbackQMedia027749_DeleteLater(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "deleteLater"); signal != nil {
		(*(*func())(signal))()
	} else {
		NewQMediaFromPointer(ptr).DeleteLaterDefault()
	}
}

func (ptr *QMedia) DeleteLaterDefault() {
	if ptr.Pointer() != nil {
		C.QMedia027749_DeleteLaterDefault(ptr.Pointer())
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackQMedia027749_Destroyed
func callbackQMedia027749_Destroyed(ptr unsafe.Pointer, obj unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "destroyed"); signal != nil {
		(*(*func(*std_core.QObject))(signal))(std_core.NewQObjectFromPointer(obj))
	}
	qt.Unregister(ptr)

}

//export callbackQMedia027749_DisconnectNotify
func callbackQMedia027749_DisconnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "disconnectNotify"); signal != nil {
		(*(*func(*std_core.QMetaMethod))(signal))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewQMediaFromPointer(ptr).DisconnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *QMedia) DisconnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749_DisconnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackQMedia027749_Event
func callbackQMedia027749_Event(ptr unsafe.Pointer, e unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "event"); signal != nil {
		return C.char(int8(qt.GoBoolToInt((*(*func(*std_core.QEvent) bool)(signal))(std_core.NewQEventFromPointer(e)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewQMediaFromPointer(ptr).EventDefault(std_core.NewQEventFromPointer(e)))))
}

func (ptr *QMedia) EventDefault(e std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.QMedia027749_EventDefault(ptr.Pointer(), std_core.PointerFromQEvent(e))) != 0
	}
	return false
}

//export callbackQMedia027749_EventFilter
func callbackQMedia027749_EventFilter(ptr unsafe.Pointer, watched unsafe.Pointer, event unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "eventFilter"); signal != nil {
		return C.char(int8(qt.GoBoolToInt((*(*func(*std_core.QObject, *std_core.QEvent) bool)(signal))(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewQMediaFromPointer(ptr).EventFilterDefault(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
}

func (ptr *QMedia) EventFilterDefault(watched std_core.QObject_ITF, event std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.QMedia027749_EventFilterDefault(ptr.Pointer(), std_core.PointerFromQObject(watched), std_core.PointerFromQEvent(event))) != 0
	}
	return false
}

//export callbackQMedia027749_ObjectNameChanged
func callbackQMedia027749_ObjectNameChanged(ptr unsafe.Pointer, objectName C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "objectNameChanged"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(objectName))
	}

}

//export callbackQMedia027749_TimerEvent
func callbackQMedia027749_TimerEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "timerEvent"); signal != nil {
		(*(*func(*std_core.QTimerEvent))(signal))(std_core.NewQTimerEventFromPointer(event))
	} else {
		NewQMediaFromPointer(ptr).TimerEventDefault(std_core.NewQTimerEventFromPointer(event))
	}
}

func (ptr *QMedia) TimerEventDefault(event std_core.QTimerEvent_ITF) {
	if ptr.Pointer() != nil {
		C.QMedia027749_TimerEventDefault(ptr.Pointer(), std_core.PointerFromQTimerEvent(event))
	}
}

type qmlBridge_ITF interface {
	std_core.QObject_ITF
	qmlBridge_PTR() *qmlBridge
}

func (ptr *qmlBridge) qmlBridge_PTR() *qmlBridge {
	return ptr
}

func (ptr *qmlBridge) Pointer() unsafe.Pointer {
	if ptr != nil {
		return ptr.QObject_PTR().Pointer()
	}
	return nil
}

func (ptr *qmlBridge) SetPointer(p unsafe.Pointer) {
	if ptr != nil {
		ptr.QObject_PTR().SetPointer(p)
	}
}

func PointerFromQmlBridge(ptr qmlBridge_ITF) unsafe.Pointer {
	if ptr != nil {
		return ptr.qmlBridge_PTR().Pointer()
	}
	return nil
}

func NewQmlBridgeFromPointer(ptr unsafe.Pointer) (n *qmlBridge) {
	if gPtr, ok := qt.Receive(ptr); !ok {
		n = new(qmlBridge)
		n.SetPointer(ptr)
	} else {
		switch deduced := gPtr.(type) {
		case *qmlBridge:
			n = deduced

		case *std_core.QObject:
			n = &qmlBridge{QObject: *deduced}

		default:
			n = new(qmlBridge)
			n.SetPointer(ptr)
		}
	}
	return
}
func (this *qmlBridge) Init() { this.init() }

//export callbackqmlBridge027749_Constructor
func callbackqmlBridge027749_Constructor(ptr unsafe.Pointer) {
	this := NewQmlBridgeFromPointer(ptr)
	qt.Register(ptr, this)
	this.ConnectAddFolders(this.addFolders)
	this.ConnectImportTracks(this.importTracks)
	this.ConnectUpdateTracksModelData(this.updateTracksModelData)
	this.ConnectToggleTrackFavourite(this.toggleTrackFavourite)
	this.init()
}

//export callbackqmlBridge027749_AddFolders
func callbackqmlBridge027749_AddFolders(ptr unsafe.Pointer, folders C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "addFolders"); signal != nil {
		(*(*func([]string))(signal))(unpackStringList(cGoUnpackString(folders)))
	}

}

func (ptr *qmlBridge) ConnectAddFolders(f func(folders []string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "addFolders"); signal != nil {
			f := func(folders []string) {
				(*(*func([]string))(signal))(folders)
				f(folders)
			}
			qt.ConnectSignal(ptr.Pointer(), "addFolders", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "addFolders", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectAddFolders() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "addFolders")
	}
}

func (ptr *qmlBridge) AddFolders(folders []string) {
	if ptr.Pointer() != nil {
		foldersC := C.CString(strings.Join(folders, "¡¦!"))
		defer C.free(unsafe.Pointer(foldersC))
		C.qmlBridge027749_AddFolders(ptr.Pointer(), C.struct_Moc_PackedString{data: foldersC, len: C.longlong(len(strings.Join(folders, "¡¦!")))})
	}
}

//export callbackqmlBridge027749_ImportTracks
func callbackqmlBridge027749_ImportTracks(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "importTracks"); signal != nil {
		(*(*func())(signal))()
	}

}

func (ptr *qmlBridge) ConnectImportTracks(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "importTracks"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "importTracks", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "importTracks", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectImportTracks() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "importTracks")
	}
}

func (ptr *qmlBridge) ImportTracks() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_ImportTracks(ptr.Pointer())
	}
}

//export callbackqmlBridge027749_UpdateTracksModelData
func callbackqmlBridge027749_UpdateTracksModelData(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "updateTracksModelData"); signal != nil {
		(*(*func())(signal))()
	}

}

func (ptr *qmlBridge) ConnectUpdateTracksModelData(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "updateTracksModelData"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "updateTracksModelData", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "updateTracksModelData", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectUpdateTracksModelData() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "updateTracksModelData")
	}
}

func (ptr *qmlBridge) UpdateTracksModelData() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_UpdateTracksModelData(ptr.Pointer())
	}
}

//export callbackqmlBridge027749_ToggleTrackFavourite
func callbackqmlBridge027749_ToggleTrackFavourite(ptr unsafe.Pointer, filepath C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "toggleTrackFavourite"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(filepath))
	}

}

func (ptr *qmlBridge) ConnectToggleTrackFavourite(f func(filepath string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "toggleTrackFavourite"); signal != nil {
			f := func(filepath string) {
				(*(*func(string))(signal))(filepath)
				f(filepath)
			}
			qt.ConnectSignal(ptr.Pointer(), "toggleTrackFavourite", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "toggleTrackFavourite", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectToggleTrackFavourite() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "toggleTrackFavourite")
	}
}

func (ptr *qmlBridge) ToggleTrackFavourite(filepath string) {
	if ptr.Pointer() != nil {
		var filepathC *C.char
		if filepath != "" {
			filepathC = C.CString(filepath)
			defer C.free(unsafe.Pointer(filepathC))
		}
		C.qmlBridge027749_ToggleTrackFavourite(ptr.Pointer(), C.struct_Moc_PackedString{data: filepathC, len: C.longlong(len(filepath))})
	}
}

//export callbackqmlBridge027749_FolderScanComplete
func callbackqmlBridge027749_FolderScanComplete(ptr unsafe.Pointer, scanCount C.int) {
	if signal := qt.GetSignal(ptr, "folderScanComplete"); signal != nil {
		(*(*func(int))(signal))(int(int32(scanCount)))
	}

}

func (ptr *qmlBridge) ConnectFolderScanComplete(f func(scanCount int)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "folderScanComplete") {
			C.qmlBridge027749_ConnectFolderScanComplete(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "folderScanComplete"); signal != nil {
			f := func(scanCount int) {
				(*(*func(int))(signal))(scanCount)
				f(scanCount)
			}
			qt.ConnectSignal(ptr.Pointer(), "folderScanComplete", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "folderScanComplete", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectFolderScanComplete() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectFolderScanComplete(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "folderScanComplete")
	}
}

func (ptr *qmlBridge) FolderScanComplete(scanCount int) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_FolderScanComplete(ptr.Pointer(), C.int(int32(scanCount)))
	}
}

//export callbackqmlBridge027749_TrackImported
func callbackqmlBridge027749_TrackImported(ptr unsafe.Pointer, trackTitle C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "trackImported"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(trackTitle))
	}

}

func (ptr *qmlBridge) ConnectTrackImported(f func(trackTitle string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "trackImported") {
			C.qmlBridge027749_ConnectTrackImported(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "trackImported"); signal != nil {
			f := func(trackTitle string) {
				(*(*func(string))(signal))(trackTitle)
				f(trackTitle)
			}
			qt.ConnectSignal(ptr.Pointer(), "trackImported", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "trackImported", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectTrackImported() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectTrackImported(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "trackImported")
	}
}

func (ptr *qmlBridge) TrackImported(trackTitle string) {
	if ptr.Pointer() != nil {
		var trackTitleC *C.char
		if trackTitle != "" {
			trackTitleC = C.CString(trackTitle)
			defer C.free(unsafe.Pointer(trackTitleC))
		}
		C.qmlBridge027749_TrackImported(ptr.Pointer(), C.struct_Moc_PackedString{data: trackTitleC, len: C.longlong(len(trackTitle))})
	}
}

//export callbackqmlBridge027749_TrackImportComplete
func callbackqmlBridge027749_TrackImportComplete(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "trackImportComplete"); signal != nil {
		(*(*func())(signal))()
	}

}

func (ptr *qmlBridge) ConnectTrackImportComplete(f func()) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "trackImportComplete") {
			C.qmlBridge027749_ConnectTrackImportComplete(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "trackImportComplete"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "trackImportComplete", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "trackImportComplete", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectTrackImportComplete() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectTrackImportComplete(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "trackImportComplete")
	}
}

func (ptr *qmlBridge) TrackImportComplete() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_TrackImportComplete(ptr.Pointer())
	}
}

//export callbackqmlBridge027749_Navigator027749
func callbackqmlBridge027749_Navigator027749(ptr unsafe.Pointer) unsafe.Pointer {
	if signal := qt.GetSignal(ptr, "navigator"); signal != nil {
		return PointerFromNavigator((*(*func() *Navigator)(signal))())
	}

	return PointerFromNavigator(NewQmlBridgeFromPointer(ptr).NavigatorDefault())
}

func (ptr *qmlBridge) ConnectNavigator(f func() *Navigator) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "navigator"); signal != nil {
			f := func() *Navigator {
				(*(*func() *Navigator)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "navigator", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "navigator", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectNavigator() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "navigator")
	}
}

func (ptr *qmlBridge) Navigator() *Navigator {
	if ptr.Pointer() != nil {
		tmpValue := NewNavigatorFromPointer(C.qmlBridge027749_Navigator027749(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) NavigatorDefault() *Navigator {
	if ptr.Pointer() != nil {
		tmpValue := NewNavigatorFromPointer(C.qmlBridge027749_NavigatorDefault(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

//export callbackqmlBridge027749_SetNavigator
func callbackqmlBridge027749_SetNavigator(ptr unsafe.Pointer, navigator unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "setNavigator"); signal != nil {
		(*(*func(*Navigator))(signal))(NewNavigatorFromPointer(navigator))
	} else {
		NewQmlBridgeFromPointer(ptr).SetNavigatorDefault(NewNavigatorFromPointer(navigator))
	}
}

func (ptr *qmlBridge) ConnectSetNavigator(f func(navigator *Navigator)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setNavigator"); signal != nil {
			f := func(navigator *Navigator) {
				(*(*func(*Navigator))(signal))(navigator)
				f(navigator)
			}
			qt.ConnectSignal(ptr.Pointer(), "setNavigator", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setNavigator", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectSetNavigator() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setNavigator")
	}
}

func (ptr *qmlBridge) SetNavigator(navigator Navigator_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetNavigator(ptr.Pointer(), PointerFromNavigator(navigator))
	}
}

func (ptr *qmlBridge) SetNavigatorDefault(navigator Navigator_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetNavigatorDefault(ptr.Pointer(), PointerFromNavigator(navigator))
	}
}

//export callbackqmlBridge027749_NavigatorChanged
func callbackqmlBridge027749_NavigatorChanged(ptr unsafe.Pointer, navigator unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "navigatorChanged"); signal != nil {
		(*(*func(*Navigator))(signal))(NewNavigatorFromPointer(navigator))
	}

}

func (ptr *qmlBridge) ConnectNavigatorChanged(f func(navigator *Navigator)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "navigatorChanged") {
			C.qmlBridge027749_ConnectNavigatorChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "navigatorChanged"); signal != nil {
			f := func(navigator *Navigator) {
				(*(*func(*Navigator))(signal))(navigator)
				f(navigator)
			}
			qt.ConnectSignal(ptr.Pointer(), "navigatorChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "navigatorChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectNavigatorChanged() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectNavigatorChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "navigatorChanged")
	}
}

func (ptr *qmlBridge) NavigatorChanged(navigator Navigator_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_NavigatorChanged(ptr.Pointer(), PointerFromNavigator(navigator))
	}
}

//export callbackqmlBridge027749_QMedia027749
func callbackqmlBridge027749_QMedia027749(ptr unsafe.Pointer) unsafe.Pointer {
	if signal := qt.GetSignal(ptr, "qMedia"); signal != nil {
		return PointerFromQMedia((*(*func() *QMedia)(signal))())
	}

	return PointerFromQMedia(NewQmlBridgeFromPointer(ptr).QMediaDefault())
}

func (ptr *qmlBridge) ConnectQMedia(f func() *QMedia) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "qMedia"); signal != nil {
			f := func() *QMedia {
				(*(*func() *QMedia)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "qMedia", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "qMedia", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectQMedia() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "qMedia")
	}
}

func (ptr *qmlBridge) QMedia() *QMedia {
	if ptr.Pointer() != nil {
		tmpValue := NewQMediaFromPointer(C.qmlBridge027749_QMedia027749(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) QMediaDefault() *QMedia {
	if ptr.Pointer() != nil {
		tmpValue := NewQMediaFromPointer(C.qmlBridge027749_QMediaDefault(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

//export callbackqmlBridge027749_SetQMedia
func callbackqmlBridge027749_SetQMedia(ptr unsafe.Pointer, qMedia unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "setQMedia"); signal != nil {
		(*(*func(*QMedia))(signal))(NewQMediaFromPointer(qMedia))
	} else {
		NewQmlBridgeFromPointer(ptr).SetQMediaDefault(NewQMediaFromPointer(qMedia))
	}
}

func (ptr *qmlBridge) ConnectSetQMedia(f func(qMedia *QMedia)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setQMedia"); signal != nil {
			f := func(qMedia *QMedia) {
				(*(*func(*QMedia))(signal))(qMedia)
				f(qMedia)
			}
			qt.ConnectSignal(ptr.Pointer(), "setQMedia", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setQMedia", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectSetQMedia() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setQMedia")
	}
}

func (ptr *qmlBridge) SetQMedia(qMedia QMedia_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetQMedia(ptr.Pointer(), PointerFromQMedia(qMedia))
	}
}

func (ptr *qmlBridge) SetQMediaDefault(qMedia QMedia_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetQMediaDefault(ptr.Pointer(), PointerFromQMedia(qMedia))
	}
}

//export callbackqmlBridge027749_QMediaChanged
func callbackqmlBridge027749_QMediaChanged(ptr unsafe.Pointer, qMedia unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "qMediaChanged"); signal != nil {
		(*(*func(*QMedia))(signal))(NewQMediaFromPointer(qMedia))
	}

}

func (ptr *qmlBridge) ConnectQMediaChanged(f func(qMedia *QMedia)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "qMediaChanged") {
			C.qmlBridge027749_ConnectQMediaChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "qMediaChanged"); signal != nil {
			f := func(qMedia *QMedia) {
				(*(*func(*QMedia))(signal))(qMedia)
				f(qMedia)
			}
			qt.ConnectSignal(ptr.Pointer(), "qMediaChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "qMediaChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectQMediaChanged() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectQMediaChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "qMediaChanged")
	}
}

func (ptr *qmlBridge) QMediaChanged(qMedia QMedia_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_QMediaChanged(ptr.Pointer(), PointerFromQMedia(qMedia))
	}
}

//export callbackqmlBridge027749_ThumbsPath
func callbackqmlBridge027749_ThumbsPath(ptr unsafe.Pointer) C.struct_Moc_PackedString {
	if signal := qt.GetSignal(ptr, "thumbsPath"); signal != nil {
		tempVal := (*(*func() string)(signal))()
		return C.struct_Moc_PackedString{data: C.CString(tempVal), len: C.longlong(len(tempVal))}
	}
	tempVal := NewQmlBridgeFromPointer(ptr).ThumbsPathDefault()
	return C.struct_Moc_PackedString{data: C.CString(tempVal), len: C.longlong(len(tempVal))}
}

func (ptr *qmlBridge) ConnectThumbsPath(f func() string) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "thumbsPath"); signal != nil {
			f := func() string {
				(*(*func() string)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "thumbsPath", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "thumbsPath", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectThumbsPath() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "thumbsPath")
	}
}

func (ptr *qmlBridge) ThumbsPath() string {
	if ptr.Pointer() != nil {
		return cGoUnpackString(C.qmlBridge027749_ThumbsPath(ptr.Pointer()))
	}
	return ""
}

func (ptr *qmlBridge) ThumbsPathDefault() string {
	if ptr.Pointer() != nil {
		return cGoUnpackString(C.qmlBridge027749_ThumbsPathDefault(ptr.Pointer()))
	}
	return ""
}

//export callbackqmlBridge027749_SetThumbsPath
func callbackqmlBridge027749_SetThumbsPath(ptr unsafe.Pointer, thumbsPath C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "setThumbsPath"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(thumbsPath))
	} else {
		NewQmlBridgeFromPointer(ptr).SetThumbsPathDefault(cGoUnpackString(thumbsPath))
	}
}

func (ptr *qmlBridge) ConnectSetThumbsPath(f func(thumbsPath string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setThumbsPath"); signal != nil {
			f := func(thumbsPath string) {
				(*(*func(string))(signal))(thumbsPath)
				f(thumbsPath)
			}
			qt.ConnectSignal(ptr.Pointer(), "setThumbsPath", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setThumbsPath", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectSetThumbsPath() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setThumbsPath")
	}
}

func (ptr *qmlBridge) SetThumbsPath(thumbsPath string) {
	if ptr.Pointer() != nil {
		var thumbsPathC *C.char
		if thumbsPath != "" {
			thumbsPathC = C.CString(thumbsPath)
			defer C.free(unsafe.Pointer(thumbsPathC))
		}
		C.qmlBridge027749_SetThumbsPath(ptr.Pointer(), C.struct_Moc_PackedString{data: thumbsPathC, len: C.longlong(len(thumbsPath))})
	}
}

func (ptr *qmlBridge) SetThumbsPathDefault(thumbsPath string) {
	if ptr.Pointer() != nil {
		var thumbsPathC *C.char
		if thumbsPath != "" {
			thumbsPathC = C.CString(thumbsPath)
			defer C.free(unsafe.Pointer(thumbsPathC))
		}
		C.qmlBridge027749_SetThumbsPathDefault(ptr.Pointer(), C.struct_Moc_PackedString{data: thumbsPathC, len: C.longlong(len(thumbsPath))})
	}
}

//export callbackqmlBridge027749_ThumbsPathChanged
func callbackqmlBridge027749_ThumbsPathChanged(ptr unsafe.Pointer, thumbsPath C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "thumbsPathChanged"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(thumbsPath))
	}

}

func (ptr *qmlBridge) ConnectThumbsPathChanged(f func(thumbsPath string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "thumbsPathChanged") {
			C.qmlBridge027749_ConnectThumbsPathChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "thumbsPathChanged"); signal != nil {
			f := func(thumbsPath string) {
				(*(*func(string))(signal))(thumbsPath)
				f(thumbsPath)
			}
			qt.ConnectSignal(ptr.Pointer(), "thumbsPathChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "thumbsPathChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectThumbsPathChanged() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectThumbsPathChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "thumbsPathChanged")
	}
}

func (ptr *qmlBridge) ThumbsPathChanged(thumbsPath string) {
	if ptr.Pointer() != nil {
		var thumbsPathC *C.char
		if thumbsPath != "" {
			thumbsPathC = C.CString(thumbsPath)
			defer C.free(unsafe.Pointer(thumbsPathC))
		}
		C.qmlBridge027749_ThumbsPathChanged(ptr.Pointer(), C.struct_Moc_PackedString{data: thumbsPathC, len: C.longlong(len(thumbsPath))})
	}
}

//export callbackqmlBridge027749_TracksModelb23e62
func callbackqmlBridge027749_TracksModelb23e62(ptr unsafe.Pointer) unsafe.Pointer {
	if signal := qt.GetSignal(ptr, "tracksModel"); signal != nil {
		return std_core.PointerFromQAbstractItemModel((*(*func() *std_core.QAbstractItemModel)(signal))())
	}

	return std_core.PointerFromQAbstractItemModel(NewQmlBridgeFromPointer(ptr).TracksModelDefault())
}

func (ptr *qmlBridge) ConnectTracksModel(f func() *std_core.QAbstractItemModel) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "tracksModel"); signal != nil {
			f := func() *std_core.QAbstractItemModel {
				(*(*func() *std_core.QAbstractItemModel)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "tracksModel", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "tracksModel", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectTracksModel() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "tracksModel")
	}
}

func (ptr *qmlBridge) TracksModel() *std_core.QAbstractItemModel {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQAbstractItemModelFromPointer(C.qmlBridge027749_TracksModelb23e62(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) TracksModelDefault() *std_core.QAbstractItemModel {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQAbstractItemModelFromPointer(C.qmlBridge027749_TracksModelDefault(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

//export callbackqmlBridge027749_SetTracksModel
func callbackqmlBridge027749_SetTracksModel(ptr unsafe.Pointer, tracksModel unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "setTracksModel"); signal != nil {
		(*(*func(*std_core.QAbstractItemModel))(signal))(std_core.NewQAbstractItemModelFromPointer(tracksModel))
	} else {
		NewQmlBridgeFromPointer(ptr).SetTracksModelDefault(std_core.NewQAbstractItemModelFromPointer(tracksModel))
	}
}

func (ptr *qmlBridge) ConnectSetTracksModel(f func(tracksModel *std_core.QAbstractItemModel)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setTracksModel"); signal != nil {
			f := func(tracksModel *std_core.QAbstractItemModel) {
				(*(*func(*std_core.QAbstractItemModel))(signal))(tracksModel)
				f(tracksModel)
			}
			qt.ConnectSignal(ptr.Pointer(), "setTracksModel", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setTracksModel", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectSetTracksModel() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setTracksModel")
	}
}

func (ptr *qmlBridge) SetTracksModel(tracksModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetTracksModel(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(tracksModel))
	}
}

func (ptr *qmlBridge) SetTracksModelDefault(tracksModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetTracksModelDefault(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(tracksModel))
	}
}

//export callbackqmlBridge027749_TracksModelChanged
func callbackqmlBridge027749_TracksModelChanged(ptr unsafe.Pointer, tracksModel unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "tracksModelChanged"); signal != nil {
		(*(*func(*std_core.QAbstractItemModel))(signal))(std_core.NewQAbstractItemModelFromPointer(tracksModel))
	}

}

func (ptr *qmlBridge) ConnectTracksModelChanged(f func(tracksModel *std_core.QAbstractItemModel)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "tracksModelChanged") {
			C.qmlBridge027749_ConnectTracksModelChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "tracksModelChanged"); signal != nil {
			f := func(tracksModel *std_core.QAbstractItemModel) {
				(*(*func(*std_core.QAbstractItemModel))(signal))(tracksModel)
				f(tracksModel)
			}
			qt.ConnectSignal(ptr.Pointer(), "tracksModelChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "tracksModelChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectTracksModelChanged() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectTracksModelChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "tracksModelChanged")
	}
}

func (ptr *qmlBridge) TracksModelChanged(tracksModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_TracksModelChanged(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(tracksModel))
	}
}

//export callbackqmlBridge027749_AlbumsModelb23e62
func callbackqmlBridge027749_AlbumsModelb23e62(ptr unsafe.Pointer) unsafe.Pointer {
	if signal := qt.GetSignal(ptr, "albumsModel"); signal != nil {
		return std_core.PointerFromQAbstractItemModel((*(*func() *std_core.QAbstractItemModel)(signal))())
	}

	return std_core.PointerFromQAbstractItemModel(NewQmlBridgeFromPointer(ptr).AlbumsModelDefault())
}

func (ptr *qmlBridge) ConnectAlbumsModel(f func() *std_core.QAbstractItemModel) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "albumsModel"); signal != nil {
			f := func() *std_core.QAbstractItemModel {
				(*(*func() *std_core.QAbstractItemModel)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "albumsModel", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "albumsModel", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectAlbumsModel() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "albumsModel")
	}
}

func (ptr *qmlBridge) AlbumsModel() *std_core.QAbstractItemModel {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQAbstractItemModelFromPointer(C.qmlBridge027749_AlbumsModelb23e62(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) AlbumsModelDefault() *std_core.QAbstractItemModel {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQAbstractItemModelFromPointer(C.qmlBridge027749_AlbumsModelDefault(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

//export callbackqmlBridge027749_SetAlbumsModel
func callbackqmlBridge027749_SetAlbumsModel(ptr unsafe.Pointer, albumsModel unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "setAlbumsModel"); signal != nil {
		(*(*func(*std_core.QAbstractItemModel))(signal))(std_core.NewQAbstractItemModelFromPointer(albumsModel))
	} else {
		NewQmlBridgeFromPointer(ptr).SetAlbumsModelDefault(std_core.NewQAbstractItemModelFromPointer(albumsModel))
	}
}

func (ptr *qmlBridge) ConnectSetAlbumsModel(f func(albumsModel *std_core.QAbstractItemModel)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setAlbumsModel"); signal != nil {
			f := func(albumsModel *std_core.QAbstractItemModel) {
				(*(*func(*std_core.QAbstractItemModel))(signal))(albumsModel)
				f(albumsModel)
			}
			qt.ConnectSignal(ptr.Pointer(), "setAlbumsModel", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setAlbumsModel", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectSetAlbumsModel() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setAlbumsModel")
	}
}

func (ptr *qmlBridge) SetAlbumsModel(albumsModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetAlbumsModel(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(albumsModel))
	}
}

func (ptr *qmlBridge) SetAlbumsModelDefault(albumsModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetAlbumsModelDefault(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(albumsModel))
	}
}

//export callbackqmlBridge027749_AlbumsModelChanged
func callbackqmlBridge027749_AlbumsModelChanged(ptr unsafe.Pointer, albumsModel unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "albumsModelChanged"); signal != nil {
		(*(*func(*std_core.QAbstractItemModel))(signal))(std_core.NewQAbstractItemModelFromPointer(albumsModel))
	}

}

func (ptr *qmlBridge) ConnectAlbumsModelChanged(f func(albumsModel *std_core.QAbstractItemModel)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "albumsModelChanged") {
			C.qmlBridge027749_ConnectAlbumsModelChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "albumsModelChanged"); signal != nil {
			f := func(albumsModel *std_core.QAbstractItemModel) {
				(*(*func(*std_core.QAbstractItemModel))(signal))(albumsModel)
				f(albumsModel)
			}
			qt.ConnectSignal(ptr.Pointer(), "albumsModelChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "albumsModelChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectAlbumsModelChanged() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectAlbumsModelChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "albumsModelChanged")
	}
}

func (ptr *qmlBridge) AlbumsModelChanged(albumsModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_AlbumsModelChanged(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(albumsModel))
	}
}

//export callbackqmlBridge027749_FavouritesModelb23e62
func callbackqmlBridge027749_FavouritesModelb23e62(ptr unsafe.Pointer) unsafe.Pointer {
	if signal := qt.GetSignal(ptr, "favouritesModel"); signal != nil {
		return std_core.PointerFromQAbstractItemModel((*(*func() *std_core.QAbstractItemModel)(signal))())
	}

	return std_core.PointerFromQAbstractItemModel(NewQmlBridgeFromPointer(ptr).FavouritesModelDefault())
}

func (ptr *qmlBridge) ConnectFavouritesModel(f func() *std_core.QAbstractItemModel) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "favouritesModel"); signal != nil {
			f := func() *std_core.QAbstractItemModel {
				(*(*func() *std_core.QAbstractItemModel)(signal))()
				return f()
			}
			qt.ConnectSignal(ptr.Pointer(), "favouritesModel", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "favouritesModel", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectFavouritesModel() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "favouritesModel")
	}
}

func (ptr *qmlBridge) FavouritesModel() *std_core.QAbstractItemModel {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQAbstractItemModelFromPointer(C.qmlBridge027749_FavouritesModelb23e62(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) FavouritesModelDefault() *std_core.QAbstractItemModel {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQAbstractItemModelFromPointer(C.qmlBridge027749_FavouritesModelDefault(ptr.Pointer()))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

//export callbackqmlBridge027749_SetFavouritesModel
func callbackqmlBridge027749_SetFavouritesModel(ptr unsafe.Pointer, favouritesModel unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "setFavouritesModel"); signal != nil {
		(*(*func(*std_core.QAbstractItemModel))(signal))(std_core.NewQAbstractItemModelFromPointer(favouritesModel))
	} else {
		NewQmlBridgeFromPointer(ptr).SetFavouritesModelDefault(std_core.NewQAbstractItemModelFromPointer(favouritesModel))
	}
}

func (ptr *qmlBridge) ConnectSetFavouritesModel(f func(favouritesModel *std_core.QAbstractItemModel)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "setFavouritesModel"); signal != nil {
			f := func(favouritesModel *std_core.QAbstractItemModel) {
				(*(*func(*std_core.QAbstractItemModel))(signal))(favouritesModel)
				f(favouritesModel)
			}
			qt.ConnectSignal(ptr.Pointer(), "setFavouritesModel", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setFavouritesModel", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectSetFavouritesModel() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "setFavouritesModel")
	}
}

func (ptr *qmlBridge) SetFavouritesModel(favouritesModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetFavouritesModel(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(favouritesModel))
	}
}

func (ptr *qmlBridge) SetFavouritesModelDefault(favouritesModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_SetFavouritesModelDefault(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(favouritesModel))
	}
}

//export callbackqmlBridge027749_FavouritesModelChanged
func callbackqmlBridge027749_FavouritesModelChanged(ptr unsafe.Pointer, favouritesModel unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "favouritesModelChanged"); signal != nil {
		(*(*func(*std_core.QAbstractItemModel))(signal))(std_core.NewQAbstractItemModelFromPointer(favouritesModel))
	}

}

func (ptr *qmlBridge) ConnectFavouritesModelChanged(f func(favouritesModel *std_core.QAbstractItemModel)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "favouritesModelChanged") {
			C.qmlBridge027749_ConnectFavouritesModelChanged(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "favouritesModelChanged"); signal != nil {
			f := func(favouritesModel *std_core.QAbstractItemModel) {
				(*(*func(*std_core.QAbstractItemModel))(signal))(favouritesModel)
				f(favouritesModel)
			}
			qt.ConnectSignal(ptr.Pointer(), "favouritesModelChanged", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "favouritesModelChanged", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectFavouritesModelChanged() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectFavouritesModelChanged(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "favouritesModelChanged")
	}
}

func (ptr *qmlBridge) FavouritesModelChanged(favouritesModel std_core.QAbstractItemModel_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_FavouritesModelChanged(ptr.Pointer(), std_core.PointerFromQAbstractItemModel(favouritesModel))
	}
}

func qmlBridge_QRegisterMetaType() int {
	return int(int32(C.qmlBridge027749_qmlBridge027749_QRegisterMetaType()))
}

func (ptr *qmlBridge) QRegisterMetaType() int {
	return int(int32(C.qmlBridge027749_qmlBridge027749_QRegisterMetaType()))
}

func qmlBridge_QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.qmlBridge027749_qmlBridge027749_QRegisterMetaType2(typeNameC)))
}

func (ptr *qmlBridge) QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.qmlBridge027749_qmlBridge027749_QRegisterMetaType2(typeNameC)))
}

func qmlBridge_QmlRegisterType() int {
	return int(int32(C.qmlBridge027749_qmlBridge027749_QmlRegisterType()))
}

func (ptr *qmlBridge) QmlRegisterType() int {
	return int(int32(C.qmlBridge027749_qmlBridge027749_QmlRegisterType()))
}

func qmlBridge_QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.qmlBridge027749_qmlBridge027749_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *qmlBridge) QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.qmlBridge027749_qmlBridge027749_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *qmlBridge) __children_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.qmlBridge027749___children_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) __children_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749___children_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *qmlBridge) __children_newList() unsafe.Pointer {
	return C.qmlBridge027749___children_newList(ptr.Pointer())
}

func (ptr *qmlBridge) __dynamicPropertyNames_atList(i int) *std_core.QByteArray {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQByteArrayFromPointer(C.qmlBridge027749___dynamicPropertyNames_atList(ptr.Pointer(), C.int(int32(i))))
		runtime.SetFinalizer(tmpValue, (*std_core.QByteArray).DestroyQByteArray)
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) __dynamicPropertyNames_setList(i std_core.QByteArray_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749___dynamicPropertyNames_setList(ptr.Pointer(), std_core.PointerFromQByteArray(i))
	}
}

func (ptr *qmlBridge) __dynamicPropertyNames_newList() unsafe.Pointer {
	return C.qmlBridge027749___dynamicPropertyNames_newList(ptr.Pointer())
}

func (ptr *qmlBridge) __findChildren_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.qmlBridge027749___findChildren_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) __findChildren_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749___findChildren_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *qmlBridge) __findChildren_newList() unsafe.Pointer {
	return C.qmlBridge027749___findChildren_newList(ptr.Pointer())
}

func (ptr *qmlBridge) __findChildren_atList3(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.qmlBridge027749___findChildren_atList3(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) __findChildren_setList3(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749___findChildren_setList3(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *qmlBridge) __findChildren_newList3() unsafe.Pointer {
	return C.qmlBridge027749___findChildren_newList3(ptr.Pointer())
}

func (ptr *qmlBridge) __qFindChildren_atList2(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.qmlBridge027749___qFindChildren_atList2(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *qmlBridge) __qFindChildren_setList2(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749___qFindChildren_setList2(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *qmlBridge) __qFindChildren_newList2() unsafe.Pointer {
	return C.qmlBridge027749___qFindChildren_newList2(ptr.Pointer())
}

func NewQmlBridge(parent std_core.QObject_ITF) *qmlBridge {
	tmpValue := NewQmlBridgeFromPointer(C.qmlBridge027749_NewQmlBridge(std_core.PointerFromQObject(parent)))
	if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
		tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
	}
	return tmpValue
}

//export callbackqmlBridge027749_DestroyQmlBridge
func callbackqmlBridge027749_DestroyQmlBridge(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "~qmlBridge"); signal != nil {
		(*(*func())(signal))()
	} else {
		NewQmlBridgeFromPointer(ptr).DestroyQmlBridgeDefault()
	}
}

func (ptr *qmlBridge) ConnectDestroyQmlBridge(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "~qmlBridge"); signal != nil {
			f := func() {
				(*(*func())(signal))()
				f()
			}
			qt.ConnectSignal(ptr.Pointer(), "~qmlBridge", unsafe.Pointer(&f))
		} else {
			qt.ConnectSignal(ptr.Pointer(), "~qmlBridge", unsafe.Pointer(&f))
		}
	}
}

func (ptr *qmlBridge) DisconnectDestroyQmlBridge() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "~qmlBridge")
	}
}

func (ptr *qmlBridge) DestroyQmlBridge() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DestroyQmlBridge(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

func (ptr *qmlBridge) DestroyQmlBridgeDefault() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DestroyQmlBridgeDefault(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackqmlBridge027749_ChildEvent
func callbackqmlBridge027749_ChildEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "childEvent"); signal != nil {
		(*(*func(*std_core.QChildEvent))(signal))(std_core.NewQChildEventFromPointer(event))
	} else {
		NewQmlBridgeFromPointer(ptr).ChildEventDefault(std_core.NewQChildEventFromPointer(event))
	}
}

func (ptr *qmlBridge) ChildEventDefault(event std_core.QChildEvent_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_ChildEventDefault(ptr.Pointer(), std_core.PointerFromQChildEvent(event))
	}
}

//export callbackqmlBridge027749_ConnectNotify
func callbackqmlBridge027749_ConnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "connectNotify"); signal != nil {
		(*(*func(*std_core.QMetaMethod))(signal))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewQmlBridgeFromPointer(ptr).ConnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *qmlBridge) ConnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_ConnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackqmlBridge027749_CustomEvent
func callbackqmlBridge027749_CustomEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "customEvent"); signal != nil {
		(*(*func(*std_core.QEvent))(signal))(std_core.NewQEventFromPointer(event))
	} else {
		NewQmlBridgeFromPointer(ptr).CustomEventDefault(std_core.NewQEventFromPointer(event))
	}
}

func (ptr *qmlBridge) CustomEventDefault(event std_core.QEvent_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_CustomEventDefault(ptr.Pointer(), std_core.PointerFromQEvent(event))
	}
}

//export callbackqmlBridge027749_DeleteLater
func callbackqmlBridge027749_DeleteLater(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "deleteLater"); signal != nil {
		(*(*func())(signal))()
	} else {
		NewQmlBridgeFromPointer(ptr).DeleteLaterDefault()
	}
}

func (ptr *qmlBridge) DeleteLaterDefault() {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DeleteLaterDefault(ptr.Pointer())
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackqmlBridge027749_Destroyed
func callbackqmlBridge027749_Destroyed(ptr unsafe.Pointer, obj unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "destroyed"); signal != nil {
		(*(*func(*std_core.QObject))(signal))(std_core.NewQObjectFromPointer(obj))
	}
	qt.Unregister(ptr)

}

//export callbackqmlBridge027749_DisconnectNotify
func callbackqmlBridge027749_DisconnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "disconnectNotify"); signal != nil {
		(*(*func(*std_core.QMetaMethod))(signal))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewQmlBridgeFromPointer(ptr).DisconnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *qmlBridge) DisconnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_DisconnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackqmlBridge027749_Event
func callbackqmlBridge027749_Event(ptr unsafe.Pointer, e unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "event"); signal != nil {
		return C.char(int8(qt.GoBoolToInt((*(*func(*std_core.QEvent) bool)(signal))(std_core.NewQEventFromPointer(e)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewQmlBridgeFromPointer(ptr).EventDefault(std_core.NewQEventFromPointer(e)))))
}

func (ptr *qmlBridge) EventDefault(e std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.qmlBridge027749_EventDefault(ptr.Pointer(), std_core.PointerFromQEvent(e))) != 0
	}
	return false
}

//export callbackqmlBridge027749_EventFilter
func callbackqmlBridge027749_EventFilter(ptr unsafe.Pointer, watched unsafe.Pointer, event unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "eventFilter"); signal != nil {
		return C.char(int8(qt.GoBoolToInt((*(*func(*std_core.QObject, *std_core.QEvent) bool)(signal))(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewQmlBridgeFromPointer(ptr).EventFilterDefault(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
}

func (ptr *qmlBridge) EventFilterDefault(watched std_core.QObject_ITF, event std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.qmlBridge027749_EventFilterDefault(ptr.Pointer(), std_core.PointerFromQObject(watched), std_core.PointerFromQEvent(event))) != 0
	}
	return false
}

//export callbackqmlBridge027749_ObjectNameChanged
func callbackqmlBridge027749_ObjectNameChanged(ptr unsafe.Pointer, objectName C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "objectNameChanged"); signal != nil {
		(*(*func(string))(signal))(cGoUnpackString(objectName))
	}

}

//export callbackqmlBridge027749_TimerEvent
func callbackqmlBridge027749_TimerEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "timerEvent"); signal != nil {
		(*(*func(*std_core.QTimerEvent))(signal))(std_core.NewQTimerEventFromPointer(event))
	} else {
		NewQmlBridgeFromPointer(ptr).TimerEventDefault(std_core.NewQTimerEventFromPointer(event))
	}
}

func (ptr *qmlBridge) TimerEventDefault(event std_core.QTimerEvent_ITF) {
	if ptr.Pointer() != nil {
		C.qmlBridge027749_TimerEventDefault(ptr.Pointer(), std_core.PointerFromQTimerEvent(event))
	}
}
