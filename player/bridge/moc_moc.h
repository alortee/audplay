/****************************************************************************
** Meta object code from reading C++ file 'moc.cpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'moc.cpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Navigator027749_t {
    QByteArrayData data[7];
    char stringdata0[74];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Navigator027749_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Navigator027749_t qt_meta_stringdata_Navigator027749 = {
    {
QT_MOC_LITERAL(0, 0, 15), // "Navigator027749"
QT_MOC_LITERAL(1, 16, 9), // "navigated"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 4), // "view"
QT_MOC_LITERAL(4, 32, 18), // "currentViewChanged"
QT_MOC_LITERAL(5, 51, 11), // "currentView"
QT_MOC_LITERAL(6, 63, 10) // "navigateTo"

    },
    "Navigator027749\0navigated\0\0view\0"
    "currentViewChanged\0currentView\0"
    "navigateTo"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Navigator027749[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       1,   38, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,
       4,    1,   32,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   35,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // properties: name, type, flags
       5, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       1,

       0        // eod
};

void Navigator027749::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Navigator027749 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->navigated((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->currentViewChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->navigateTo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Navigator027749::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Navigator027749::navigated)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Navigator027749::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Navigator027749::currentViewChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<Navigator027749 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->currentView(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<Navigator027749 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setCurrentView(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Navigator027749::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Navigator027749.data,
    qt_meta_data_Navigator027749,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Navigator027749::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Navigator027749::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Navigator027749.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Navigator027749::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Navigator027749::navigated(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Navigator027749::currentViewChanged(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
struct qt_meta_stringdata_QMedia027749_t {
    QByteArrayData data[25];
    char stringdata0[333];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QMedia027749_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QMedia027749_t qt_meta_stringdata_QMedia027749 = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QMedia027749"
QT_MOC_LITERAL(1, 13, 18), // "playerStateChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 5), // "state"
QT_MOC_LITERAL(4, 39, 19), // "currentMediaChanged"
QT_MOC_LITERAL(5, 59, 5), // "title"
QT_MOC_LITERAL(6, 65, 6), // "artist"
QT_MOC_LITERAL(7, 72, 7), // "picture"
QT_MOC_LITERAL(8, 80, 8), // "filePath"
QT_MOC_LITERAL(9, 89, 9), // "favourite"
QT_MOC_LITERAL(10, 99, 27), // "currentTrackFilePathChanged"
QT_MOC_LITERAL(11, 127, 20), // "currentTrackFilePath"
QT_MOC_LITERAL(12, 148, 22), // "currentPositionChanged"
QT_MOC_LITERAL(13, 171, 15), // "currentPosition"
QT_MOC_LITERAL(14, 187, 22), // "currentDurationChanged"
QT_MOC_LITERAL(15, 210, 15), // "currentDuration"
QT_MOC_LITERAL(16, 226, 14), // "togglePlayback"
QT_MOC_LITERAL(17, 241, 4), // "seek"
QT_MOC_LITERAL(18, 246, 8), // "position"
QT_MOC_LITERAL(19, 255, 9), // "setVolume"
QT_MOC_LITERAL(20, 265, 15), // "playSingleTrack"
QT_MOC_LITERAL(21, 281, 10), // "trackIndex"
QT_MOC_LITERAL(22, 292, 16), // "shuffleAllTracks"
QT_MOC_LITERAL(23, 309, 13), // "previousTrack"
QT_MOC_LITERAL(24, 323, 9) // "nextTrack"

    },
    "QMedia027749\0playerStateChanged\0\0state\0"
    "currentMediaChanged\0title\0artist\0"
    "picture\0filePath\0favourite\0"
    "currentTrackFilePathChanged\0"
    "currentTrackFilePath\0currentPositionChanged\0"
    "currentPosition\0currentDurationChanged\0"
    "currentDuration\0togglePlayback\0seek\0"
    "position\0setVolume\0playSingleTrack\0"
    "trackIndex\0shuffleAllTracks\0previousTrack\0"
    "nextTrack"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QMedia027749[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       3,  110, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       4,    5,   77,    2, 0x06 /* Public */,
      10,    1,   88,    2, 0x06 /* Public */,
      12,    1,   91,    2, 0x06 /* Public */,
      14,    1,   94,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      16,    0,   97,    2, 0x0a /* Public */,
      17,    1,   98,    2, 0x0a /* Public */,
      19,    1,  101,    2, 0x0a /* Public */,
      20,    1,  104,    2, 0x0a /* Public */,
      22,    0,  107,    2, 0x0a /* Public */,
      23,    0,  108,    2, 0x0a /* Public */,
      24,    0,  109,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::Bool,    5,    6,    7,    8,    9,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void, QMetaType::LongLong,   13,
    QMetaType::Void, QMetaType::LongLong,   15,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::LongLong,   18,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      11, QMetaType::QString, 0x00495103,
      13, QMetaType::LongLong, 0x00495103,
      15, QMetaType::LongLong, 0x00495103,

 // properties: notify_signal_id
       2,
       3,
       4,

       0        // eod
};

void QMedia027749::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QMedia027749 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->playerStateChanged((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 1: _t->currentMediaChanged((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< bool(*)>(_a[5]))); break;
        case 2: _t->currentTrackFilePathChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->currentPositionChanged((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 4: _t->currentDurationChanged((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 5: _t->togglePlayback(); break;
        case 6: _t->seek((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 7: _t->setVolume((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 8: _t->playSingleTrack((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 9: _t->shuffleAllTracks(); break;
        case 10: _t->previousTrack(); break;
        case 11: _t->nextTrack(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QMedia027749::*)(qint32 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMedia027749::playerStateChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QMedia027749::*)(QString , QString , QString , QString , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMedia027749::currentMediaChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QMedia027749::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMedia027749::currentTrackFilePathChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QMedia027749::*)(qint64 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMedia027749::currentPositionChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QMedia027749::*)(qint64 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMedia027749::currentDurationChanged)) {
                *result = 4;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QMedia027749 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->currentTrackFilePath(); break;
        case 1: *reinterpret_cast< qint64*>(_v) = _t->currentPosition(); break;
        case 2: *reinterpret_cast< qint64*>(_v) = _t->currentDuration(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QMedia027749 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setCurrentTrackFilePath(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setCurrentPosition(*reinterpret_cast< qint64*>(_v)); break;
        case 2: _t->setCurrentDuration(*reinterpret_cast< qint64*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QMedia027749::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QMedia027749.data,
    qt_meta_data_QMedia027749,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QMedia027749::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QMedia027749::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QMedia027749.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QMedia027749::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QMedia027749::playerStateChanged(qint32 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QMedia027749::currentMediaChanged(QString _t1, QString _t2, QString _t3, QString _t4, bool _t5)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t4))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t5))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QMedia027749::currentTrackFilePathChanged(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QMedia027749::currentPositionChanged(qint64 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QMedia027749::currentDurationChanged(qint64 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
struct qt_meta_stringdata_qmlBridge027749_t {
    QByteArrayData data[28];
    char stringdata0[404];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_qmlBridge027749_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_qmlBridge027749_t qt_meta_stringdata_qmlBridge027749 = {
    {
QT_MOC_LITERAL(0, 0, 15), // "qmlBridge027749"
QT_MOC_LITERAL(1, 16, 18), // "folderScanComplete"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 9), // "scanCount"
QT_MOC_LITERAL(4, 46, 13), // "trackImported"
QT_MOC_LITERAL(5, 60, 10), // "trackTitle"
QT_MOC_LITERAL(6, 71, 19), // "trackImportComplete"
QT_MOC_LITERAL(7, 91, 16), // "navigatorChanged"
QT_MOC_LITERAL(8, 108, 16), // "Navigator027749*"
QT_MOC_LITERAL(9, 125, 9), // "navigator"
QT_MOC_LITERAL(10, 135, 13), // "qMediaChanged"
QT_MOC_LITERAL(11, 149, 13), // "QMedia027749*"
QT_MOC_LITERAL(12, 163, 6), // "qMedia"
QT_MOC_LITERAL(13, 170, 17), // "thumbsPathChanged"
QT_MOC_LITERAL(14, 188, 10), // "thumbsPath"
QT_MOC_LITERAL(15, 199, 18), // "tracksModelChanged"
QT_MOC_LITERAL(16, 218, 19), // "QAbstractItemModel*"
QT_MOC_LITERAL(17, 238, 11), // "tracksModel"
QT_MOC_LITERAL(18, 250, 18), // "albumsModelChanged"
QT_MOC_LITERAL(19, 269, 11), // "albumsModel"
QT_MOC_LITERAL(20, 281, 22), // "favouritesModelChanged"
QT_MOC_LITERAL(21, 304, 15), // "favouritesModel"
QT_MOC_LITERAL(22, 320, 10), // "addFolders"
QT_MOC_LITERAL(23, 331, 7), // "folders"
QT_MOC_LITERAL(24, 339, 12), // "importTracks"
QT_MOC_LITERAL(25, 352, 21), // "updateTracksModelData"
QT_MOC_LITERAL(26, 374, 20), // "toggleTrackFavourite"
QT_MOC_LITERAL(27, 395, 8) // "filepath"

    },
    "qmlBridge027749\0folderScanComplete\0\0"
    "scanCount\0trackImported\0trackTitle\0"
    "trackImportComplete\0navigatorChanged\0"
    "Navigator027749*\0navigator\0qMediaChanged\0"
    "QMedia027749*\0qMedia\0thumbsPathChanged\0"
    "thumbsPath\0tracksModelChanged\0"
    "QAbstractItemModel*\0tracksModel\0"
    "albumsModelChanged\0albumsModel\0"
    "favouritesModelChanged\0favouritesModel\0"
    "addFolders\0folders\0importTracks\0"
    "updateTracksModelData\0toggleTrackFavourite\0"
    "filepath"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_qmlBridge027749[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       6,  112, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x06 /* Public */,
       4,    1,   82,    2, 0x06 /* Public */,
       6,    0,   85,    2, 0x06 /* Public */,
       7,    1,   86,    2, 0x06 /* Public */,
      10,    1,   89,    2, 0x06 /* Public */,
      13,    1,   92,    2, 0x06 /* Public */,
      15,    1,   95,    2, 0x06 /* Public */,
      18,    1,   98,    2, 0x06 /* Public */,
      20,    1,  101,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      22,    1,  104,    2, 0x0a /* Public */,
      24,    0,  107,    2, 0x0a /* Public */,
      25,    0,  108,    2, 0x0a /* Public */,
      26,    1,  109,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 16,   19,
    QMetaType::Void, 0x80000000 | 16,   21,

 // slots: parameters
    QMetaType::Void, QMetaType::QStringList,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   27,

 // properties: name, type, flags
       9, 0x80000000 | 8, 0x0049510b,
      12, 0x80000000 | 11, 0x0049510b,
      14, QMetaType::QString, 0x00495103,
      17, 0x80000000 | 16, 0x0049510b,
      19, 0x80000000 | 16, 0x0049510b,
      21, 0x80000000 | 16, 0x0049510b,

 // properties: notify_signal_id
       3,
       4,
       5,
       6,
       7,
       8,

       0        // eod
};

void qmlBridge027749::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<qmlBridge027749 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->folderScanComplete((*reinterpret_cast< qint32(*)>(_a[1]))); break;
        case 1: _t->trackImported((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->trackImportComplete(); break;
        case 3: _t->navigatorChanged((*reinterpret_cast< Navigator027749*(*)>(_a[1]))); break;
        case 4: _t->qMediaChanged((*reinterpret_cast< QMedia027749*(*)>(_a[1]))); break;
        case 5: _t->thumbsPathChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->tracksModelChanged((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 7: _t->albumsModelChanged((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 8: _t->favouritesModelChanged((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 9: _t->addFolders((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 10: _t->importTracks(); break;
        case 11: _t->updateTracksModelData(); break;
        case 12: _t->toggleTrackFavourite((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Navigator027749* >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMedia027749* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (qmlBridge027749::*)(qint32 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::folderScanComplete)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::trackImported)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::trackImportComplete)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)(Navigator027749 * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::navigatorChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)(QMedia027749 * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::qMediaChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::thumbsPathChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)(QAbstractItemModel * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::tracksModelChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)(QAbstractItemModel * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::albumsModelChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (qmlBridge027749::*)(QAbstractItemModel * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&qmlBridge027749::favouritesModelChanged)) {
                *result = 8;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Navigator027749* >(); break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMedia027749* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<qmlBridge027749 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Navigator027749**>(_v) = _t->navigator(); break;
        case 1: *reinterpret_cast< QMedia027749**>(_v) = _t->qMedia(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->thumbsPath(); break;
        case 3: *reinterpret_cast< QAbstractItemModel**>(_v) = _t->tracksModel(); break;
        case 4: *reinterpret_cast< QAbstractItemModel**>(_v) = _t->albumsModel(); break;
        case 5: *reinterpret_cast< QAbstractItemModel**>(_v) = _t->favouritesModel(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<qmlBridge027749 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setNavigator(*reinterpret_cast< Navigator027749**>(_v)); break;
        case 1: _t->setQMedia(*reinterpret_cast< QMedia027749**>(_v)); break;
        case 2: _t->setThumbsPath(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setTracksModel(*reinterpret_cast< QAbstractItemModel**>(_v)); break;
        case 4: _t->setAlbumsModel(*reinterpret_cast< QAbstractItemModel**>(_v)); break;
        case 5: _t->setFavouritesModel(*reinterpret_cast< QAbstractItemModel**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject qmlBridge027749::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_qmlBridge027749.data,
    qt_meta_data_qmlBridge027749,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *qmlBridge027749::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *qmlBridge027749::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_qmlBridge027749.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int qmlBridge027749::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void qmlBridge027749::folderScanComplete(qint32 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void qmlBridge027749::trackImported(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void qmlBridge027749::trackImportComplete()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void qmlBridge027749::navigatorChanged(Navigator027749 * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void qmlBridge027749::qMediaChanged(QMedia027749 * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void qmlBridge027749::thumbsPathChanged(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void qmlBridge027749::tracksModelChanged(QAbstractItemModel * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void qmlBridge027749::albumsModelChanged(QAbstractItemModel * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void qmlBridge027749::favouritesModelChanged(QAbstractItemModel * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
