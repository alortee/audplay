package bridge

import "github.com/therecipe/qt/core"

type Navigator struct {
	core.QObject

	// props
	_ string `property:"currentView"`

	// slot
	_ func(view string) `slot:"navigateTo,auto"`

	// signal
	_ func(view string) `signal:"navigated"`
}

func (n *Navigator) navigateTo(view string) {
	if n.CurrentView() != view {
		n.SetCurrentView(view)
		n.Navigated(view)
	}
}
