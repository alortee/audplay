package bridge

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/multimedia"
)

type QMedia struct {
	core.QObject

	audioPlayer *multimedia.QMediaPlayer
	playlists   map[string]*multimedia.QMediaPlaylist

	_ func() `constructor:"init"`

	// props
	_ string `property:"currentTrackFilePath"`
	_ int64  `property:"currentPosition"`
	_ int64  `property:"currentDuration"`

	// slots
	_ func()               `slot:"togglePlayback,auto"`
	_ func(position int64) `slot:"seek,auto"`
	_ func(position int)   `slot:"setVolume,auto"`
	_ func(trackIndex int) `slot:"playSingleTrack,auto"`
	_ func()               `slot:"shuffleAllTracks,auto"`
	_ func()               `slot:"previousTrack,auto"`
	_ func()               `slot:"nextTrack,auto"`

	// signals
	_ func(state int)                                               `signal:"playerStateChanged"`
	_ func(title, artist, picture, filePath string, favourite bool) `signal:"currentMediaChanged"`
}

func (q *QMedia) init() {
	q.audioPlayer = multimedia.NewQMediaPlayer(nil, multimedia.QMediaPlayer__StreamPlayback)
	q.playlists = make(map[string]*multimedia.QMediaPlaylist, 10)
	q.AddPlaylist("main")
	q.AddPlaylist("shuffled")
	q.audioPlayer.ConnectCurrentMediaChanged(func(media *multimedia.QMediaContent) {
		filePath := media.CanonicalUrl().ToLocalFile()
		t, err := Engine.Player.GetTrackByFilePath(filePath)
		if err != nil {
			return
		}
		q.SetCurrentTrackFilePath(filePath)
		q.CurrentMediaChanged(t.Title, t.Artist, t.Picture, filePath, t.IsFavourite)
	})
	q.audioPlayer.ConnectMediaStatusChanged(func(status multimedia.QMediaPlayer__MediaStatus) {
		
	})
	q.audioPlayer.ConnectStateChanged(func(state multimedia.QMediaPlayer__State) {
		q.PlayerStateChanged(int(state))
	})
	q.audioPlayer.ConnectPositionChanged(func(position int64) {
		q.SetCurrentPosition(position)
	})
	q.audioPlayer.ConnectDurationChanged(func(duration int64) {
		q.SetCurrentDuration(duration)
	})
}

func (q *QMedia) AddPlaylist(name string) {
	p := multimedia.NewQMediaPlaylist(nil)
	q.playlists[name] = p
}

func (q *QMedia) RemovePlaylist(name string) {
	delete(q.playlists, name)
}

func (q *QMedia) Playlist(name string) *multimedia.QMediaPlaylist {
	return q.playlists[name]
}

// Playback
func (q *QMedia) playSingleTrack(trackIndex int) {
	p := q.Playlist("main")
	if p.IsEmpty() {
		tracks := Engine.Player.Tracks()
		for _, t := range tracks {
			p.AddMedia(multimedia.NewQMediaContent2(core.QUrl_FromLocalFile(t.FilePath)))
		}
	}
	q.audioPlayer.SetPlaylist(p)
	q.audioPlayer.Playlist().SetCurrentIndex(trackIndex)
	q.audioPlayer.Play()
}

func (q *QMedia) shuffleAllTracks() {
	p := q.Playlist("shuffled")
	if p.IsEmpty() {
		tracks := Engine.Player.Tracks()
		for _, t := range tracks {
			p.AddMedia(multimedia.NewQMediaContent2(core.QUrl_FromLocalFile(t.FilePath)))
		}
	}
	q.audioPlayer.SetPlaylist(p)
	q.audioPlayer.Playlist().Shuffle()
	q.audioPlayer.Play()
}

func (q *QMedia) togglePlayback() {
	switch q.audioPlayer.State() {
	case multimedia.QMediaPlayer__PlayingState:
		q.audioPlayer.Pause()
	default:
		q.audioPlayer.Play()
	}
}

func (q *QMedia) seek(position int64) {
	q.audioPlayer.SetPosition(position)
}

func (q *QMedia) setVolume(position int) {
	p := float64(position)
	qAudio := multimedia.QAudio{}
	v1 := qAudio.ConvertVolume(p/100, 2, 0)
	q.audioPlayer.SetVolume(int(v1 * 100))
}

func (q *QMedia) previousTrack() {
	if q.audioPlayer.Position() < 1800 {
		q.audioPlayer.Playlist().Previous()
	} else {
		q.audioPlayer.SetPosition(0)
	}
}

func (q *QMedia) nextTrack() {
	q.audioPlayer.Playlist().Next()
}
