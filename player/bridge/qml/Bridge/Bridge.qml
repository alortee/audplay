pragma Singleton

import QMLBridge 1.0

QMLBridge {
    property string currentTitle: ""
    property string currentArtist: ""
    property string currentPicture: ""
    property string currentFilePath: ""
    property var currentFavourite: false

    property int breakPoint: 1024
    property int rootWidth: 0

    function formatDuration(elapsed) {

        elapsed = elapsed / 1000
        function twoDigits(n) {
            n = n.toString()
            while (n.length < 2) {
                n = '0' + n
            }
            return n
        }
        if (elapsed >= 0) {
            const diff = {};
            diff.days    = twoDigits(Math.floor(elapsed / 86400))
            diff.hours   = twoDigits(Math.floor(elapsed / 3600 % 24))
            diff.minutes = twoDigits(Math.floor(elapsed / 60 % 60))
            diff.seconds = twoDigits(Math.floor(elapsed % 60))
            let message = `${diff.days}:${diff.hours}:${diff.minutes}:${diff.seconds}`
            message = message.replace(/(00:){1,2}/, '')
            return message
        } else {
            return "0"
        }
    }
}