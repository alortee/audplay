import QtQuick 2.7
import QtQuick.Controls 2.5

Button {
	id: control

    property string sideIcon: ""

    leftPadding: 15
    rightPadding: 15

	contentItem: Row {

        spacing: 10

        IconButton {
            visible: control.sideIcon != ""
            color: "#dedede"
            anchors.verticalCenter: parent.verticalCenter
            icon: control.sideIcon || "md-add"
            bold: false
            size: 13
            asText: true
            opacity: control.enabled ? 1.0 : 0.3
        }

        Text {
            anchors.verticalCenter: parent.verticalCenter
            text: control.text
            font.pixelSize: 13
            opacity: control.enabled ? 1.0 : 0.3
            color: "#dedede"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
    }

    background: Rectangle {
        implicitWidth: 0
        implicitHeight: 42
        opacity: control.enabled ? 1 : 0.3
        color: "#413f40"
        radius: 4
    }
}