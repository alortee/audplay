import QtQuick 2.7
import "assets/ionicons.js" as Ionicons

Item {
	id: iconButton

	property var size: 14
	property var asText: false
	property var icon
	property var hoverIcon: icon

	onIconChanged: {
	    text.text = Ionicons.icon[iconButton.icon]
	}

	property var hoverColor: "#fefefe"
	property var color: Qt.lighter("#413f40", 3)

	onColorChanged: {
        text.color = color
    }

	property var bold: true

	signal clicked

	implicitWidth: 16
	implicitHeight: 16

	height: size
	width: size + 1

	Text {
		id: text
		text: Ionicons.icon[iconButton.icon]
	    font.family: "Ionicons"
	    font.pointSize: iconButton.size
		color: iconButton.color
		anchors.centerIn: parent
		horizontalAlignment: Text.AlignHCenter
		verticalAlignment: Text.AlignVCenter
		font.bold: iconButton.bold
	}

	MouseArea {
	    anchors.fill: parent
	    cursorShape: parent.asText ? 0 : Qt.PointingHandCursor
	    hoverEnabled: !parent.asText
	    enabled: !parent.asText
	    onEntered: {
	    	text.text = Ionicons.icon[iconButton.hoverIcon]
	    	text.color = parent.hoverColor
	    }
	    onExited: {
	    	text.text = Ionicons.icon[iconButton.icon]
	    	text.color = parent.color
	    }
	    onClicked: {
	    	if (!parent.asText) {
                iconButton.clicked()
            }
	    }
	}
}