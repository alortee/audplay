import QtQuick 2.7
import QtQuick.Controls 2.5
import Theme 1.0
import Bridge 1.0

Item {
    id: libraryItem
    width: 250
    height: 30

    signal clicked

    property string itemIcon
    property string textContent
    property var active: Bridge.navigator.currentView === textContent

    property var accent: (active || libraryItemMouseArea.containsMouse) ? Theme.active.accent : Theme.active.text

    Rectangle {
        id: selectIndicator
        height: parent.height * 0.5
        width: 3
        color: libraryItem.accent
        visible: active

        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }
    }

    Rectangle {
        id: labelIcon
        height: parent.height
        width: parent.height
        color: "transparent"

        anchors {
            left: parent.left
            leftMargin: 30
        }

        IconButton {
            anchors.verticalCenter: parent.verticalCenter
            icon: itemIcon
            size: 13
            color: libraryItem.accent
            asText: true
        }
    }

    Rectangle {
        id: labelText
        height: parent.height
        color: "transparent"

        anchors {
            right: parent.right
            left: labelIcon.right
        }

        Text {
            anchors.verticalCenter: parent.verticalCenter
            text: textContent
            color: libraryItem.accent
            font.pixelSize: 13
        }
    }

    MouseArea {
        id: libraryItemMouseArea
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onClicked: {
            libraryItem.clicked()
        }
    }
}