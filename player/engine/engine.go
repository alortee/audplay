package engine

import (
	"bytes"
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"github.com/dhowden/tag"
	"github.com/disintegration/imaging"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

type Engine struct {
	Player      *Player
	db          *sql.DB
	appDataDir  string
	appCacheDir string
}

func NewEngine() *Engine {
	return &Engine{
		Player: &Player{},
	}
}

func (e *Engine) Bootstrap() {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	cacheDir, err := os.UserCacheDir()
	if err != nil {
		panic(err)
	}

	appDataDir := path.Join(homeDir, ".audplay")
	appCacheDir := path.Join(cacheDir, "audplay")

	e.appDataDir = appDataDir
	e.appCacheDir = appCacheDir

	if _, err := os.Stat(appDataDir); os.IsNotExist(err) {
		log.Println(os.MkdirAll(appDataDir, 0755))
	}

	log.Println(os.MkdirAll(path.Join(appCacheDir), 0755))
	log.Println(os.MkdirAll(path.Join(appCacheDir, "thumbnails"), 0755))

	dbPath := path.Join(appDataDir, "data.db")
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		panic(err)
	}
	e.db = db

	createTracksTableQuery := `
		CREATE TABLE IF NOT EXISTS tracks (
			id INTEGER PRIMARY KEY,
			title TEXT,
			album TEXT,
			artist TEXT,
			album_artist TEXT,
			genre TEXT,
			track_position INTEGER,
			disc INTEGER,
			picture TEXT,
			file_path TEXT,
			is_favourite INTEGER
		)
	`
	createFoldersTableQuery := `
		CREATE TABLE IF NOT EXISTS folders (
			id INTEGER PRIMARY KEY,
			path TEXT
		)
	`
	createSettingsTableQuery := `
		CREATE TABLE IF NOT EXISTS folders (
			id INTEGER PRIMARY KEY,
			first_run INTEGER
		)
	`
	stmt, err := db.Prepare(createTracksTableQuery)
	if err != nil {
		panic(err)
	}
	_, err = stmt.Exec()
	if err != nil {
		panic(err)
	}
	stmt, err = db.Prepare(createFoldersTableQuery)
	if err != nil {
		panic(err)
	}
	_, err = stmt.Exec()
	if err != nil {
		panic(err)
	}
	stmt, err = db.Prepare(createSettingsTableQuery)
	if err != nil {
		panic(err)
	}
	_, err = stmt.Exec()
	if err != nil {
		panic(err)
	}

	e.loadFoldersFromDb()
	e.loadTracksFromDb()
	e.getAlbums()
	_ = e.Player.RescanFolders()
	e.ImportTracks()
}

func (e *Engine) FirstRun() bool {
	count := 0
	rows, err := e.db.Query("SELECT COUNT(*) as count FROM  folders")
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			panic(err)
		}
	}
	if count < 1 {
		return true
	}
	return false
}

func (e *Engine) ThumbsPath() string {
	return path.Join(e.appCacheDir, "thumbnails")
}

func (e *Engine) processImage(extension string, imageData []byte) (string, error) {
	hasher := md5.New()
	hasher.Write(imageData)
	id := hex.EncodeToString(hasher.Sum(nil)) + "." + extension
	r := bytes.NewReader(imageData)
	image, err := imaging.Decode(r)
	if err != nil {
		return "", err
	}
	resolutions := map[string]int{
		"150x150": 150,
		"500x500": 500,
	}
	for key, value := range resolutions {
		imagePath := path.Join(e.ThumbsPath(), key+"_"+id)
		if _, err := os.Stat(imagePath); os.IsNotExist(err) {
			i := imaging.Resize(image, value, value, imaging.Lanczos)
			err = imaging.Save(i, imagePath)
			if err != nil {
				return "", err
			}
		}
	}
	return id, nil
}

func (e *Engine) newTrack(filePath string, metadata tag.Metadata) *Track {
	trackPosition, _ := metadata.Track()
	disc, _ := metadata.Disc()

	track := &Track{
		Title:         metadata.Title(),
		Album:         metadata.Album(),
		Artist:        metadata.Artist(),
		AlbumArtist:   metadata.AlbumArtist(),
		Genre:         metadata.AlbumArtist(),
		TrackPosition: trackPosition,
		Disc:          disc,
		Duration:      0,
		Picture:       "",
		FilePath:      filePath,
	}
	if track.Title == "" {
		_, filename := filepath.Split(filePath)
		track.Title = strings.Split(filename, ".")[0]
	}
	picture := metadata.Picture()
	if picture != nil {
		id, err := e.processImage(picture.Ext, picture.Data)
		if err == nil {
			track.Picture = id
		}
	}
	return track
}

func (e *Engine) AddFolder(folderPath string) error {
	err := e.Player.addFolder(folderPath)
	if err != nil {
		return err
	}
	e.addFolderToDb(folderPath)
	return nil
}

func (e *Engine) ImportTracks() (chan string, chan error) {
	addedChan := make(chan string, 10)
	errorChan := make(chan error, 10)

	go func() {
		for _, tPath := range e.Player.trackPaths {
			if e.trackInDb(tPath) {
				continue
			}
			meta, err := e.Player.getMetadata(tPath)
			if err != nil {
				errorChan <- err
				continue
			}
			track := e.newTrack(tPath, meta)
			e.addTrackToDb(track)
			e.Player.tracks = append(e.Player.tracks, track)
			addedChan <- track.Title
		}
		e.Player.SortTracks()
		close(addedChan)
		close(errorChan)
	}()
	return addedChan, errorChan
}

func (e *Engine) addFolderToDb(f string) {
	query := `INSERT into folders (path) 
				VALUES (?)`
	stmt, _ := e.db.Prepare(query)
	_, _ = stmt.Exec(f)
}

func (e *Engine) loadFoldersFromDb() {
	tq := `SELECT * FROM folders`
	rows, _ := e.db.Query(tq)
	for rows.Next() {
		p := ""
		_ = rows.Scan(new(int), &p)
		e.Player.folderPaths = append(e.Player.folderPaths, p)
	}
}

func (e *Engine) addTrackToDb(t *Track) {
	query := `INSERT into tracks (title, album, artist, album_artist, genre, track_position, disc, picture, file_path, is_favourite) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
	stmt, _ := e.db.Prepare(query)
	_, _ = stmt.Exec(t.Title, t.Album, t.Artist, t.AlbumArtist, t.Genre, t.TrackPosition, t.Disc, t.Picture, t.FilePath, t.IsFavourite)
}

func (e *Engine) loadTracksFromDb() {
	tq := `SELECT * FROM tracks`
	rows, _ := e.db.Query(tq)
	for rows.Next() {
		var t Track
		_ = rows.Scan(new(int), &t.Title, &t.Album, &t.Artist, &t.AlbumArtist, &t.Genre, &t.TrackPosition, &t.Disc, &t.Picture, &t.FilePath, &t.IsFavourite)
		e.Player.tracks = append(e.Player.tracks, &t)
		e.Player.trackPaths = append(e.Player.trackPaths, t.FilePath)
	}
	e.Player.SortTracks()
}

func (e *Engine) getAlbums() {
	tq := `SELECT album, album_artist, picture FROM tracks group by album, album_artist order by album_artist`
	rows, _ := e.db.Query(tq)
	for rows.Next() {
		var a Album
		_ = rows.Scan( &a.Album, &a.AlbumArtist, &a.Picture)
		e.Player.albums = append(e.Player.albums, &a)
	}
}

func (e *Engine) trackInDb(path string) bool {
	count := 0
	rows, err := e.db.Query("SELECT COUNT(*) as count FROM tracks where file_path = (?)", path)
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			panic(err)
		}
	}
	if count > 0 {
		return true
	}
	return false
}

func (e *Engine) ToggleTrackFavourite(t *Track) error {
	t.IsFavourite = !t.IsFavourite
	tq := `UPDATE tracks SET is_favourite = ? WHERE file_path = ?`
	stmt, err := e.db.Prepare(tq)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(t.IsFavourite, t.FilePath)
	if err != nil {
		return err
	}
	return nil
}