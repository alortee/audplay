package engine

import (
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/dhowden/tag"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type StringSlice []string

func (ss StringSlice) Contains(s string) bool {
	for _, value := range ss {
		if value == s {
			return true
		}
	}
	return false
}

type Player struct {
	folderPaths StringSlice
	trackPaths  StringSlice
	tracks      []*Track
	trackIndex  map[string]int
	albums      []*Album
}

type Track struct {
	Title         string
	Album         string
	Artist        string
	AlbumArtist   string
	Genre         string
	TrackPosition int
	Disc          int
	Duration      int64
	Picture       string
	FilePath      string
	IsFavourite   bool
}

type Album struct {
	Album       string
	AlbumArtist string
	Picture     string
}

type Picture struct {
	mimeType  string
	extension string
	data      []byte
}

func (mp *Player) Tracks() []*Track {
	return mp.tracks
}

func (mp *Player) FolderPaths() []string {
	return mp.folderPaths
}

func (mp *Player) TrackPathsCount() int {
	return len(mp.trackPaths)
}

func (mp *Player) Albums() []*Album {
	return mp.albums
}

func (mp *Player) Favourites() []*Track {
	var ret []*Track
	for _, t := range mp.tracks {
		if t.IsFavourite {
			ret = append(ret, t)
		}
	}
	return ret
}

func (mp *Player) addFolder(folderPath string) error {
	if mp.folderPaths.Contains(folderPath) {
		return nil
	}
	folderPath, err := filepath.Abs(folderPath)
	if err != nil {
		return err
	}
	walkFunc := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			mp.folderPaths = append(mp.folderPaths, path)
		}
		if !info.IsDir() && mp.isAudioExtension(info.Name()) {
			fileType, err := mp.getFileContentType(path)
			if err != nil {
				return err
			}
			if strings.Contains(fileType, "audio") {
				mp.trackPaths = append(mp.trackPaths, path)
			}
		}
		return nil
	}
	return filepath.Walk(folderPath, walkFunc)
}

func (mp *Player) RescanFolders() error {
	walkFunc := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() && !mp.folderPaths.Contains(path) {
			mp.folderPaths = append(mp.folderPaths, path)
		}
		if !info.IsDir() && mp.isAudioExtension(info.Name()) && !mp.trackPaths.Contains(path) {
			fileType, err := mp.getFileContentType(path)
			if err != nil {
				return err
			}
			if strings.Contains(fileType, "audio") {
				mp.trackPaths = append(mp.trackPaths, path)
			}
		}
		return nil
	}
	for _, path := range mp.folderPaths {
		_ = filepath.Walk(path, walkFunc)
	}
	return nil
}

func (mp *Player) AddFolder(folderPath string) error {
	err := mp.addFolder(folderPath)
	if err != nil {
		return err
	}
	return nil
}

func (mp *Player) SortTracks() {
	sort.Slice(mp.tracks, func(i, j int) bool {
		return mp.tracks[i].Title < mp.tracks[j].Title
	})
	index := make(map[string]int)
	for key, t := range mp.tracks {
		index[t.FilePath] = key
	}
	mp.trackIndex = index
}

func (mp *Player) GetTrackByFilePath(filePath string) (*Track, error) {
	if key, ok := mp.trackIndex[filePath]; ok {
		return mp.tracks[key], nil
	}
	return nil, errors.New("track not found")
}

func (mp *Player) isAudioExtension(fileName string) bool {
	extensions := StringSlice{
		".mp3", ".aac", ".ape", ".wv", ".m4a", ".wav", ".aiff", ".aa", ".aax", ".act", ".aiff", ".alac", ".amr", ".ape", ".au", ".awb", ".dct", ".dss", ".dvf", ".flac", ".gsm", ".iklax", ".ivs", ".m4a", ".m4b",
		".m4p", ".mmf", ".mpc", ".msv", ".nmf", ".nsf", ".ogg", ".oga", ".mogg", ".opus", ".ra", ".rm", ".ra", ".raw", ".sln", ".tta", ".voc", ".vox", ".webm", ".wav", ".wma", ".wv", ".8svx",
	}
	ext := strings.ToLower(filepath.Ext(fileName))
	return ext != "" && extensions.Contains(ext)
}

func (mp *Player) getFileContentType(filePath string) (string, error) {
	out, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer out.Close()
	buffer := make([]byte, 512)
	_, err = out.Read(buffer)
	if err != nil {
		return "", err
	}
	contentType := http.DetectContentType(buffer)
	return contentType, nil
}

func (mp *Player) getMetadata(filePath string) (tag.Metadata, error) {
	out, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer out.Close()
	return tag.ReadFrom(out)
}



func (p *Picture) Base64Url() string {
	format := "data:%s;base64,%s"
	mime := p.mimeType
	encoded := base64.StdEncoding.EncodeToString(p.data)
	return fmt.Sprintf(format, mime, encoded)
}
