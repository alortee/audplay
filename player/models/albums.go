package models

import (
	"github.com/therecipe/qt/core"
)

const (
	AAlbum = int(core.Qt__UserRole) + 1<<iota
	AAlbumArtist
	APicture
)

type AlbumsModel struct {
	core.QAbstractListModel
	_ func() `constructor:"init"`
}

func (am *AlbumsModel) init() {
	am.ConnectRowCount(func(parent *core.QModelIndex) int {
		return len(Engine.Player.Albums())
	})
	am.ConnectRoleNames(func() map[int]*core.QByteArray {
		return map[int]*core.QByteArray{
			AAlbum:         core.NewQByteArray2("album", -1),
			AAlbumArtist:   core.NewQByteArray2("albumArtist", -1),
			APicture:       core.NewQByteArray2("picture", -1),
		}
	})
	am.ConnectData(func(index *core.QModelIndex, role int) *core.QVariant {
		if !index.IsValid() {
			return core.NewQVariant()
		}
		if index.Row() >= len(Engine.Player.Albums()) {
			return core.NewQVariant()
		}
		a := Engine.Player.Albums()[index.Row()]

		switch role {
		case AAlbum:
			{
				return core.NewQVariant1(a.Album)
			}
		case AAlbumArtist:
			{
				return core.NewQVariant1(a.AlbumArtist)
			}
		case APicture:
			{
				return core.NewQVariant1(a.Picture)
			}
		default:
			{
				return core.NewQVariant()
			}
		}
	})
}
