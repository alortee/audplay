package models

import (
	"github.com/therecipe/qt/core"
)

const (
	FTitle = int(core.Qt__UserRole) + 1<<iota
	FAlbum
	FArtist
	FAlbumArtist
	FGenre
	FTrackPosition
	FDisc
	FDuration
	FPicture
	FFilePath
	FIsFavourite
)

type FavouritesModel struct {
	core.QAbstractListModel
	_ func() `constructor:"init"`
}

func (fm *FavouritesModel) init() {
	fm.ConnectRowCount(func(parent *core.QModelIndex) int {
		return len(Engine.Player.Favourites())
	})
	fm.ConnectRoleNames(func() map[int]*core.QByteArray {
		return map[int]*core.QByteArray{
			FTitle:         core.NewQByteArray2("title", -1),
			FAlbum:         core.NewQByteArray2("album", -1),
			FArtist:        core.NewQByteArray2("artist", -1),
			FAlbumArtist:   core.NewQByteArray2("albumArtist", -1),
			FGenre:         core.NewQByteArray2("genre", -1),
			FTrackPosition: core.NewQByteArray2("trackPosition", -1),
			FDisc:          core.NewQByteArray2("disc", -1),
			FDuration:      core.NewQByteArray2("duration", -1),
			FPicture:       core.NewQByteArray2("picture", -1),
			FFilePath:      core.NewQByteArray2("filePath", -1),
			FIsFavourite:   core.NewQByteArray2("isFavourite", -1),
		}
	})
	fm.ConnectData(func(index *core.QModelIndex, role int) *core.QVariant {
		if !index.IsValid() {
			return core.NewQVariant()
		}
		if index.Row() >= len(Engine.Player.Favourites()) {
			return core.NewQVariant()
		}
		t := Engine.Player.Favourites()[index.Row()]

		switch role {
		case FTitle:
			{
				return core.NewQVariant1(t.Title)
			}
		case FAlbum:
			{
				return core.NewQVariant1(t.Album)
			}
		case FArtist:
			{
				return core.NewQVariant1(t.Artist)
			}
		case FAlbumArtist:
			{
				return core.NewQVariant1(t.AlbumArtist)
			}
		case FGenre:
			{
				return core.NewQVariant1(t.Genre)
			}
		case FTrackPosition:
			{
				return core.NewQVariant1(t.TrackPosition)
			}
		case FDisc:
			{
				return core.NewQVariant1(t.Disc)
			}
		case FDuration:
			{
				return core.NewQVariant1(t.Duration)
			}
		case FPicture:
			{
				return core.NewQVariant1(t.Picture)
			}
		case FFilePath:
			{
				return core.NewQVariant1(t.FilePath)
			}
		case FIsFavourite:
			{
				return core.NewQVariant1(t.IsFavourite)
			}
		default:
			{
				return core.NewQVariant()
			}
		}
	})
}