

#pragma once

#ifndef GO_MOC_b23e62_H
#define GO_MOC_b23e62_H

#include <stdint.h>

#ifdef __cplusplus
class AlbumsModelb23e62;
void AlbumsModelb23e62_AlbumsModelb23e62_QRegisterMetaTypes();
class FavouritesModelb23e62;
void FavouritesModelb23e62_FavouritesModelb23e62_QRegisterMetaTypes();
class TracksModelb23e62;
void TracksModelb23e62_TracksModelb23e62_QRegisterMetaTypes();
extern "C" {
#endif

struct Moc_PackedString { char* data; long long len; };
struct Moc_PackedList { void* data; long long len; };
int FavouritesModelb23e62_FavouritesModelb23e62_QRegisterMetaType();
int FavouritesModelb23e62_FavouritesModelb23e62_QRegisterMetaType2(char* typeName);
int FavouritesModelb23e62_FavouritesModelb23e62_QmlRegisterType();
int FavouritesModelb23e62_FavouritesModelb23e62_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName);
int FavouritesModelb23e62_____itemData_keyList_atList(void* ptr, int i);
void FavouritesModelb23e62_____itemData_keyList_setList(void* ptr, int i);
void* FavouritesModelb23e62_____itemData_keyList_newList(void* ptr);
int FavouritesModelb23e62_____roleNames_keyList_atList(void* ptr, int i);
void FavouritesModelb23e62_____roleNames_keyList_setList(void* ptr, int i);
void* FavouritesModelb23e62_____roleNames_keyList_newList(void* ptr);
int FavouritesModelb23e62_____setItemData_roles_keyList_atList(void* ptr, int i);
void FavouritesModelb23e62_____setItemData_roles_keyList_setList(void* ptr, int i);
void* FavouritesModelb23e62_____setItemData_roles_keyList_newList(void* ptr);
void* FavouritesModelb23e62___changePersistentIndexList_from_atList(void* ptr, int i);
void FavouritesModelb23e62___changePersistentIndexList_from_setList(void* ptr, void* i);
void* FavouritesModelb23e62___changePersistentIndexList_from_newList(void* ptr);
void* FavouritesModelb23e62___changePersistentIndexList_to_atList(void* ptr, int i);
void FavouritesModelb23e62___changePersistentIndexList_to_setList(void* ptr, void* i);
void* FavouritesModelb23e62___changePersistentIndexList_to_newList(void* ptr);
int FavouritesModelb23e62___dataChanged_roles_atList(void* ptr, int i);
void FavouritesModelb23e62___dataChanged_roles_setList(void* ptr, int i);
void* FavouritesModelb23e62___dataChanged_roles_newList(void* ptr);
void* FavouritesModelb23e62___itemData_atList(void* ptr, int v, int i);
void FavouritesModelb23e62___itemData_setList(void* ptr, int key, void* i);
void* FavouritesModelb23e62___itemData_newList(void* ptr);
struct Moc_PackedList FavouritesModelb23e62___itemData_keyList(void* ptr);
void* FavouritesModelb23e62___layoutAboutToBeChanged_parents_atList(void* ptr, int i);
void FavouritesModelb23e62___layoutAboutToBeChanged_parents_setList(void* ptr, void* i);
void* FavouritesModelb23e62___layoutAboutToBeChanged_parents_newList(void* ptr);
void* FavouritesModelb23e62___layoutChanged_parents_atList(void* ptr, int i);
void FavouritesModelb23e62___layoutChanged_parents_setList(void* ptr, void* i);
void* FavouritesModelb23e62___layoutChanged_parents_newList(void* ptr);
void* FavouritesModelb23e62___match_atList(void* ptr, int i);
void FavouritesModelb23e62___match_setList(void* ptr, void* i);
void* FavouritesModelb23e62___match_newList(void* ptr);
void* FavouritesModelb23e62___mimeData_indexes_atList(void* ptr, int i);
void FavouritesModelb23e62___mimeData_indexes_setList(void* ptr, void* i);
void* FavouritesModelb23e62___mimeData_indexes_newList(void* ptr);
void* FavouritesModelb23e62___persistentIndexList_atList(void* ptr, int i);
void FavouritesModelb23e62___persistentIndexList_setList(void* ptr, void* i);
void* FavouritesModelb23e62___persistentIndexList_newList(void* ptr);
void* FavouritesModelb23e62___roleNames_atList(void* ptr, int v, int i);
void FavouritesModelb23e62___roleNames_setList(void* ptr, int key, void* i);
void* FavouritesModelb23e62___roleNames_newList(void* ptr);
struct Moc_PackedList FavouritesModelb23e62___roleNames_keyList(void* ptr);
void* FavouritesModelb23e62___setItemData_roles_atList(void* ptr, int v, int i);
void FavouritesModelb23e62___setItemData_roles_setList(void* ptr, int key, void* i);
void* FavouritesModelb23e62___setItemData_roles_newList(void* ptr);
struct Moc_PackedList FavouritesModelb23e62___setItemData_roles_keyList(void* ptr);
int FavouritesModelb23e62_____doSetRoleNames_roleNames_keyList_atList(void* ptr, int i);
void FavouritesModelb23e62_____doSetRoleNames_roleNames_keyList_setList(void* ptr, int i);
void* FavouritesModelb23e62_____doSetRoleNames_roleNames_keyList_newList(void* ptr);
int FavouritesModelb23e62_____setRoleNames_roleNames_keyList_atList(void* ptr, int i);
void FavouritesModelb23e62_____setRoleNames_roleNames_keyList_setList(void* ptr, int i);
void* FavouritesModelb23e62_____setRoleNames_roleNames_keyList_newList(void* ptr);
void* FavouritesModelb23e62___children_atList(void* ptr, int i);
void FavouritesModelb23e62___children_setList(void* ptr, void* i);
void* FavouritesModelb23e62___children_newList(void* ptr);
void* FavouritesModelb23e62___dynamicPropertyNames_atList(void* ptr, int i);
void FavouritesModelb23e62___dynamicPropertyNames_setList(void* ptr, void* i);
void* FavouritesModelb23e62___dynamicPropertyNames_newList(void* ptr);
void* FavouritesModelb23e62___findChildren_atList(void* ptr, int i);
void FavouritesModelb23e62___findChildren_setList(void* ptr, void* i);
void* FavouritesModelb23e62___findChildren_newList(void* ptr);
void* FavouritesModelb23e62___findChildren_atList3(void* ptr, int i);
void FavouritesModelb23e62___findChildren_setList3(void* ptr, void* i);
void* FavouritesModelb23e62___findChildren_newList3(void* ptr);
void* FavouritesModelb23e62___qFindChildren_atList2(void* ptr, int i);
void FavouritesModelb23e62___qFindChildren_setList2(void* ptr, void* i);
void* FavouritesModelb23e62___qFindChildren_newList2(void* ptr);
void* FavouritesModelb23e62_NewFavouritesModel(void* parent);
void FavouritesModelb23e62_DestroyFavouritesModel(void* ptr);
void FavouritesModelb23e62_DestroyFavouritesModelDefault(void* ptr);
char FavouritesModelb23e62_DropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent);
long long FavouritesModelb23e62_FlagsDefault(void* ptr, void* index);
void* FavouritesModelb23e62_IndexDefault(void* ptr, int row, int column, void* parent);
void* FavouritesModelb23e62_SiblingDefault(void* ptr, int row, int column, void* idx);
void* FavouritesModelb23e62_BuddyDefault(void* ptr, void* index);
char FavouritesModelb23e62_CanDropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent);
char FavouritesModelb23e62_CanFetchMoreDefault(void* ptr, void* parent);
int FavouritesModelb23e62_ColumnCountDefault(void* ptr, void* parent);
void* FavouritesModelb23e62_DataDefault(void* ptr, void* index, int role);
void FavouritesModelb23e62_FetchMoreDefault(void* ptr, void* parent);
char FavouritesModelb23e62_HasChildrenDefault(void* ptr, void* parent);
void* FavouritesModelb23e62_HeaderDataDefault(void* ptr, int section, long long orientation, int role);
char FavouritesModelb23e62_InsertColumnsDefault(void* ptr, int column, int count, void* parent);
char FavouritesModelb23e62_InsertRowsDefault(void* ptr, int row, int count, void* parent);
struct Moc_PackedList FavouritesModelb23e62_ItemDataDefault(void* ptr, void* index);
struct Moc_PackedList FavouritesModelb23e62_MatchDefault(void* ptr, void* start, int role, void* value, int hits, long long flags);
void* FavouritesModelb23e62_MimeDataDefault(void* ptr, void* indexes);
struct Moc_PackedString FavouritesModelb23e62_MimeTypesDefault(void* ptr);
char FavouritesModelb23e62_MoveColumnsDefault(void* ptr, void* sourceParent, int sourceColumn, int count, void* destinationParent, int destinationChild);
char FavouritesModelb23e62_MoveRowsDefault(void* ptr, void* sourceParent, int sourceRow, int count, void* destinationParent, int destinationChild);
void* FavouritesModelb23e62_ParentDefault(void* ptr, void* index);
char FavouritesModelb23e62_RemoveColumnsDefault(void* ptr, int column, int count, void* parent);
char FavouritesModelb23e62_RemoveRowsDefault(void* ptr, int row, int count, void* parent);
void FavouritesModelb23e62_ResetInternalDataDefault(void* ptr);
void FavouritesModelb23e62_RevertDefault(void* ptr);
struct Moc_PackedList FavouritesModelb23e62_RoleNamesDefault(void* ptr);
int FavouritesModelb23e62_RowCountDefault(void* ptr, void* parent);
char FavouritesModelb23e62_SetDataDefault(void* ptr, void* index, void* value, int role);
char FavouritesModelb23e62_SetHeaderDataDefault(void* ptr, int section, long long orientation, void* value, int role);
char FavouritesModelb23e62_SetItemDataDefault(void* ptr, void* index, void* roles);
void FavouritesModelb23e62_SortDefault(void* ptr, int column, long long order);
void* FavouritesModelb23e62_SpanDefault(void* ptr, void* index);
char FavouritesModelb23e62_SubmitDefault(void* ptr);
long long FavouritesModelb23e62_SupportedDragActionsDefault(void* ptr);
long long FavouritesModelb23e62_SupportedDropActionsDefault(void* ptr);
void FavouritesModelb23e62_ChildEventDefault(void* ptr, void* event);
void FavouritesModelb23e62_ConnectNotifyDefault(void* ptr, void* sign);
void FavouritesModelb23e62_CustomEventDefault(void* ptr, void* event);
void FavouritesModelb23e62_DeleteLaterDefault(void* ptr);
void FavouritesModelb23e62_DisconnectNotifyDefault(void* ptr, void* sign);
char FavouritesModelb23e62_EventDefault(void* ptr, void* e);
char FavouritesModelb23e62_EventFilterDefault(void* ptr, void* watched, void* event);
void FavouritesModelb23e62_TimerEventDefault(void* ptr, void* event);
int TracksModelb23e62_TracksModelb23e62_QRegisterMetaType();
int TracksModelb23e62_TracksModelb23e62_QRegisterMetaType2(char* typeName);
int TracksModelb23e62_TracksModelb23e62_QmlRegisterType();
int TracksModelb23e62_TracksModelb23e62_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName);
int TracksModelb23e62_____itemData_keyList_atList(void* ptr, int i);
void TracksModelb23e62_____itemData_keyList_setList(void* ptr, int i);
void* TracksModelb23e62_____itemData_keyList_newList(void* ptr);
int TracksModelb23e62_____roleNames_keyList_atList(void* ptr, int i);
void TracksModelb23e62_____roleNames_keyList_setList(void* ptr, int i);
void* TracksModelb23e62_____roleNames_keyList_newList(void* ptr);
int TracksModelb23e62_____setItemData_roles_keyList_atList(void* ptr, int i);
void TracksModelb23e62_____setItemData_roles_keyList_setList(void* ptr, int i);
void* TracksModelb23e62_____setItemData_roles_keyList_newList(void* ptr);
void* TracksModelb23e62___changePersistentIndexList_from_atList(void* ptr, int i);
void TracksModelb23e62___changePersistentIndexList_from_setList(void* ptr, void* i);
void* TracksModelb23e62___changePersistentIndexList_from_newList(void* ptr);
void* TracksModelb23e62___changePersistentIndexList_to_atList(void* ptr, int i);
void TracksModelb23e62___changePersistentIndexList_to_setList(void* ptr, void* i);
void* TracksModelb23e62___changePersistentIndexList_to_newList(void* ptr);
int TracksModelb23e62___dataChanged_roles_atList(void* ptr, int i);
void TracksModelb23e62___dataChanged_roles_setList(void* ptr, int i);
void* TracksModelb23e62___dataChanged_roles_newList(void* ptr);
void* TracksModelb23e62___itemData_atList(void* ptr, int v, int i);
void TracksModelb23e62___itemData_setList(void* ptr, int key, void* i);
void* TracksModelb23e62___itemData_newList(void* ptr);
struct Moc_PackedList TracksModelb23e62___itemData_keyList(void* ptr);
void* TracksModelb23e62___layoutAboutToBeChanged_parents_atList(void* ptr, int i);
void TracksModelb23e62___layoutAboutToBeChanged_parents_setList(void* ptr, void* i);
void* TracksModelb23e62___layoutAboutToBeChanged_parents_newList(void* ptr);
void* TracksModelb23e62___layoutChanged_parents_atList(void* ptr, int i);
void TracksModelb23e62___layoutChanged_parents_setList(void* ptr, void* i);
void* TracksModelb23e62___layoutChanged_parents_newList(void* ptr);
void* TracksModelb23e62___match_atList(void* ptr, int i);
void TracksModelb23e62___match_setList(void* ptr, void* i);
void* TracksModelb23e62___match_newList(void* ptr);
void* TracksModelb23e62___mimeData_indexes_atList(void* ptr, int i);
void TracksModelb23e62___mimeData_indexes_setList(void* ptr, void* i);
void* TracksModelb23e62___mimeData_indexes_newList(void* ptr);
void* TracksModelb23e62___persistentIndexList_atList(void* ptr, int i);
void TracksModelb23e62___persistentIndexList_setList(void* ptr, void* i);
void* TracksModelb23e62___persistentIndexList_newList(void* ptr);
void* TracksModelb23e62___roleNames_atList(void* ptr, int v, int i);
void TracksModelb23e62___roleNames_setList(void* ptr, int key, void* i);
void* TracksModelb23e62___roleNames_newList(void* ptr);
struct Moc_PackedList TracksModelb23e62___roleNames_keyList(void* ptr);
void* TracksModelb23e62___setItemData_roles_atList(void* ptr, int v, int i);
void TracksModelb23e62___setItemData_roles_setList(void* ptr, int key, void* i);
void* TracksModelb23e62___setItemData_roles_newList(void* ptr);
struct Moc_PackedList TracksModelb23e62___setItemData_roles_keyList(void* ptr);
int TracksModelb23e62_____doSetRoleNames_roleNames_keyList_atList(void* ptr, int i);
void TracksModelb23e62_____doSetRoleNames_roleNames_keyList_setList(void* ptr, int i);
void* TracksModelb23e62_____doSetRoleNames_roleNames_keyList_newList(void* ptr);
int TracksModelb23e62_____setRoleNames_roleNames_keyList_atList(void* ptr, int i);
void TracksModelb23e62_____setRoleNames_roleNames_keyList_setList(void* ptr, int i);
void* TracksModelb23e62_____setRoleNames_roleNames_keyList_newList(void* ptr);
void* TracksModelb23e62___children_atList(void* ptr, int i);
void TracksModelb23e62___children_setList(void* ptr, void* i);
void* TracksModelb23e62___children_newList(void* ptr);
void* TracksModelb23e62___dynamicPropertyNames_atList(void* ptr, int i);
void TracksModelb23e62___dynamicPropertyNames_setList(void* ptr, void* i);
void* TracksModelb23e62___dynamicPropertyNames_newList(void* ptr);
void* TracksModelb23e62___findChildren_atList(void* ptr, int i);
void TracksModelb23e62___findChildren_setList(void* ptr, void* i);
void* TracksModelb23e62___findChildren_newList(void* ptr);
void* TracksModelb23e62___findChildren_atList3(void* ptr, int i);
void TracksModelb23e62___findChildren_setList3(void* ptr, void* i);
void* TracksModelb23e62___findChildren_newList3(void* ptr);
void* TracksModelb23e62___qFindChildren_atList2(void* ptr, int i);
void TracksModelb23e62___qFindChildren_setList2(void* ptr, void* i);
void* TracksModelb23e62___qFindChildren_newList2(void* ptr);
void* TracksModelb23e62_NewTracksModel(void* parent);
void TracksModelb23e62_DestroyTracksModel(void* ptr);
void TracksModelb23e62_DestroyTracksModelDefault(void* ptr);
char TracksModelb23e62_DropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent);
long long TracksModelb23e62_FlagsDefault(void* ptr, void* index);
void* TracksModelb23e62_IndexDefault(void* ptr, int row, int column, void* parent);
void* TracksModelb23e62_SiblingDefault(void* ptr, int row, int column, void* idx);
void* TracksModelb23e62_BuddyDefault(void* ptr, void* index);
char TracksModelb23e62_CanDropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent);
char TracksModelb23e62_CanFetchMoreDefault(void* ptr, void* parent);
int TracksModelb23e62_ColumnCountDefault(void* ptr, void* parent);
void* TracksModelb23e62_DataDefault(void* ptr, void* index, int role);
void TracksModelb23e62_FetchMoreDefault(void* ptr, void* parent);
char TracksModelb23e62_HasChildrenDefault(void* ptr, void* parent);
void* TracksModelb23e62_HeaderDataDefault(void* ptr, int section, long long orientation, int role);
char TracksModelb23e62_InsertColumnsDefault(void* ptr, int column, int count, void* parent);
char TracksModelb23e62_InsertRowsDefault(void* ptr, int row, int count, void* parent);
struct Moc_PackedList TracksModelb23e62_ItemDataDefault(void* ptr, void* index);
struct Moc_PackedList TracksModelb23e62_MatchDefault(void* ptr, void* start, int role, void* value, int hits, long long flags);
void* TracksModelb23e62_MimeDataDefault(void* ptr, void* indexes);
struct Moc_PackedString TracksModelb23e62_MimeTypesDefault(void* ptr);
char TracksModelb23e62_MoveColumnsDefault(void* ptr, void* sourceParent, int sourceColumn, int count, void* destinationParent, int destinationChild);
char TracksModelb23e62_MoveRowsDefault(void* ptr, void* sourceParent, int sourceRow, int count, void* destinationParent, int destinationChild);
void* TracksModelb23e62_ParentDefault(void* ptr, void* index);
char TracksModelb23e62_RemoveColumnsDefault(void* ptr, int column, int count, void* parent);
char TracksModelb23e62_RemoveRowsDefault(void* ptr, int row, int count, void* parent);
void TracksModelb23e62_ResetInternalDataDefault(void* ptr);
void TracksModelb23e62_RevertDefault(void* ptr);
struct Moc_PackedList TracksModelb23e62_RoleNamesDefault(void* ptr);
int TracksModelb23e62_RowCountDefault(void* ptr, void* parent);
char TracksModelb23e62_SetDataDefault(void* ptr, void* index, void* value, int role);
char TracksModelb23e62_SetHeaderDataDefault(void* ptr, int section, long long orientation, void* value, int role);
char TracksModelb23e62_SetItemDataDefault(void* ptr, void* index, void* roles);
void TracksModelb23e62_SortDefault(void* ptr, int column, long long order);
void* TracksModelb23e62_SpanDefault(void* ptr, void* index);
char TracksModelb23e62_SubmitDefault(void* ptr);
long long TracksModelb23e62_SupportedDragActionsDefault(void* ptr);
long long TracksModelb23e62_SupportedDropActionsDefault(void* ptr);
void TracksModelb23e62_ChildEventDefault(void* ptr, void* event);
void TracksModelb23e62_ConnectNotifyDefault(void* ptr, void* sign);
void TracksModelb23e62_CustomEventDefault(void* ptr, void* event);
void TracksModelb23e62_DeleteLaterDefault(void* ptr);
void TracksModelb23e62_DisconnectNotifyDefault(void* ptr, void* sign);
char TracksModelb23e62_EventDefault(void* ptr, void* e);
char TracksModelb23e62_EventFilterDefault(void* ptr, void* watched, void* event);
void TracksModelb23e62_TimerEventDefault(void* ptr, void* event);
int AlbumsModelb23e62_AlbumsModelb23e62_QRegisterMetaType();
int AlbumsModelb23e62_AlbumsModelb23e62_QRegisterMetaType2(char* typeName);
int AlbumsModelb23e62_AlbumsModelb23e62_QmlRegisterType();
int AlbumsModelb23e62_AlbumsModelb23e62_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName);
int AlbumsModelb23e62_____itemData_keyList_atList(void* ptr, int i);
void AlbumsModelb23e62_____itemData_keyList_setList(void* ptr, int i);
void* AlbumsModelb23e62_____itemData_keyList_newList(void* ptr);
int AlbumsModelb23e62_____roleNames_keyList_atList(void* ptr, int i);
void AlbumsModelb23e62_____roleNames_keyList_setList(void* ptr, int i);
void* AlbumsModelb23e62_____roleNames_keyList_newList(void* ptr);
int AlbumsModelb23e62_____setItemData_roles_keyList_atList(void* ptr, int i);
void AlbumsModelb23e62_____setItemData_roles_keyList_setList(void* ptr, int i);
void* AlbumsModelb23e62_____setItemData_roles_keyList_newList(void* ptr);
void* AlbumsModelb23e62___changePersistentIndexList_from_atList(void* ptr, int i);
void AlbumsModelb23e62___changePersistentIndexList_from_setList(void* ptr, void* i);
void* AlbumsModelb23e62___changePersistentIndexList_from_newList(void* ptr);
void* AlbumsModelb23e62___changePersistentIndexList_to_atList(void* ptr, int i);
void AlbumsModelb23e62___changePersistentIndexList_to_setList(void* ptr, void* i);
void* AlbumsModelb23e62___changePersistentIndexList_to_newList(void* ptr);
int AlbumsModelb23e62___dataChanged_roles_atList(void* ptr, int i);
void AlbumsModelb23e62___dataChanged_roles_setList(void* ptr, int i);
void* AlbumsModelb23e62___dataChanged_roles_newList(void* ptr);
void* AlbumsModelb23e62___itemData_atList(void* ptr, int v, int i);
void AlbumsModelb23e62___itemData_setList(void* ptr, int key, void* i);
void* AlbumsModelb23e62___itemData_newList(void* ptr);
struct Moc_PackedList AlbumsModelb23e62___itemData_keyList(void* ptr);
void* AlbumsModelb23e62___layoutAboutToBeChanged_parents_atList(void* ptr, int i);
void AlbumsModelb23e62___layoutAboutToBeChanged_parents_setList(void* ptr, void* i);
void* AlbumsModelb23e62___layoutAboutToBeChanged_parents_newList(void* ptr);
void* AlbumsModelb23e62___layoutChanged_parents_atList(void* ptr, int i);
void AlbumsModelb23e62___layoutChanged_parents_setList(void* ptr, void* i);
void* AlbumsModelb23e62___layoutChanged_parents_newList(void* ptr);
void* AlbumsModelb23e62___match_atList(void* ptr, int i);
void AlbumsModelb23e62___match_setList(void* ptr, void* i);
void* AlbumsModelb23e62___match_newList(void* ptr);
void* AlbumsModelb23e62___mimeData_indexes_atList(void* ptr, int i);
void AlbumsModelb23e62___mimeData_indexes_setList(void* ptr, void* i);
void* AlbumsModelb23e62___mimeData_indexes_newList(void* ptr);
void* AlbumsModelb23e62___persistentIndexList_atList(void* ptr, int i);
void AlbumsModelb23e62___persistentIndexList_setList(void* ptr, void* i);
void* AlbumsModelb23e62___persistentIndexList_newList(void* ptr);
void* AlbumsModelb23e62___roleNames_atList(void* ptr, int v, int i);
void AlbumsModelb23e62___roleNames_setList(void* ptr, int key, void* i);
void* AlbumsModelb23e62___roleNames_newList(void* ptr);
struct Moc_PackedList AlbumsModelb23e62___roleNames_keyList(void* ptr);
void* AlbumsModelb23e62___setItemData_roles_atList(void* ptr, int v, int i);
void AlbumsModelb23e62___setItemData_roles_setList(void* ptr, int key, void* i);
void* AlbumsModelb23e62___setItemData_roles_newList(void* ptr);
struct Moc_PackedList AlbumsModelb23e62___setItemData_roles_keyList(void* ptr);
int AlbumsModelb23e62_____doSetRoleNames_roleNames_keyList_atList(void* ptr, int i);
void AlbumsModelb23e62_____doSetRoleNames_roleNames_keyList_setList(void* ptr, int i);
void* AlbumsModelb23e62_____doSetRoleNames_roleNames_keyList_newList(void* ptr);
int AlbumsModelb23e62_____setRoleNames_roleNames_keyList_atList(void* ptr, int i);
void AlbumsModelb23e62_____setRoleNames_roleNames_keyList_setList(void* ptr, int i);
void* AlbumsModelb23e62_____setRoleNames_roleNames_keyList_newList(void* ptr);
void* AlbumsModelb23e62___children_atList(void* ptr, int i);
void AlbumsModelb23e62___children_setList(void* ptr, void* i);
void* AlbumsModelb23e62___children_newList(void* ptr);
void* AlbumsModelb23e62___dynamicPropertyNames_atList(void* ptr, int i);
void AlbumsModelb23e62___dynamicPropertyNames_setList(void* ptr, void* i);
void* AlbumsModelb23e62___dynamicPropertyNames_newList(void* ptr);
void* AlbumsModelb23e62___findChildren_atList(void* ptr, int i);
void AlbumsModelb23e62___findChildren_setList(void* ptr, void* i);
void* AlbumsModelb23e62___findChildren_newList(void* ptr);
void* AlbumsModelb23e62___findChildren_atList3(void* ptr, int i);
void AlbumsModelb23e62___findChildren_setList3(void* ptr, void* i);
void* AlbumsModelb23e62___findChildren_newList3(void* ptr);
void* AlbumsModelb23e62___qFindChildren_atList2(void* ptr, int i);
void AlbumsModelb23e62___qFindChildren_setList2(void* ptr, void* i);
void* AlbumsModelb23e62___qFindChildren_newList2(void* ptr);
void* AlbumsModelb23e62_NewAlbumsModel(void* parent);
void AlbumsModelb23e62_DestroyAlbumsModel(void* ptr);
void AlbumsModelb23e62_DestroyAlbumsModelDefault(void* ptr);
char AlbumsModelb23e62_DropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent);
long long AlbumsModelb23e62_FlagsDefault(void* ptr, void* index);
void* AlbumsModelb23e62_IndexDefault(void* ptr, int row, int column, void* parent);
void* AlbumsModelb23e62_SiblingDefault(void* ptr, int row, int column, void* idx);
void* AlbumsModelb23e62_BuddyDefault(void* ptr, void* index);
char AlbumsModelb23e62_CanDropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent);
char AlbumsModelb23e62_CanFetchMoreDefault(void* ptr, void* parent);
int AlbumsModelb23e62_ColumnCountDefault(void* ptr, void* parent);
void* AlbumsModelb23e62_DataDefault(void* ptr, void* index, int role);
void AlbumsModelb23e62_FetchMoreDefault(void* ptr, void* parent);
char AlbumsModelb23e62_HasChildrenDefault(void* ptr, void* parent);
void* AlbumsModelb23e62_HeaderDataDefault(void* ptr, int section, long long orientation, int role);
char AlbumsModelb23e62_InsertColumnsDefault(void* ptr, int column, int count, void* parent);
char AlbumsModelb23e62_InsertRowsDefault(void* ptr, int row, int count, void* parent);
struct Moc_PackedList AlbumsModelb23e62_ItemDataDefault(void* ptr, void* index);
struct Moc_PackedList AlbumsModelb23e62_MatchDefault(void* ptr, void* start, int role, void* value, int hits, long long flags);
void* AlbumsModelb23e62_MimeDataDefault(void* ptr, void* indexes);
struct Moc_PackedString AlbumsModelb23e62_MimeTypesDefault(void* ptr);
char AlbumsModelb23e62_MoveColumnsDefault(void* ptr, void* sourceParent, int sourceColumn, int count, void* destinationParent, int destinationChild);
char AlbumsModelb23e62_MoveRowsDefault(void* ptr, void* sourceParent, int sourceRow, int count, void* destinationParent, int destinationChild);
void* AlbumsModelb23e62_ParentDefault(void* ptr, void* index);
char AlbumsModelb23e62_RemoveColumnsDefault(void* ptr, int column, int count, void* parent);
char AlbumsModelb23e62_RemoveRowsDefault(void* ptr, int row, int count, void* parent);
void AlbumsModelb23e62_ResetInternalDataDefault(void* ptr);
void AlbumsModelb23e62_RevertDefault(void* ptr);
struct Moc_PackedList AlbumsModelb23e62_RoleNamesDefault(void* ptr);
int AlbumsModelb23e62_RowCountDefault(void* ptr, void* parent);
char AlbumsModelb23e62_SetDataDefault(void* ptr, void* index, void* value, int role);
char AlbumsModelb23e62_SetHeaderDataDefault(void* ptr, int section, long long orientation, void* value, int role);
char AlbumsModelb23e62_SetItemDataDefault(void* ptr, void* index, void* roles);
void AlbumsModelb23e62_SortDefault(void* ptr, int column, long long order);
void* AlbumsModelb23e62_SpanDefault(void* ptr, void* index);
char AlbumsModelb23e62_SubmitDefault(void* ptr);
long long AlbumsModelb23e62_SupportedDragActionsDefault(void* ptr);
long long AlbumsModelb23e62_SupportedDropActionsDefault(void* ptr);
void AlbumsModelb23e62_ChildEventDefault(void* ptr, void* event);
void AlbumsModelb23e62_ConnectNotifyDefault(void* ptr, void* sign);
void AlbumsModelb23e62_CustomEventDefault(void* ptr, void* event);
void AlbumsModelb23e62_DeleteLaterDefault(void* ptr);
void AlbumsModelb23e62_DisconnectNotifyDefault(void* ptr, void* sign);
char AlbumsModelb23e62_EventDefault(void* ptr, void* e);
char AlbumsModelb23e62_EventFilterDefault(void* ptr, void* watched, void* event);
void AlbumsModelb23e62_TimerEventDefault(void* ptr, void* event);

#ifdef __cplusplus
}
#endif

#endif