package models

import (
	"github.com/therecipe/qt/core"
)

const (
	TTitle = int(core.Qt__UserRole) + 1<<iota
	TAlbum
	TArtist
	TAlbumArtist
	TGenre
	TTrackPosition
	TDisc
	TDuration
	TPicture
	TFilePath
	TIsFavourite
)

type TracksModel struct {
	core.QAbstractListModel
	_ func() `constructor:"init"`
}

func (tm *TracksModel) init() {
	tm.ConnectRowCount(func(parent *core.QModelIndex) int {
		return len(Engine.Player.Tracks())
	})
	tm.ConnectRoleNames(func() map[int]*core.QByteArray {
		return map[int]*core.QByteArray{
			TTitle:         core.NewQByteArray2("title", -1),
			TAlbum:         core.NewQByteArray2("album", -1),
			TArtist:        core.NewQByteArray2("artist", -1),
			TAlbumArtist:   core.NewQByteArray2("albumArtist", -1),
			TGenre:         core.NewQByteArray2("genre", -1),
			TTrackPosition: core.NewQByteArray2("trackPosition", -1),
			TDisc:          core.NewQByteArray2("disc", -1),
			TDuration:      core.NewQByteArray2("duration", -1),
			TPicture:       core.NewQByteArray2("picture", -1),
			TFilePath:      core.NewQByteArray2("filePath", -1),
			TIsFavourite:   core.NewQByteArray2("isFavourite", -1),
		}
	})
	tm.ConnectData(func(index *core.QModelIndex, role int) *core.QVariant {
		if !index.IsValid() {
			return core.NewQVariant()
		}
		if index.Row() >= len(Engine.Player.Tracks()) {
			return core.NewQVariant()
		}
		t := Engine.Player.Tracks()[index.Row()]

		switch role {
		case TTitle:
			{
				return core.NewQVariant1(t.Title)
			}
		case TAlbum:
			{
				return core.NewQVariant1(t.Album)
			}
		case TArtist:
			{
				return core.NewQVariant1(t.Artist)
			}
		case TAlbumArtist:
			{
				return core.NewQVariant1(t.AlbumArtist)
			}
		case TGenre:
			{
				return core.NewQVariant1(t.Genre)
			}
		case TTrackPosition:
			{
				return core.NewQVariant1(t.TrackPosition)
			}
		case TDisc:
			{
				return core.NewQVariant1(t.Disc)
			}
		case TDuration:
			{
				return core.NewQVariant1(t.Duration)
			}
		case TPicture:
			{
				return core.NewQVariant1(t.Picture)
			}
		case TFilePath:
			{
				return core.NewQVariant1(t.FilePath)
			}
		case TIsFavourite:
			{
				return core.NewQVariant1(t.IsFavourite)
			}
		default:
			{
				return core.NewQVariant()
			}
		}
	})
}