package player

import (
	"alortee/audplay/player/bridge"
	"alortee/audplay/player/engine"
	"alortee/audplay/player/models"
	"alortee/audplay/player/views"
	"github.com/therecipe/qt/widgets"

	_ "alortee/audplay/player/assets"
	_ "alortee/audplay/player/components"
	_ "alortee/audplay/player/theme"
)

type Player struct {
	application *widgets.QApplication
	engine      *engine.Engine
	views       *views.Views
}

func NewPlayer(args []string) *Player {
	p := &Player{}

	p.engine = engine.NewEngine()
	p.engine.Bootstrap()

	p.views = views.NewViews()

	models.Engine = p.engine
	bridge.Engine = p.engine
	bridge.Views = p.views

	p.application = widgets.NewQApplication(len(args), args)
	return p
}

func (p *Player) Run() {
	p.run()
	p.application.Exec()
}

func (p *Player) run() {
	if p.engine.FirstRun() {
		p.views.AddView("FirstRun")
		p.views.ShowView("FirstRun")
	} else {
		p.views.AddView("Default")
		p.views.ShowView("Default")
	}
}
