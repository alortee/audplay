pragma Singleton

import QtQuick 2.7

QtObject {
    id: theme

    property QtObject active: theme.dark
    property QtObject dark: QtObject {
        property var name: "dark"
        property var background: "#101010"
        property var sidebar: "#202020"
        property var accent: "#6affe2"
        property var buttonBg: "#525252"
        property var inputFieldBg: "#525252"
        property var heading: "#f7f8fa"
        property var text: Qt.lighter("#413f40", 3)
    }
    property QtObject light: QtObject {
        property var name: "light"
        property var background: "#ffffff"
        property var sidebar: "#f7f8fa"
        property var accent: "#6affe2"
        property var buttonBg: "#f7f8fa"
        property var inputFieldBg: "#f7f8fa"
        property var heading: "#f7f8fa"
        property var text: Qt.lighter("#413f40", 3)
    }
    property var setActive: function (name) {
        if (name === "dark") {
            theme.active = theme.dark
        } else {
            theme.active = theme.light
        }
    }
}
