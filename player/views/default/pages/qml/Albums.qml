import QtQuick 2.7
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.13
import Bridge 1.0
import Theme 1.0

Item {

    ColumnLayout {
    	anchors.fill: parent
    	anchors.margins: 20
    	spacing: 15

    	RowLayout {
            id: rowLayout
            Layout.alignment: Qt.AlignTop
            spacing: 10

            Text {
				Layout.fillWidth: true
				Layout.minimumWidth: 150
                text:  grid.count + " Albums"
                font.pixelSize: 14
                color: Theme.active.text
                elide: Text.ElideRight
            }
    	}

    	RowLayout {
    		Rectangle {
				Layout.fillWidth: true
				Layout.fillHeight: true
				color: "transparent"

				GridView {
                    id: grid
                    anchors.fill: parent
                    cellWidth: width / columns; cellHeight: 260
                    ScrollBar.vertical: ScrollBar {}
                    focus: true
                    clip: true

                    property var columns: 4
                    property var coverWidth: 170

                    onWidthChanged: {
                        let newColumns = Math.floor(width / (coverWidth + 20))
                        if (newColumns < 1) {
                            newColumns = 1
                        }
                        columns = newColumns
                    }

                    model: Bridge.albumsModel

                    delegate: Item {
                        height: grid.cellHeight
                        width: grid.cellWidth

                        Rectangle {
                            id: imgContainer
                            height: grid.coverWidth + 6
                            width: grid.coverWidth + 6
                            color: "transparent"
                            anchors.horizontalCenter: parent.horizontalCenter

                            // border.width: 1
                            // border.color: "#6affe2"

                            Image {
                                id: image
                                source: picture == "" ? Qt.resolvedUrl("assets/default-artwork.png") : Qt.resolvedUrl(Bridge.thumbsPath + "/150x150_" + picture)
                                anchors.centerIn: parent
                                fillMode: Image.PreserveAspectCrop
                                clip: true
                                smooth: true
                                visible: true
                                height: grid.coverWidth
                                width: grid.coverWidth
                            }
                        }

                        Text {
                            id: albumName
                            anchors.top: imgContainer.bottom
                            anchors.left: imgContainer.left
                            anchors.topMargin: 10
                            anchors.leftMargin: 2.5
                            text: album
                            font.pixelSize: 13
                            font.bold: true
                            elide: Text.ElideRight
                            color: "white"
                            width: image.width
                        }

                        Text {
                            id: albumArtistName
                            anchors.top: albumName.bottom
                            anchors.left: imgContainer.left
                            anchors.leftMargin: 2.5
                            text: albumArtist == "" ? "Unknown Artist" : albumArtist
                            font.pixelSize: 13
                            elide: Text.ElideRight
                            color: "white"
                            width: image.width
                        }
                    }
                }
    		}
    	}
    }
}