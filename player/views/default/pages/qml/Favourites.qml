import QtQuick 2.7
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.13
import Theme 1.0
import Bridge 1.0

Item {
	id: ___root

	Component {
        id: trackDelegate
        Rectangle {
        	id: dgRoot
        	width: parent.width
        	height: 35
        	color: (Bridge.currentFilePath == filePath || maa.containsMouse) ? Qt.lighter("#413f40", 1) : "transparent"
        	radius: 4

        	RowLayout {
        		spacing: 20
        		anchors.verticalCenter: parent.verticalCenter

        		Item {
    				Layout.preferredWidth: 45
	                Layout.leftMargin: 0

	                property var isPlaying: Bridge.currentFilePath == filePath

        			IconButton {
	        			visible: parent.isPlaying
			        	anchors.verticalCenter: parent.verticalCenter
			        	anchors.right: parent.right
		            	anchors.rightMargin: 5
				    	icon: "md-pulse"
				    	color: Theme.active.accent
				    	size: 13
				    	bold: false
				    	asText: true
				    }

	        		Label {
                        visible: !parent.isPlaying
			        	anchors.verticalCenter: parent.verticalCenter
		            	anchors.right: parent.right
		            	anchors.rightMargin: 5

		            	text: index + 1
		            	elide: Text.ElideRight
		            	horizontalAlignment: Text.AlignRight
		            	color: Theme.active.text
		            	font.pixelSize: 13
		            }
        		}

	            Label {
	                Layout.preferredWidth: parent.parent.width * 0.3 - 40
	            	text: title
	            	elide: Text.ElideRight
	            	color: Theme.active.text
	            	font.pixelSize: 13
	            }

	            Label {
	                Layout.preferredWidth: parent.parent.width * 0.3 - 60
	            	text: artist
	            	elide: Text.ElideRight
	            	color: Theme.active.text
	            	font.pixelSize: 13
	            }

	            Label {
	                Layout.preferredWidth: parent.parent.width * 0.25 - 40
	            	text: album
	            	elide: Text.ElideRight
					color: Theme.active.text
	            	font.pixelSize: 13
	            }

	            Label {
	                Layout.preferredWidth: 70
	            	text: duration
	            	horizontalAlignment: Text.AlignRight
					color: Theme.active.text
	            	font.pixelSize: 13
	            }
	        }

	        MouseArea {
        		id: maa
        		anchors.fill: parent
        		hoverEnabled: true
        		onClicked: {
        		    tracksListView.currentIndex = index
        		    Bridge.qMedia.playSingleTrack(index)
        		}
        	}

            IconButton {
                id: favIndicator

                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 20
                hoverIcon: "md-heart"
                hoverColor: Theme.active.accent
                size: 14
                bold: false
                states: [
                    State {
                        name: "isFavouriteState"; when: isFavourite
                        PropertyChanges { target: favIndicator; icon: "md-heart"; color: Theme.active.accent }
                    },
                    State {
                        name: "isNotFavouriteState"; when: !isFavourite
                        PropertyChanges { target: favIndicator; icon: "md-heart-empty"; color: Theme.active.text }
                    }
                ]
                onClicked: {
                    Bridge.toggleTrackFavourite(filePath)
                }
            }
        }
    }

    Component {
        id: headerDelegate
        Rectangle {
        	width: parent.width
        	height: 40
        	color: "transparent"

        	RowLayout {
        		spacing: 20
        		anchors.verticalCenter: parent.verticalCenter

                Item {
                    Layout.preferredWidth: 45
                    Layout.leftMargin: 0

                    Label {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: 5

                        text: "#"
                        elide: Text.ElideRight
                        horizontalAlignment: Text.AlignRight
                        color: Theme.active.text
                        font.pixelSize: 13
                        font.bold: true
                        font.capitalization: Font.AllUppercase
                    }
                }

	            Label {
	                Layout.preferredWidth: parent.parent.width * 0.3 - 40
	            	text: "title"
	            	elide: Text.ElideRight
	            	color: Theme.active.text
	            	font.pixelSize: 13
	            	font.bold: true
	            	font.capitalization: Font.AllUppercase
	            }

	            Label {
	                Layout.preferredWidth: parent.parent.width * 0.3 - 60
	            	text: "artist"
	            	elide: Text.ElideRight
	            	color: Theme.active.text
	            	font.pixelSize: 13
	            	font.bold: true
	            	font.capitalization: Font.AllUppercase
	            }

	            Label {
	                Layout.preferredWidth: parent.parent.width * 0.25 - 40
	            	text: "album"
	            	elide: Text.ElideRight
					color: Theme.active.text
	            	font.pixelSize: 13
	            	font.bold: true
	            	font.capitalization: Font.AllUppercase
	            }

	            Label {
	                Layout.preferredWidth: 70
	            	text: "length"
	            	horizontalAlignment: Text.AlignRight
					color: Theme.active.text
	            	font.pixelSize: 13
	            	font.bold: true
	            	font.capitalization: Font.AllUppercase
	            }
	        }
        }
    }

    ColumnLayout {
    	anchors.fill: parent
    	anchors.margins: 20
    	spacing: 15

    	RowLayout {
            id: rowLayout
            Layout.alignment: Qt.AlignTop
            spacing: 10

            Text {
				Layout.fillWidth: true
				Layout.minimumWidth: 150
                text:  tracksListView.count + " Tracks"
                font.pixelSize: 14
                color: Theme.active.text
                elide: Text.ElideRight
            }

            DarkButton {
				text:"Play All"
				onClicked: {
				    Bridge.qMedia.playSingleTrack(0)
				}
			}

			DarkButton {
				text:"Shuffle"
				onClicked: {
				    Bridge.qMedia.shuffleAllTracks()
                }
			}
    	}

    	RowLayout {
    		Rectangle {
				Layout.fillWidth: true
				Layout.fillHeight: true
				color: "transparent"

				ListView {
                    id: tracksListView
				    anchors.fill: parent
					clip: true
			    	spacing: 0
					model: Bridge.favouritesModel
					delegate: trackDelegate
					header: headerDelegate
			        ScrollBar.vertical: ScrollBar {}
			        // preferredHighlightBegin: height / 2 - 17.5
                    // preferredHighlightEnd: height / 2 + 17.5
                    // highlightRangeMode: ListView.ApplyRange
                    // snapMode: ListView.SnapToItem
                    // boundsBehavior: Flickable.StopAtBounds
				}
    		}
    	}
    }
}
