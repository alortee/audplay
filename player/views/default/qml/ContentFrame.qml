import QtQuick 2.7
import QtQuick.Controls 2.5
import Theme 1.0
import Bridge 1.0

Item {
    anchors.fill: parent
    Rectangle {
        id: topNav

        height: 60
        width: parent.width
        anchors.top: parent.top

        color: "transparent"

        Row {
            id: navButtons
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            spacing: 30

            IconButton {
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                icon: "md-arrow-back"
                color: Theme.active.text
                size: 14
            }
        }

        TextField {
            id: search
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            placeholderText: "Search"

            palette.text: "#dedede"
            leftPadding: 35
            background: Rectangle {
                implicitWidth: 280
                implicitHeight: 32
                radius: 4
                color: Qt.lighter("#413f40", 1)

                IconButton {
                    icon: "md-search"
                    size: 13
                    anchors.verticalCenter: parent.verticalCenter
                    x: 10
                    color: Theme.active.text
                    bold: false
                }
            }
        }
    }

    StackView {
        id: stack
        initialItem: Tracks{}
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.top: topNav.bottom
        clip: true
    }

    Connections {
        target: Bridge.navigator
        onNavigated: {
            switch (view) {
                case "Tracks":
                    stack.replace("Tracks.qml")
                    break
                case "Albums":
                    stack.replace("Albums.qml")
                    break
                case "Favourites":
                    stack.replace("Favourites.qml")
                    break
                default:
                    stack.replace("Tracks.qml")
            }
        }
    }
}