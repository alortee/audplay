import QtQuick 2.7
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.13
import Theme 1.0
import Bridge 1.0


Item {
	id: controlsbar
	anchors.fill: parent

	property string albumArt: Bridge.currentPicture == "" ? Qt.resolvedUrl("assets/default-artwork.png") : Qt.resolvedUrl(Bridge.thumbsPath + "/150x150_" + Bridge.currentPicture)

	Slider {
    	id: control
    	anchors.top: parent.top
    	width: parent.width
    	anchors.topMargin: 0 - control.handle.height * 0.5

	    z: 1
	    leftPadding: 0
		rightPadding: 0
		topPadding: 0
		bottomPadding: 0

    	from: 0
    	to: 100
    	value: 0

    	live: false

    	onPressedChanged: {
            if (!control.pressed) {
                Bridge.qMedia.seek(control.valueAt(control.position))
            }
    	}

    	Connections {
    	    target: Bridge.qMedia
    	    onCurrentDurationChanged: {
                control.to = Bridge.qMedia.currentDuration
    	    }
    	}

    	Connections {
            target: Bridge.qMedia
            onCurrentPositionChanged: {
                if (!control.pressed) {
                    control.value = Bridge.qMedia.currentPosition
                }
            }
        }

    	background: Rectangle {
    		x: control.leftPadding
	        y: control.topPadding + control.availableHeight / 2 - height / 2
	        implicitWidth: 200
	        implicitHeight: 4
	        width: control.width
	        height: 5
	        radius: 0
	        color: "#bdbebf"

	        Rectangle {
	            width: control.visualPosition * parent.width
	            height: parent.height
	            color: Theme.active.accent
	        }
	    }

	    handle: Rectangle {
	    	visible: control.hovered || control.pressed
        	x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
	        y: control.topPadding + control.availableHeight / 2 - height / 2
	        implicitWidth: 20
	        implicitHeight: 20
	        height: 15
	        width: 15
	        radius: 30
	        color: control.pressed ? Theme.active.accent : Theme.active.accent
	        border.color: Theme.active.accent
	    }
    }

    Item {
    	id: ij45
    	anchors.top: parent.top
    	anchors.bottom: parent.bottom
    	anchors.left: parent.left
    	anchors.right: parent.right

    	anchors.topMargin: (control.availableHeight / 4)

    	Image {
    		id: albumArtBG
	    	anchors.fill: parent
	    	source: controlsbar.albumArt
	    	fillMode: Image.PreserveAspectCrop
		    clip: true
		    smooth: true
	        visible: false
	    }

	    GaussianBlur {
	    	id: blurBG
	        anchors.fill: albumArtBG
	        source: albumArtBG
	        samples: 256
	    }

	    ColorOverlay {
	    	id: colorOverlay
	        anchors.fill: blurBG
	        source: blurBG
	        color: "#d9202020"
	    }

	    Row {

	    	anchors.verticalCenter: parent.verticalCenter
	    	leftPadding: 20

	    	spacing: 20

	    	Rectangle {
				id: currentTrackImage
                visible: Bridge.rootWidth > Bridge.breakPoint
		    	anchors.verticalCenter: parent.verticalCenter

			    width: 70
			    height: 70
			    color: "blue"

			    Image {
			    	anchors.fill: parent
			    	source: controlsbar.albumArt
			    	fillMode: Image.PreserveAspectCrop
			    }
			}

			Rectangle {
				id: trackInfo
				width: Math.min(Math.max(trackTitle.implicitWidth, trackArtist.implicitWidth), ij45.width * 0.2)
			    height: 45
			    color: "transparent"
		    	anchors.verticalCenter: parent.verticalCenter

			    Text {
			        id: trackTitle
			    	width: parent.width
			    	anchors.top: parent.top
			    	text: Bridge.currentTitle
			    	color: Qt.lighter(Theme.active.heading, 1.8)
			    	elide: Text.ElideRight
			    	font.pixelSize: 15
			    	font.bold: true
			    }

			    Text {
                    id: trackArtist
			    	width: parent.width
			    	anchors.top: parent.children[0].bottom
			    	text: Bridge.currentArtist
			    	elide: Text.ElideRight
			    	color: Theme.active.heading
			    	font.pixelSize: 14
			    }
			}

			Row {
				id: trackActions
		    	anchors.verticalCenter: parent.verticalCenter

				spacing: 10

			    IconButton {
			    	anchors.verticalCenter: parent.verticalCenter
			    	icon: Bridge.currentFavourite ? "md-heart" : "md-heart-empty"
			    	hoverIcon: "md-heart"
			    	hoverColor: Theme.active.accent
			    	color: Bridge.currentFavourite ? Theme.active.accent : Theme.active.text
			    	size: 14
			    	onClicked: {
                        Bridge.toggleTrackFavourite(Bridge.currentFilePath)
                    }
			    }
			}
	    }

		Rectangle {
			id: playbackControls
			height: 30
			width: 220
		    color: "transparent"

			anchors.verticalCenter: parent.verticalCenter
			anchors.centerIn: parent

			IconButton {
				anchors.verticalCenter: parent.verticalCenter
		    	icon: "ios-shuffle"
		    	color: Theme.active.text
		    	size: 16
		    }

		    IconButton {
				anchors.verticalCenter: parent.verticalCenter
				anchors.right: parent.right
		    	icon: "ios-repeat"
		    	color: Theme.active.text
		    	size: 16
		    }

		    Row {
		    	anchors.centerIn: parent
		    	spacing: 40

		    	IconButton {
					anchors.verticalCenter: parent.verticalCenter
			    	icon: "md-skip-backward"
			    	color: "whitesmoke"
		    		hoverColor: Theme.active.text
			    	size: 14
			    	onClicked: {
                        Bridge.qMedia.previousTrack()
			    	}
			    }

			    IconButton {
			        id: playbtn
			        property string btnIcon: "md-play"

			        visible: false
					anchors.verticalCenter: parent.verticalCenter
			    	icon: btnIcon
			    	color: "whitesmoke"
		    		hoverColor: Theme.active.text
			    	size: 24
			    	onClicked: {
                        Bridge.qMedia.togglePlayback()
			    	}
			    }

			    IconButton {
			        id: pausebtn
			        property string btnIcon: "md-pause"

			        visible: false
					anchors.verticalCenter: parent.verticalCenter
			    	icon: btnIcon
			    	color: "whitesmoke"
		    		hoverColor: Theme.active.text
			    	size: 24
			    	onClicked: {
                        Bridge.qMedia.togglePlayback()
			    	}
			    }

			    Connections {
                    target: Bridge.qMedia
                    onPlayerStateChanged: {
                        if (state == 1) {
                            playbtn.visible = false
                            pausebtn.visible = true
                        } else {
                            playbtn.visible = true
                            pausebtn.visible = false
                        }
                    }
                }

			    IconButton {
					anchors.verticalCenter: parent.verticalCenter
			    	icon: "md-skip-forward"
			    	color: "whitesmoke"
		    		hoverColor: Theme.active.text
			    	size: 14
			    	onClicked: {
                        Bridge.qMedia.nextTrack()
			    	}
			    }
		    }
		}

		Row {
		    id: rightPanel
	    	spacing: 20

	    	anchors.verticalCenter: parent.verticalCenter
	    	anchors.right: parent.right
	    	anchors.rightMargin: 20

	    	Text {
	    	    id: playbackTime

                property string duration: ""
                property string position: ""

                anchors.verticalCenter: parent.verticalCenter
                text: position + " / " + "<b>" + duration + "</b>"
                color: Theme.active.text
                font.pixelSize: 14

                Connections {
                    target: Bridge.qMedia
                    onCurrentDurationChanged: {
                        playbackTime.duration = Bridge.formatDuration(Bridge.qMedia.currentDuration)
                    }
                }

                Connections {
                    target: Bridge.qMedia
                    onCurrentPositionChanged: {
                        playbackTime.position = Bridge.formatDuration(Bridge.qMedia.currentPosition)
                    }
                }
            }

	    	Row {
	    		spacing: 7

                IconButton {
                    visible: volumeSlider.position > 0.5
					anchors.verticalCenter: parent.verticalCenter
			    	icon: "md-volume-high"
			    	hoverIcon: "md-volume-off"
			    	color: "whitesmoke"
			    	hoverColor: Theme.active.text
			    	size: 18
			    	bold: false
			    }

		    	IconButton {
                    visible: volumeSlider.position <= 0.5
					anchors.verticalCenter: parent.verticalCenter
			    	icon: "md-volume-low"
			    	hoverIcon: "md-volume-off"
			    	color: "whitesmoke"
			    	hoverColor: Theme.active.text
			    	size: 18
			    	bold: false
			    }

			    Slider {
			    	id: volumeSlider

			    	leftPadding: 0
					rightPadding: 0
					topPadding: 0
					bottomPadding: 0

					visible: Bridge.rootWidth > Bridge.breakPoint

					from: 0
			    	to: 100
			    	value: 100

			    	onMoved: {
			    	    Bridge.qMedia.setVolume(volumeSlider.valueAt(volumeSlider.position))
			    	}

			    	Component.onCompleted: {
			    	    Bridge.qMedia.setVolume(volumeSlider.valueAt(volumeSlider.position))
			    	}

			    	background: Rectangle {
			    		x: volumeSlider.leftPadding
				        y: volumeSlider.topPadding + volumeSlider.availableHeight / 2 - height / 2
				        implicitWidth: 100
				        implicitHeight: 4
				        height: 4
				        radius: 2
				        color: "#aa020202"

				        Rectangle {
				            width: volumeSlider.visualPosition * parent.width
				            height: parent.height
					        radius: 3
				            color: "whitesmoke"
				        }
			    	}

				    handle: Rectangle {
				    	visible: volumeSlider.hovered

			        	x: volumeSlider.leftPadding + volumeSlider.visualPosition * (volumeSlider.availableWidth - width)
				        y: volumeSlider.topPadding + volumeSlider.availableHeight / 2 - height / 2
				        implicitWidth: 20
				        implicitHeight: 20
				        height: 13
				        width: 13
				        radius: 30
				        color: control.pressed ? "#f0f0f0" : "#f6f6f6"
				        border.color: "#bdbebf"
				    }
			    }
	    	}

		    IconButton {
				anchors.verticalCenter: parent.verticalCenter
		    	icon: "md-reorder"
		    	color: Theme.active.text
		    	size: 18
		    	bold: false
		    }
	    }
    }
}