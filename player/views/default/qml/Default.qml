import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.13
import Theme 1.0
import Bridge 1.0

ColumnLayout {
    id: appLayout
    spacing: 0
    visible: true

    FontLoader { id: ioniconsFont; source: "assets/ionicons.ttf" }

    onWidthChanged: {
        Bridge.rootWidth = width
    }

    Item {
        id: app
        Layout.fillHeight: true
        Layout.fillWidth: true

        RowLayout {
            anchors.fill: parent
            spacing: 0

            Rectangle {
                id: sidebar
                Layout.fillHeight: true
                width: 250
                color: Theme.active.sidebar
                visible: Bridge.rootWidth > Bridge.breakPoint
                SideBar{}
            }

            Rectangle {
                id: contentFrame
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: Theme.active.background
                ContentFrame{}
            }
        }
    }

    Item {
        id: controls
        Layout.fillWidth: true
        height: 100
        visible: controls.shouldBeVisible
        ControlBar{}

        property var shouldBeVisible: false

        Connections {
            target: Bridge.qMedia
            onCurrentMediaChanged: {
                Bridge.currentTitle = title
                Bridge.currentArtist = artist
                Bridge.currentPicture = picture
                Bridge.currentFilePath = filePath
                Bridge.currentFavourite = favourite
                controls.shouldBeVisible = true
            }
        }
    }
}