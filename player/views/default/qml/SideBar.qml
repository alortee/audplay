import QtQuick 2.7
import QtQuick.Controls 2.5
import Theme 1.0
import Bridge 1.0
import QtQuick.Layouts 1.13

Item {
	id: sidebar
	anchors.fill: parent

	ListModel {
		id: sidebarItemsModel

		ListElement { active: true;	    group: "library"; 		textContent: "Tracks"; iconn: "md-disc" }
		ListElement { active: false; 	group: "library"; 		textContent: "Albums"; iconn: "md-albums" }
		ListElement { active: false; 	group: "library"; 		textContent: "Artists"; iconn: "md-person" }
		ListElement { active: false; 	group: "library"; 		textContent: "Favourites"; iconn: "md-heart" }
		ListElement { active: false; 	group: "library"; 		textContent: "Recent"; iconn: "md-time" }

		// ListElement { active: false; 	group: "playlists"; 	textContent: "Create Playlist"; iconn: "md-add-circle" }
		// ListElement { active: false; 	group: "playlists"; 	textContent: "Playback History"; iconn: "md-list" }
		// ListElement { active: false; 	group: "playlists"; 	textContent: "Random Tracks"; iconn: "md-list" }
		// ListElement { active: false; 	group: "playlists"; 	textContent: "Cool Vibez"; iconn: "md-list" }
	}

	Component {
		id: modelDelegate

		Rectangle {
			id: libraryItem
			width: parent.width
			height: 30
			color: "transparent"

			property int myIndex: index

			property var accent: (ListView.isCurrentItem || libraryItemMouseArea.containsMouse) ? Theme.active.accent : Theme.active.text

			MouseArea {
				id: libraryItemMouseArea
			    anchors.fill: parent
			    cursorShape: Qt.PointingHandCursor
			    hoverEnabled: true
			    onEntered: {}
			    onExited: {}
			    onClicked: {
			    	list.currentIndex = index
			    	Bridge.navigator.navigateTo(textContent)
			    }
			}

			Rectangle {
				id: selectIndicator
				height: parent.height * 0.5
				width: 3
				color: libraryItem.accent
				visible: libraryItem.ListView.isCurrentItem

				anchors {
					left: parent.left
					verticalCenter: parent.verticalCenter
				}
			}

			Rectangle {
				id: labelIcon
				height: parent.height
				width: parent.height
				color: "transparent"

				anchors {
					left: parent.left
					leftMargin: 30
				}

				IconButton {
                    anchors.verticalCenter: parent.verticalCenter
				    icon: iconn
				    size: 13
                    color: libraryItem.accent
                    asText: true
				}
			}

			Rectangle {
				id: labelText
				height: parent.height
				color: "transparent"

				anchors {
					right: parent.right
					left: labelIcon.right
				}

				Text {
					anchors.verticalCenter: parent.verticalCenter
					text: textContent
					color: libraryItem.accent
					font.pixelSize: 13
				}
			}
		}
	}

	Component {
		id: sectionDelegate

		Rectangle {
			width: parent.width
			height: 70
			color: "transparent"

			Text {
				y: 40
				id: libraryLabel
				text: section
				color: Theme.active.heading
				font.pixelSize: 13
				font.bold: true
				font.letterSpacing: 1.5
				font.capitalization: Font.AllUppercase

				anchors.left: parent.left
				anchors.leftMargin: 30
			}
		}
	}

	ScrollView {
        anchors.fill: parent

        Column {
            spacing: 40
            y: 40

            Column {
                id: libLab
                spacing: 30

                Text {
                    id: libraryLabel
                    text: "LIBRARY"
                    color: Theme.active.heading
                    font.pixelSize: 13
                    font.bold: true
                    font.letterSpacing: 1.5
                    font.capitalization: Font.AllUppercase

                    anchors.left: parent.left
                    anchors.leftMargin: 30
                }

                Column {
                    spacing: 5

                    LibraryItem { itemIcon: "md-disc"; textContent: "Tracks"; onClicked: Bridge.navigator.navigateTo("Tracks") }
                    LibraryItem { itemIcon: "md-albums"; textContent: "Albums"; onClicked: Bridge.navigator.navigateTo("Albums") }
                    LibraryItem { itemIcon: "md-person"; textContent: "Artists"; onClicked: Bridge.navigator.navigateTo("Artists") }
                    LibraryItem { itemIcon: "md-heart"; textContent: "Favourites"; onClicked: Bridge.navigator.navigateTo("Favourites") }
                }
            }

             Column {
                 id: playLab

                 spacing: 30

                 Text {
                     y: 40
                     id: playlistsLabel
                     text: "PLAYLISTS"
                     color: Theme.active.heading
                     font.pixelSize: 13
                     font.bold: true
                     font.letterSpacing: 1.5
                     font.capitalization: Font.AllUppercase
                     anchors.left: parent.left
                     anchors.leftMargin: 30
                 }

                 Column {
                     spacing: 5

                     LibraryItem { itemIcon: "md-add-circle"; textContent: "Create Playlist" }
                 }
            }
        }
    }
}
