import QtQuick 2.7
import QtQuick.Controls 2.4
import Theme 1.0
import Bridge 1.0
import Qt.labs.platform 1.1

Rectangle {
    id: addFoldersView
    color: Theme.active.background
    visible: true

    FontLoader { id: ioniconsFont; source: "assets/ionicons.ttf" }

    property ListModel folders: ListModel{}
    property var foldersArray: []

    Text {
        id: message
        anchors.top: parent.top
        anchors.topMargin: 80
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Add folders with your music to your library"
        color: Theme.active.heading
        font.pixelSize: 16
    }

    DarkButton {
        id: addFoldersButton
        anchors.top: message.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Add A Folder"
        sideIcon: "md-add"
        onClicked: {
            folderDialog.open()
        }
    }

    Item {
        width: parent.width > 640 ? 600 : parent.width - 40
        height: Math.min(parent.height - 300, foldersListView.count * 50)
        anchors.top: addFoldersButton.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter

        ListView {
            id: foldersListView
            anchors.fill: parent
            model: addFoldersView.folders
            delegate: folderListDelegate
            clip: true
            ScrollBar.vertical: ScrollBar {
                 // policy: ScrollBar.AlwaysOn
            }
        }

        Text {
            id: statusText

            property int tracksCount: 0
            property int importedCount: 0
            anchors.top: foldersListView.bottom
            anchors.topMargin: 20
            color: Theme.active.text
            font.pixelSize: 12
        }

        DarkButton {
            id: importBtn
            anchors.top: foldersListView.bottom
            anchors.topMargin: 20
            anchors.right: parent.right
            text: "Import"
            visible: foldersListView.count > 0

            onClicked: {
                importBtn.enabled = false
                addFoldersButton.enabled = false
                statusText.text = "scanning folders for audio files..."
                Bridge.addFolders(addFoldersView.foldersArray)
            }
        }

        Component {
            id: folderListDelegate

            Item {
                height: 50
                width: parent.width

                IconButton {
                    id: ficon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    icon: "md-folder"
                    size: 30
                    asText: true
                }

                Item {
                    id: wrapper
                    height: parent.height
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: ficon.right
                    anchors.leftMargin: 15
                    anchors.right: ricon.left
                    Column {
                        anchors.verticalCenter: parent.verticalCenter
                        Text {
                            width: wrapper.width
                            text: path.split("/").pop()
                            font.pixelSize: 14
                            color: Theme.active.text
                            elide: Text.ElideRight
                            font.bold: true

                        }
                        Text {
                            width: wrapper.width
                            text: path.split("file://").pop()
                            font.pixelSize: 13
                            color: Theme.active.text
                            elide: Text.ElideRight
                        }
                    }
                }

                IconButton {
                    id: ricon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    icon: "md-remove"
                    onClicked: {
                        addFoldersView.folders.remove(index)
                        addFoldersView.foldersArray.splice(index, 1)
                    }
                }
            }
        }
    }

    FolderDialog {
        id: folderDialog
        folder: StandardPaths.standardLocations(StandardPaths.MusicLocation)[0]
        currentFolder: StandardPaths.standardLocations(StandardPaths.MusicLocation)[0]
        options: FolderDialog.ShowDirsOnly
        onAccepted: {
            if (!addFoldersView.foldersArray.includes(folderDialog.folder.toString())) {
                addFoldersView.foldersArray.push(folderDialog.folder.toString())
                addFoldersView.folders.append({
                    path: folderDialog.folder.toString()
                })
            }
        }
    }

    Connections {
        target: Bridge
        onFolderScanComplete: {
            statusText.tracksCount = scanCount
            Bridge.importTracks()
        }
    }

    Connections {
        target: Bridge
        onTrackImported: {
            statusText.importedCount++
            statusText.text = "importing " + statusText.importedCount + "/" + statusText.tracksCount + " tracks: " + trackTitle
        }
    }
}