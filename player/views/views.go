package views

import (
	"errors"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/quick"

	_ "alortee/audplay/player/views/default"
	_ "alortee/audplay/player/views/default/pages"
	_ "alortee/audplay/player/views/firstrun"
)

var (
	scale                = 50
	ErrViewNotRegistered = errors.New("view not registered")
)

type Views struct {
	Data map[string]*quick.QQuickView
}

func NewViews() *Views {
	return &Views{
		Data: make(map[string]*quick.QQuickView, 2),
	}
}

func (vs *Views) AddView(name string) {
	v := quick.NewQQuickView(nil)
	v.SetResizeMode(quick.QQuickView__SizeRootObjectToView)
	v.SetMinimumSize(core.NewQSize2(16*scale, 9*scale))
	v.Resize(core.NewQSize2(16*scale, 9*scale))
	v.Engine().AddImportPath("qrc:/qml/")
	v.SetSource(core.NewQUrl3("qrc:/qml/"+name+".qml", 0))
	vs.Data[name] = v
}

func (vs *Views) RemoveView(name string) {
	v, ok := vs.Data[name]
	if ok {
		v.Hide()
		v.Destroy()
		delete(vs.Data, name)
	}
}

func (vs *Views) ShowView(name string) {
	v, ok := vs.Data[name]
	if ok {
		v.SetVisibility(gui.QWindow__Maximized)
		v.Show()
	}
}

func (vs *Views) HideView(name string) {
	v, ok := vs.Data[name]
	if ok {
		v.Hide()
	}
}

func (vs *Views) IsVisible(name string) bool {
	v, ok := vs.Data[name]
	if ok {
		return v.IsVisible()
	}
	return false
}

func (vs *Views) Has(name string) bool {
	_, ok := vs.Data[name]
	if ok {
		return true
	}
	return false
}

func (vs *Views) Update(name string) {
	v, ok := vs.Data[name]
	if ok {
		v.Update()
	}
}